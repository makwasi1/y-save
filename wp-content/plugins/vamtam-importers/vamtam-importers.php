<?php

/*
Plugin Name: VamTam Importers
Description: This plugin is used in order to import the sample content for VamTam themes
Version: 2.17.0
Author: VamTam
Author URI: http://vamtam.com
*/

class Vamtam_Importers {
	public static $import_attachments;

	public function __construct() {
		add_action( 'admin_init', array( __CLASS__, 'admin_init' ), 1 );
		add_action( 'plugins_loaded', array( __CLASS__, 'plugins_loaded' ) );

		if ( ! class_exists( 'Vamtam_Updates_2' ) ) {
			require 'vamtam-updates/class-vamtam-updates.php';
		}

		new Vamtam_Updates_2( __FILE__ );

		add_filter( 'heartbeat_received', [ __CLASS__, 'attachment_progress_heartbeat_callback' ], 10, 2 );
	}

	public static function admin_init() {
		add_action( 'vamtam_before_content_import', array( __CLASS__, 'before_content_import' ) );
		add_action( 'vamtam_after_content_import', array( __CLASS__, 'after_content_import' ) );

		require_once 'importers/importer/importer.php';
		require 'importers/widget-importer/importer.php';
		require 'importers/revslider/importer.php';
		require 'importers/ninja-forms/importer.php';
		require 'importers/acx-coming-soon/importer.php';
		require 'importers/booked/importer.php';
		require 'importers/google-maps-easy/importer.php';
	}

	public static function plugins_loaded() {
		include 'wp-background-process/wp-async-request.php';
		include 'wp-background-process/wp-background-process.php';
		include 'vamtam-import-attachments.php';

		self::$import_attachments = new Vamtam_Import_Attachments();
	}

	/**
	 * Initialize thumbnail generation
	 */
	protected static function process_attachments() {
		delete_option( 'vamtam_import_attachments_url_remap' );
		update_option( 'vamtam_import_attachments_done', 0 );

		$attachments = get_option( 'vamtam_import_attachments_todo' );

		foreach ( $attachments['attachments'] as $attachment_data ) {
			self::$import_attachments->push_to_queue( [
				'step' => 1,
				'data' => [
					'attachment' => $attachment_data,
					'base_url'   => $attachments['base_url'],
				],
			] );
		}

		self::$import_attachments->save()->dispatch();
	}

	public static function get_attachment_progress() {
		$remaining = (int) self::$import_attachments->get_queue_length();

		$text = $remaining > 0 ?
			sprintf( esc_html__( '%d remaining', 'wpv' ), $remaining ) :
			esc_html__( 'all done', 'wpv' );

		return compact( 'text', 'remaining' );
	}

	public static function attachment_progress_heartbeat_callback( $response, $data ) {
		if ( empty( $data['vamtam_attachment_import_poll'] ) ) {
			return $response;
		}

		$attachments = get_option( 'vamtam_import_attachments_todo' )['attachments'];

		$total = is_countable( $attachments ) ? count( $attachments ) : 0;

		$progress = self::get_attachment_progress();

		$response['vamtam_attachment_import_progress'] = $progress['text'];

		// faster updates if we are expecting updates
		if ( $total > 0 ) {
			$response['heartbeat_interval'] = 5;
		}

		// slow down updates if we're done
		if ( $progress['remaining'] === 0 ) {
			$response['heartbeat_interval'] = 30;
		}

		return $response;
	}

	public static function process_post_additional_data( $post, $post_id, $post_exists, $processed_authors, &$featured_images = null ) {
		$comment_post_ID = $post_id;

		if ( ! isset( $post['terms'] ) ) {
			$post['terms'] = array();
		}

		$post['terms'] = apply_filters( 'wp_import_post_terms', $post['terms'], $post_id, $post );

		// add categories, tags and other terms
		if ( ! empty( $post['terms'] ) ) {
			$terms_to_set = array();
			foreach ( $post['terms'] as $term ) {
				// back compat with WXR 1.0 map 'tag' to 'post_tag'
				$taxonomy = ( 'tag' == $term['domain'] ) ? 'post_tag' : $term['domain'];
				$term_exists = term_exists( $term['slug'], $taxonomy );
				$term_id = is_array( $term_exists ) ? $term_exists['term_id'] : $term_exists;
				if ( ! $term_id ) {
					$t = wp_insert_term( $term['name'], $taxonomy, array( 'slug' => $term['slug'] ) );
					if ( ! is_wp_error( $t ) ) {
						$term_id = $t['term_id'];
						do_action( 'wp_import_insert_term', $t, $term, $post_id, $post );
					} else {
						printf( __( 'Failed to import %s %s', 'wordpress-importer' ), esc_html($taxonomy), esc_html($term['name']) );
						if ( defined('IMPORT_DEBUG') && IMPORT_DEBUG )
							echo ': ' . $t->get_error_message();
						echo '<br />';
						do_action( 'wp_import_insert_term_failed', $t, $term, $post_id, $post );
						continue;
					}
				}
				$terms_to_set[$taxonomy][] = intval( $term_id );
			}

			foreach ( $terms_to_set as $tax => $ids ) {
				$tt_ids = wp_set_post_terms( $post_id, $ids, $tax );
				do_action( 'wp_import_set_post_terms', $tt_ids, $ids, $tax, $post_id, $post );
			}
			unset( $post['terms'], $terms_to_set );
		}

		if ( ! isset( $post['comments'] ) )
			$post['comments'] = array();

		$post['comments'] = apply_filters( 'wp_import_post_comments', $post['comments'], $post_id, $post );

		// add/update comments
		if ( ! empty( $post['comments'] ) ) {
			$num_comments = 0;
			$inserted_comments = array();
			foreach ( $post['comments'] as $comment ) {
				$comment_id	= $comment['comment_id'];
				$newcomments[$comment_id]['comment_post_ID']      = $comment_post_ID;
				$newcomments[$comment_id]['comment_author']       = $comment['comment_author'];
				$newcomments[$comment_id]['comment_author_email'] = $comment['comment_author_email'];
				$newcomments[$comment_id]['comment_author_IP']    = $comment['comment_author_IP'];
				$newcomments[$comment_id]['comment_author_url']   = $comment['comment_author_url'];
				$newcomments[$comment_id]['comment_date']         = $comment['comment_date'];
				$newcomments[$comment_id]['comment_date_gmt']     = $comment['comment_date_gmt'];
				$newcomments[$comment_id]['comment_content']      = $comment['comment_content'];
				$newcomments[$comment_id]['comment_approved']     = $comment['comment_approved'];
				$newcomments[$comment_id]['comment_type']         = $comment['comment_type'];
				$newcomments[$comment_id]['comment_parent'] 	  = $comment['comment_parent'];
				$newcomments[$comment_id]['commentmeta']          = isset( $comment['commentmeta'] ) ? $comment['commentmeta'] : array();

				if ( isset( $processed_authors[$comment['comment_user_id']] ) )
					$newcomments[$comment_id]['user_id'] = $processed_authors[$comment['comment_user_id']];
			}
			ksort( $newcomments );

			foreach ( $newcomments as $key => $comment ) {
				// if this is a new post we can skip the comment_exists() check
				if ( ! $post_exists || ! comment_exists( $comment['comment_author'], $comment['comment_date'] ) ) {
					if ( isset( $inserted_comments[$comment['comment_parent']] ) )
						$comment['comment_parent'] = $inserted_comments[$comment['comment_parent']];
					$comment = wp_filter_comment( $comment );
					$inserted_comments[$key] = wp_insert_comment( $comment );
					do_action( 'wp_import_insert_comment', $inserted_comments[$key], $comment, $comment_post_ID, $post );

					foreach( $comment['commentmeta'] as $meta ) {
						$value = maybe_unserialize( $meta['value'] );
						add_comment_meta( $inserted_comments[$key], $meta['key'], $value );
					}

					$num_comments++;
				}
			}
			unset( $newcomments, $inserted_comments, $post['comments'] );
		}

		if ( ! isset( $post['postmeta'] ) ) {
			$post['postmeta'] = array();
		}

		$post['postmeta'] = apply_filters( 'wp_import_post_meta', $post['postmeta'], $post_id, $post );

		// add/update post meta
		if ( ! empty( $post['postmeta'] ) ) {
			foreach ( $post['postmeta'] as $meta ) {
				$key = apply_filters( 'import_post_meta_key', $meta['key'], $post_id, $post );
				$value = false;

				if ( '_edit_last' == $key ) {
					if ( isset( $processed_authors[intval($meta['value'])] ) ) {
						$value = $processed_authors[intval($meta['value'])];
					} else {
						$key = false;
					}
				}

				if ( $key ) {
					// export gets meta straight from the DB so could have a serialized string
					if ( ! $value )
						$value = maybe_unserialize( $meta['value'] );

					add_post_meta( $post_id, $key, $value );
					do_action( 'import_post_meta', $post_id, $key, $value );

					// if the post has a featured image, take note of this in case of remap
					if ( '_thumbnail_id' == $key && is_array( $featured_images ) ) {
						printf( __( 'Setting featured image for %s', 'wpv' ), $post_id );
						$featured_images[$post_id] = (int) $value;
					}
				}
			}
		}
	}

	public static function before_content_import() {
		wp_suspend_cache_invalidation( true );

		self::generic_option_import( 'jetpack', array( __CLASS__, 'jetpack_import' ) );
		self::generic_option_import( 'foodpress', array( __CLASS__, 'foodpress_import' ) );
		self::generic_option_import( 'the-events-calendar', array( __CLASS__, 'tribe_events_import' ) );

		if ( class_exists( 'FLBuilderModel' ) ) {
			$post_types = get_post_types( array( 'public' => true ), 'objects' );
			$post_types = apply_filters( 'fl_builder_admin_settings_post_types', $post_types );
			$post_types = array_diff( array_keys( $post_types ), array( 'attachment', 'fl-builder-template' ) );

			FLBuilderModel::update_admin_settings_option( '_fl_builder_post_types', $post_types, true );

			FLBuilderModel::save_global_settings( json_decode( file_get_contents( VAMTAM_SAMPLES_DIR . 'beaver-global-settings.json' ), true ) );

			FLBuilderUserAccess::save_settings( json_decode( file_get_contents( VAMTAM_SAMPLES_DIR . 'beaver-user-access.json' ), true ) );
		}

		wp_suspend_cache_invalidation( false );
	}

	public static function after_content_import() {
		$map = get_option( 'vamtam_last_import_map' );

		$posts = get_posts( array(
			'post_type'      => get_post_types(),
			'posts_per_page' => -1,
			'meta_query'     => array(
				'key'     => '_fl_builder_data',
				'compare' => 'EXISTS',
			),
			'orderby' => 'ID',
			'order' => 'ASC',
		) );

		// loop through the Beaver data for all pages and map old post/term IDs to the new ones (after import)
		foreach ( $posts as $post ) {
			$data = get_post_meta( $post->ID, '_fl_builder_data', true );

			if ( ! $data ) {
				$meta = get_post_meta( $post->ID );

				if ( isset( $meta[ '_fl_builder_data' ] ) ) {
					$data = maybe_unserialize( self::fix_serialized( $meta[ '_fl_builder_data' ][0] ) );
				} else {
					echo "missing _fl_builder_data for {$post->ID} {$post->post_type}\n";
					unset( $data );
				}
			}

			if ( isset( $data ) && is_array( $data ) ) {
				foreach ( $data as $node_id => $node ) {
					// only for vamtam-blog and vamtam-projects modules
					if ( $node->type === 'module' && in_array( $node->settings->type, array( 'vamtam-blog', 'vamtam-projects' ), true ) ) {
						$settings = get_object_vars( $node->settings );

						foreach ( $settings as $setting_name => $setting_value ) {
							if ( $setting_name !== 'posts_per_page' ) {
								if ( strpos( $setting_name, 'posts_' ) === 0 && ! empty( $setting_value ) ) {
									$setting_value = explode( ',', $setting_value );
									$new_value     = [];

									foreach ( $setting_value as $post_id ) {
										if ( isset( $map['posts'][ (int)$post_id ] ) ) {
											$new_value[] = $map['posts'][ (int)$post_id ];
										}
									}

									$data[ $node_id ]->settings->{$setting_name} = implode( ',', $new_value );
								} elseif ( strpos( $setting_name, 'tax_' ) === 0 && ! empty( $setting_value ) ) {
									$setting_value = explode( ',', $setting_value );
									$new_value     = [];

									foreach ( $setting_value as &$term_id ) {
										if ( isset( $map['terms'][ (int)$term_id ] ) ) {
											$new_value[] = $map['terms'][ (int)$term_id ];
										}
									}

									$data[ $node_id ]->settings->{$setting_name} = implode( ',', $new_value );
								}
							}
						}
					} else if ( $node->type === 'module' && $node->settings->type === 'widget' && isset( $node->settings->{"widget-nav_menu"} ) ) {
						$from = $node->settings->{'widget-nav_menu'}->nav_menu;

						if ( isset( $map['terms'][ (int) $from ] ) ) {
							$mapped = $map['terms'][ (int) $from ];
							echo "will map menu {$from} to {$mapped}\n";
							$data[ $node_id ]->settings->{"widget-nav_menu"}->nav_menu = $mapped;
						} else {
							echo "no mapped id for nav_menu {$from}\n";
						}
					} else if ( $node->type === 'module' && $node->settings->type === 'vamtam-booked' && isset( $node->settings->calendar ) ) {
						$from = $node->settings->calendar;

						if ( isset( $map['terms'][ (int) $from ] ) ) {
							$mapped = $map['terms'][ (int) $from ];
							echo "will map calendar id {$from} to {$mapped}\n";
							$data[ $node_id ]->settings->calendar = strval( $mapped );
						} else {
							echo "no mapped id for calendar {$from}\n";
						}
					}
				}

				update_post_meta( $post->ID, '_fl_builder_data', $data );
			}
		}

		self::megamenu_import();

		$wpforms = get_option( 'wpforms_settings', [] );
		$wpforms['disable-css'] = 2;
		update_option( 'wpforms_settings', $wpforms );

		self::process_attachments();
	}

	public static function set_menu_locations() {
		$map  = get_option( 'vamtam_last_import_map', false );
		$path = VAMTAM_SAMPLES_DIR . 'theme-mods.json';

		if ( $map && ! get_theme_mod( 'vamtam_force_demo_menu', false ) && file_exists( $path ) ) {
			$theme_mods = json_decode( file_get_contents( $path ), true );

			if ( isset( $theme_mods['nav_menu_locations'] ) ) {
				foreach ( $theme_mods['nav_menu_locations'] as $location => $term_id ) {
					if ( isset( $map['terms'][ (int)$term_id ] ) ) {
						$theme_mods['nav_menu_locations'][ $location ] = $map['terms'][ (int)$term_id ];
					}
				}
			}

			foreach ( $theme_mods as $opt_name => $mod_val ) {
				set_theme_mod( $opt_name, $mod_val );
			}

			set_theme_mod( 'vamtam_force_demo_menu', true );
		}
	}

	public static function generic_option_import( $file, $callback ) {
		$path = VAMTAM_SAMPLES_DIR . $file . '.json';

		if ( file_exists( $path ) ) {
			$settings = json_decode( file_get_contents( $path ), true );

			foreach ( $settings as $opt_name => $opt_val ) {
				update_option( $opt_name, $opt_val );
			}

			call_user_func( $callback );
		}
	}

	public static function megamenu_import() {
		$file = VAMTAM_SAMPLES_DIR . 'megamenu.json';

		if ( file_exists( $file ) ) {
			$exported = json_decode( file_get_contents( $file ), true );

			update_site_option( 'megamenu_themes', $exported['themes'] );
			update_site_option( 'megamenu_settings', $exported['settings'] );

			//update post_meta that mega_menu uses for menu layouts (id remapping)
			$map = get_option( 'vamtam_last_import_map' );

			$posts = get_posts( array(
				'post_type'      => 'nav_menu_item',
				'posts_per_page' => -1,
				'orderby' => 'ID',
				'order' => 'ASC',
			) );
			
			foreach ( $posts as $post ) {
				$data = get_post_meta( $post->ID, '_megamenu', true );

				if ( ! $data ) {
					$meta = get_post_meta( $post->ID );

					if ( isset( $meta[ '_megamenu' ] ) ) {
						$data = maybe_unserialize( self::fix_serialized( $meta[ '_megamenu' ] ) );
					} else {
						echo "missing _megamenu for {$post->ID} {$post->post_type}\n";
						unset( $data );
					}
				}

				if ( isset( $data ) && is_array( $data ) && array_key_exists( 'grid', $data ) ) {
					foreach ( $data['grid'] as $gi_key => $grid_items ) {
						foreach ( $grid_items['columns'] as $c_key => $column ) {
							foreach ( $column['items'] as $ci_key => $column_item ) {
								$item_id = $column_item['id'];
								if ( isset( $map['menus'][ (int)$item_id ] ) ) {
									$column_item['id'] = $map['menus'][ (int)$item_id ]; 
									$data['grid'][ $gi_key ]['columns'][ $c_key ]['items'][ $ci_key ]['id'] = $map['menus'][ (int)$item_id ];
								}
							}
						}
					}
				}
				update_post_meta( $post->ID, '_megamenu', $data );

				do_action( 'megamenu_after_theme_import' );
			}
		}
	}

	public static function jetpack_import() {
		Jetpack::load_modules();

		if ( class_exists( 'Jetpack_Portfolio' ) ) {
			Jetpack_Portfolio::init()->register_post_types();
		}

		if ( class_exists( 'Jetpack_Testimonial' ) ) {
			Jetpack_Testimonial::init()->register_post_types();
		}
	}

	public static function foodpress_import() {
		if ( function_exists( 'foodpress_generate_options_css' ) ) {
			foodpress_generate_options_css();
		}
	}

	public static function tribe_events_import() {
		// no cache to regenerate at this time
	}

	/**
	 * @return string
	 */
	static public function fix_serialized( $src ) {
		if ( empty( $src ) ) {
			return $src;
		}

		$data = maybe_unserialize( $src );

		// return if maybe_unserialize() returns an object or array, this is good.
		if( is_object( $data ) || is_array( $data ) ) {
			return $data;
		}

		$data = preg_replace_callback( '!s:(\d+):([\\\\]?"[\\\\]?"|[\\\\]?"((.*?)[^\\\\])[\\\\]?");!', array( __CLASS__, 'fix_serial_callback' ), $src );

		if ( ! isset( $data ) && strlen( $data ) === 0 ) {
			return $src;
		}

		return $data;
	}

	/**
	 * @return string
	 */
	static public function fix_serial_callback( $matches ) {
		if ( ! isset( $matches[3] ) ) {
			return $matches[0];
		}

		return 's:' . strlen( self::unescape_mysql( $matches[3] ) ) . ':"' . self::unescape_quotes( $matches[3] ) . '";';
	}

	/**
	 * Unescape to avoid dump-text issues.
	 *
	 * @access private
	 * @return string
	 */
	static private function unescape_mysql( $value ) {
		return str_replace( array( "\\\\", "\\0", "\\n", "\\r", "\Z", "\'", '\"' ),
			array( "\\",   "\0",  "\n",  "\r",  "\x1a", "'", '"' ),
		$value );
	}

	/**
	 * Fix strange behaviour if you have escaped quotes in your replacement.
	 *
	 * @access private
	 * @return string
	 */
	static private function unescape_quotes( $value ) {
		return str_replace( '\"', '"', $value );
	}
}

new Vamtam_Importers;
