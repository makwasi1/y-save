<?php

class VamtamTeamMember extends FLBuilderModule {

	public function __construct() {
		$path = trailingslashit( 'modules/' . basename( dirname( __FILE__ ) ) );

		parent::__construct(array(
			'name'            => __( 'Team Member', 'vamtam-elements-b' ),
			'description'     => '',
			'category'        => __( 'VamTam Modules', 'vamtam-elements-b' ),
			'partial_refresh' => true,
			'dir'             => VAMTAMEL_B_DIR . $path,
			'url'             => VAMTAMEL_B_URL . $path,
		));
	}
}

FLBuilder::register_module( 'VamtamTeamMember', array(
	'vamtam-team-member-tab-basic' => array(
		'title'    => __( 'Basic', 'vamtam-elements-b' ),
		'sections' => array(
			'vamtam-team-member-section-main' => array(
				'title'  => __( 'Main', 'vamtam-elements-b' ),
				'fields' => array(
					'name' => array(
						'label'       => esc_html__( 'Name', 'wpv' ),
						'type'        => 'text',
						'holder'      => 'h5',
						'vamtam-wpml' => 'line',
					),
					'position' => array(
						'label'       => esc_html__( 'Position', 'wpv' ),
						'type'        => 'text',
						'vamtam-wpml' => 'line',
					),
					'url' => array(
						'label' => esc_html__( 'Link', 'wpv' ),
						'type'  => 'link',
					),
					'email' => array(
						'label'       => esc_html__( 'Email', 'wpv' ),
						'type'        => 'text',
						'vamtam-wpml' => 'line',
					),
					'phone' => array(
						'label'       => esc_html__( 'Phone', 'wpv' ),
						'type'        => 'text',
						'vamtam-wpml' => 'line',
					),
					'picture' => array(
						'label' => esc_html__( 'Picture', 'wpv' ),
						'type'  => 'photo',
					),

					'biography' => array(
						'label'   => esc_html__( 'Biography', 'wpv' ),
						'default' => esc_html__( 'Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit.', 'wpv' ),
						'type'    => 'editor',
					),
				),
			),
		),
	),
	'vamtam-team-member-tab-social' => array(
		'title'    => __( 'Social', 'vamtam-elements-b' ),
		'sections' => array(
			'vamtam-team-member-section-social' => array(
				'title'  => __( 'Social', 'vamtam-elements-b' ),
				'fields' => array(
					'googleplus' => array(
						'label' => esc_html__( 'Google+', 'wpv' ),
						'type'  => 'link',
					),

					'linkedin' => array(
						'label'   => esc_html__( 'LinkedIn', 'wpv' ),
						'default' => '',
						'type'    => 'link',
					),

					'facebook' => array(
						'label' => esc_html__( 'Facebook', 'wpv' ),
						'type'  => 'link',
					),

					'twitter' => array(
						'label' => esc_html__( 'Twitter', 'wpv' ),
						'type'  => 'link',
					),

					'youtube' => array(
						'label' => esc_html__( 'YouTube', 'wpv' ),
						'type'  => 'link',
					),

					'pinterest' => array(
						'label' => esc_html__( 'Pinterest', 'wpv' ),
						'type'  => 'link',
					),

					'lastfm' => array(
						'label' => esc_html__( 'LastFM', 'wpv' ),
						'type'  => 'link',
					),

					'instagram' => array(
						'label' => esc_html__( 'Instagram', 'wpv' ),
						'type'  => 'link',
					),

					'dribble' => array(
						'label' => esc_html__( 'Dribble', 'wpv' ),
						'type'  => 'link',
					),

					'vimeo' => array(
						'label' => esc_html__( 'Vimeo', 'wpv' ),
						'type'  => 'link',
					),
				),
			),
		),
	),
) );
