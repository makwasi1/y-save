<?php

/**
 * @class VamtamSeparatorModule
 */
class VamtamSeparatorModule extends FLBuilderModule {

	/**
	 * @method __construct
	 */
	public function __construct() {
		$path = trailingslashit( 'modules/' . basename( dirname( __FILE__ ) ) );

		parent::__construct(array(
			'name'            => __( 'Separator', 'vamtam-elements-b' ),
			'description'     => __( 'A divider line to separate content.', 'vamtam-elements-b' ),
			'category'        => __( 'VamTam Modules', 'vamtam-elements-b' ),
			'editor_export'   => false,
			'partial_refresh' => true,
			'dir'             => VAMTAMEL_B_DIR . $path,
			'url'             => VAMTAMEL_B_URL . $path,
		));
	}
}

/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('VamtamSeparatorModule', array(
	'general' => array( // Tab
		'title'    => __( 'General', 'vamtam-elements-b' ), // Tab title
		'sections' => array( // Tab Sections
			'general' => array( // Section
				'title'  => '', // Section Title
				'fields' => array( // Section Fields
					'color' => array(
						'type'    => 'vamtam-color',
						'label'   => __( 'Color', 'vamtam-elements-b' ),
						'default' => 'accent4',
					),
					'opacity' => array(
						'type'    => 'unit',
						'label'   => __( 'Opacity', 'vamtam-elements-b' ),
						'default' => '1',
						'min'     => 0,
						'max'     => 1,
						'step'    => 0.05,
					),
					'height' => array(
						'type'        => 'text',
						'label'       => __( 'Height', 'vamtam-elements-b' ),
						'default'     => '1',
						'maxlength'   => '2',
						'size'        => '3',
						'description' => 'px',
						'preview'     => array(
							'type'     => 'css',
							'selector' => '.fl-separator',
							'property' => 'border-top-width',
							'unit'     => 'px',
						),
					),
					'width' => array(
						'type'    => 'select',
						'label'   => __( 'Width', 'vamtam-elements-b' ),
						'default' => 'full',
						'options' => array(
							'full'   => __( 'Full Width', 'vamtam-elements-b' ),
							'custom' => __( 'Custom', 'vamtam-elements-b' ),
						),
						'toggle' => array(
							'full'   => array(),
							'custom' => array(
								'fields' => array( 'align', 'custom_width' ),
							),
						),
					),
					'custom_width' => array(
						'type'        => 'text',
						'label'       => __( 'Custom Width', 'vamtam-elements-b' ),
						'default'     => '10',
						'maxlength'   => '3',
						'size'        => '4',
						'description' => '%',
					),
					'align' => array(
						'type'    => 'select',
						'label'   => __( 'Align', 'vamtam-elements-b' ),
						'default' => 'center',
						'options' => array(
							'center' => _x( 'Center', 'Alignment.', 'vamtam-elements-b' ),
							'left'   => _x( 'Left', 'Alignment.', 'vamtam-elements-b' ),
							'right'  => _x( 'Right', 'Alignment.', 'vamtam-elements-b' ),
						),
					),
					'style' => array(
						'type'    => 'select',
						'label'   => __( 'Style', 'vamtam-elements-b' ),
						'default' => 'solid',
						'options' => array(
							'solid'  => _x( 'Solid', 'Border type.', 'vamtam-elements-b' ),
							'dashed' => _x( 'Dashed', 'Border type.', 'vamtam-elements-b' ),
							'dotted' => _x( 'Dotted', 'Border type.', 'vamtam-elements-b' ),
							'double' => _x( 'Double', 'Border type.', 'vamtam-elements-b' ),
						),
						'preview' => array(
							'type'     => 'css',
							'selector' => '.fl-separator',
							'property' => 'border-top-style',
						),
						'help' => __( 'The type of border to use. Double borders must have a height of at least 3px to render properly.', 'vamtam-elements-b' ),
					),
				),
			),
		),
	),
));

