<?php

FLBuilderModel::default_settings( $settings, array(
	'post_type'      => 'jetpack-testimonial',
	'order'          => 'ASC',
	'offset'         => 0,
	'posts_per_page' => -1,
) );

$q = FLBuilderLoop::query( $settings );

if ( $settings->layout_type === 'slider' ) {
	if ( class_exists( 'VamtamTemplates') && VamtamTemplates::early_cube_load() ) {
		wp_enqueue_style( 'cubeportfolio' );
		wp_enqueue_script( 'cubeportfolio' );
	}

	$slider_options = array(
		'layoutMode'       => 'slider',
		'drag'             => true,
		'auto'             => vamtam_sanitize_bool( $settings->autorotate ),
		'autoTimeout'      => apply_filters( 'vamtam_testimonials_slider_delay', 5000 ),
		'autoPauseOnHover' => true,
		'showNavigation'   => false,
		'showPagination'   => true,
		'rewindNav'        => true,
		'scrollByPage'     => false,
		'gridAdjustment'   => 'responsive',
		'mediaQueries' => array(
			array(
				'width' => 1,
				'cols'  => 1,
			),
		),
		'gapHorizontal' => 0,
		'gapVertical'   => 0,
		'caption'       => '',
		'displayType'   => 'default',
	);

	echo '<div class="vamtam-cubeportfolio cbp cbp-slider-edge vamtam-testimonials-slider vamtam-tst-align-' . esc_attr( $settings->alignment ) . '" data-options="' . esc_attr( json_encode( $slider_options ) ) . '">';

	while ( $q->have_posts() ) {
		$q->the_post();

		echo '<div class="cbp-item">';

		include locate_template( 'templates/beaver/vamtam-testimonials.php' );

		echo '</div>';
	}

	echo '</div>';
} else {
	echo '<div class="blockquote-list vamtam-tst-align-' . esc_attr( $settings->alignment ) . '">';

	while ( $q->have_posts() ) {
		$q->the_post();

		include locate_template( 'templates/beaver/vamtam-testimonials.php' );
	}

	echo '</div>';

	if ( vamtam_sanitize_bool( $settings->pagination ) ) {
		VamtamTemplates::pagination( 'paged', true, array(), $q );
	}
}

wp_reset_postdata();
