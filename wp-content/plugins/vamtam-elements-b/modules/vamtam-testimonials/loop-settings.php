<?php

FLBuilderModel::default_settings($settings, array(
	'post_type'      => 'jetpack-testimonial',
	'order_by'       => 'menu_order',
	'order'          => 'ASC',
	'offset'         => 0,
	'users'          => '',
	'posts_per_page' => -1,
));

?>
<div id="fl-builder-settings-section-general" class="fl-loop-builder fl-builder-settings-section">

	<table class="fl-form-table">
	<?php

	// Order by
	FLBuilder::render_settings_field('posts_per_page', array(
		'type'    => 'text',
		'label'   => __( 'Per Page', 'fl-builder' ),
		'default' => -1,
		'min'     => -1,
	), $settings);

	// Order by
	FLBuilder::render_settings_field('order_by', array(
		'type'    => 'select',
		'label'   => __( 'Order By', 'fl-builder' ),
		'options' => array(
			'ID'            => __( 'ID', 'fl-builder' ),
			'date'          => __( 'Date', 'fl-builder' ),
			'modified'      => __( 'Date Last Modified', 'fl-builder' ),
			'title'         => __( 'Title', 'fl-builder' ),
			'author'        => __( 'Author', 'fl-builder' ),
			'comment_count' => __( 'Comment Count', 'fl-builder' ),
			'menu_order'    => __( 'Menu Order', 'fl-builder' ),
			'rand'          => __( 'Random', 'fl-builder' ),
		),
	), $settings);

	// Order
	FLBuilder::render_settings_field('order', array(
		'type'    => 'select',
		'label'   => __( 'Order', 'fl-builder' ),
		'options' => array(
			'DESC' => __( 'Descending', 'fl-builder' ),
			'ASC'  => __( 'Ascending', 'fl-builder' ),
		),
	), $settings);

	// Offset
	FLBuilder::render_settings_field('offset', array(
		'type'    => 'text',
		'label'   => _x( 'Offset', 'How many posts to skip.', 'fl-builder' ),
		'default' => '0',
		'size'    => '4',
		'help'    => __( 'Skip this many posts that match the specified criteria.', 'fl-builder' ),
	), $settings);

	?>
	</table>
</div>
<div id="fl-builder-settings-section-filter" class="fl-builder-settings-section">
	<h3 class="fl-builder-settings-title"><?php esc_html_e( 'Filter', 'fl-builder' ); ?></h3>
	<?php
		$slug = 'jetpack-testimonial';
		$type = get_post_type_object( $slug );
	?>
	<table class="fl-form-table fl-loop-builder-filter fl-loop-builder-<?php echo esc_attr( $slug ) ?>-filter" <?php if ( $slug == $settings->post_type ) echo 'style="display:table;"'; ?>>
	<?php

	// Posts
	FLBuilder::render_settings_field('posts_' . $slug, array(
		'type'   => 'suggest',
		'action' => 'fl_as_posts',
		'data'   => $slug,
		'label'  => $type->label,
		'help'   => sprintf( __( 'Enter a list of %1$s. Only these %2$s will be shown.', 'fl-builder' ), $type->label, $type->label ),
	), $settings);

	// Taxonomies
	$taxonomies = FLBuilderLoop::taxonomies( $slug );

	foreach ( $taxonomies as $tax_slug => $tax ) {

		FLBuilder::render_settings_field('tax_' . $slug . '_' . $tax_slug, array(
			'type'   => 'suggest',
			'action' => 'fl_as_terms',
			'data'   => $tax_slug,
			'label'  => $tax->label,
			'help'   => sprintf( __( 'Enter a list of %1$s. Only posts with these %2$s will be shown.', 'fl-builder' ), $tax->label, $tax->label ),
		), $settings);
	}

	?>
	</table>
	<table class="fl-form-table">
	<?php

	// Author
	FLBuilder::render_settings_field('users', array(
		'type'   => 'suggest',
		'action' => 'fl_as_users',
		'label'  => __( 'Authors', 'fl-builder' ),
		'help'   => __( 'Enter a list of authors usernames. Only posts with these authors will be shown.', 'fl-builder' ),
	), $settings);

	?>
	</table>
</div>
