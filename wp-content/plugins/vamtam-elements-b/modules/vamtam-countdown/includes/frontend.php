<?php

	$counters = array(
		'days' => array(
			'singular' => __( 'Day', 'vamtam-elements-b' ),
			'plural'   => __( 'Days', 'vamtam-elements-b' ),
		),
		'hours' => array(
			'singular' => __( 'Hour', 'vamtam-elements-b' ),
			'plural'   => __( 'Hours', 'vamtam-elements-b' ),
		),
		'minutes' => array(
			'singular' => __( 'Minute', 'vamtam-elements-b' ),
			'plural'   => __( 'Minutes', 'vamtam-elements-b' ),
		),
		'seconds' => array(
			'singular' => __( 'Second', 'vamtam-elements-b' ),
			'plural'   => __( 'Seconds', 'vamtam-elements-b' ),
		),
	);

	$unit_number_style = '';

	if ( ! empty( $settings->number_color ) ) {
		$unit_number_style .= 'color:' . vamtam_el_sanitize_accent( $settings->number_color ) . ';';
	}

	if ( ! empty( $settings->number_size ) ) {
		$unit_number_style .= 'font-size:' . vamtam_el_sanitize_accent( $settings->number_size ) . 'px;';
	}

?>

<div class="fl-countdown<?php if ( $settings->layout == 'default' && $settings->show_separator == 'yes' && isset( $settings->separator_type ) ) echo ' fl-countdown-separator-' . esc_attr( $settings->separator_type ) ?>">

	<div>
		<?php foreach ( $counters as $class => $label ) :  ?>
			<div class="fl-countdown-number fl-countdown-<?php echo esc_attr( $class ) ?>">
				<div class="fl-countdown-unit">
					<span class="fl-countdown-unit-number" style="<?php echo esc_attr( $unit_number_style ) ?>"></span>
					<div
						class="fl-countdown-unit-label"
						data-label='<?php echo json_encode( $label ); ?>'
						style="color:<?php echo esc_attr( vamtam_el_sanitize_accent( $settings->label_color ) ) ?>"
					><?php echo $label['singular']; // xss ok ?></div>
				</div>
				<?php if ( $settings->layout == 'circle' ) : ?>
					<div class="fl-countdown-circle-container">
						<?php $module->render_circle(); ?>
					</div>
				<?php endif; ?>
			</div>
			<?php if ( $class === 'hours') : ?>
				</div><div>
			<?php endif ?>
		<?php endforeach; ?>
	</div>
</div>
