<?php

// Opening Wrapper
echo '<div class="fl-woocommerce-' . esc_attr( $settings->layout ) . '">';

// Shortcodes
$pages = array(
	'cart'     => '[woocommerce_cart]',
	'checkout' => '[woocommerce_checkout]',
	'tracking' => '[woocommerce_order_tracking]',
	'account'  => '[woocommerce_my_account]',
);

// WooCommerce Pages
if ( isset( $pages[ $settings->layout ] ) ) {
	echo $pages[ $settings->layout ]; // xss ok
}
elseif ( $settings->layout == 'product' ) {
	add_filter( 'post_class', array( $module, 'single_product_post_class' ) );
	echo '[product id="' . esc_attr( $settings->product_id ) . '" columns="1"]';
	remove_filter( 'post_class', array( $module, 'single_product_post_class' ) );
} // Single Product Page
elseif ( $settings->layout == 'product_page' ) {
	add_filter( 'post_class', array( $module, 'single_product_post_class' ) );
	echo '[product_page id="' . esc_attr( $settings->product_id ) . '"]';
	remove_filter( 'post_class', array( $module, 'single_product_post_class' ) );
} // Add to Cart Button
elseif ( $settings->layout == 'add-cart' ) {
	echo '[add_to_cart id="' . esc_attr( $settings->product_id ) . '" style=""]';
} // Categories
elseif ( $settings->layout == 'categories' ) {
	echo '[product_categories parent="' . esc_attr( $settings->parent_cat_id ) . '" columns="' . esc_attr( $settings->cat_columns ) . '"]';
} // Multiple Products
elseif ( $settings->layout == 'products' ) {
	add_filter( 'post_class', array( $module, 'products_post_class' ) );

	$query_args = array(
		'post_type'           => 'product',
		'post_status'         => 'publish',
		'ignore_sticky_posts' => 1,
		'posts_per_page'      => $settings->num_products,
		'orderby'             => $settings->orderby,
		'order'               => $settings->order,
		'meta_query'          => WC()->query->get_meta_query(),
		'tax_query'           => WC()->query->get_tax_query(),
	);

	if ( $settings->products_source == 'ids' ) {
		$query_args['post__in'] = explode( ',', $settings->product_ids );
	} elseif ( $settings->products_source == 'category' ) {
		$query_args['tax_query'][] = array(
			array(
				'taxonomy' => 'product_cat',
				'terms'    => array_map( 'sanitize_title', explode( ',', $settings->category_slug ) ),
				'field'    => 'slug',
				'operator' => 'IN',
			),
		);
	} elseif ( $settings->products_source == 'featured' ) {
		$query_args['tax_query'][] = array(
			'taxonomy' => 'product_visibility',
			'field'    => 'name',
			'terms'    => 'featured',
			'operator' => 'IN',
		);
	} elseif ( $settings->products_source == 'sale' ) {
		$query_args['post__in'] = array_merge( array( 0 ), wc_get_product_ids_on_sale() );
	} elseif ( $settings->products_source == 'best-selling' ) {
		$query_args['meta_key'] = 'total_sales';
		$query_args['orderby']  = 'meta_value_num';
	} elseif ( $settings->products_source == 'top-rated' ) {
		add_filter( 'posts_clauses', array( 'WC_Shortcodes', 'order_by_rating_post_clauses' ) );
	}

	$products = new WP_Query( $query_args );

	$columns = intval( $settings->columns );

	$max_columns = $columns;

	if ( 0 === $columns ) {
		$columns = 4; // this is used for thumbnails only
	}

	$woocommerce_loop['columns'] = $columns;

	include locate_template( array( 'templates/woocommerce-scrollable/loop.php' ) );

	$products->reset_postdata();
	wp_reset_postdata();

	remove_filter( 'posts_clauses', array( 'WC_Shortcodes', 'order_by_rating_post_clauses' ) );
	remove_filter( 'post_class', array( $module, 'products_post_class' ) );
}

// Closing Wrapper
echo '</div>';
