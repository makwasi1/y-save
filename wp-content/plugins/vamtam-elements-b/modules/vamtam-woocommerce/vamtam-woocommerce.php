<?php

/**
 * @class VamtamWooCommerceModule
 */
class VamtamWooCommerceModule extends FLBuilderModule {

	/**
	 * @method __construct
	 */
	public function __construct() {
		$path = trailingslashit( 'modules/' . basename( dirname( __FILE__ ) ) );

		parent::__construct(array(
			'name'            => esc_html__( 'WooCommerce', 'vamtam-elements-b' ),
			'description'     => esc_html__( 'Display products or categories from your WooCommerce store.', 'vamtam-elements-b' ),
			'category'        => esc_html__( 'VamTam Modules', 'vamtam-elements-b' ),
			'enabled'         => class_exists( 'Woocommerce' ),
			'partial_refresh' => true,
			'dir'             => VAMTAMEL_B_DIR . $path,
			'url'             => VAMTAMEL_B_URL . $path,
		));
	}

	/**
	 * @method products_post_class
	 */
	public function products_post_class( $classes ) {

		$classes[] = 'product';

		return $classes;
	}

	/**
	 * @method single_product_post_class
	 */
	public function single_product_post_class( $classes ) {

		$classes[] = 'product';
		$classes[] = 'single-product';

		return $classes;
	}
}

/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('VamtamWooCommerceModule', array(
	'general' => array(
		'title'    => esc_html__( 'General', 'vamtam-elements-b' ),
		'sections' => array(
			'general' => array(
				'title'  => '',
				'fields' => array(
					'layout' => array(
						'type'    => 'select',
						'label'   => esc_html__( 'Layout', 'vamtam-elements-b' ),
						'default' => '',
						'options' => array(
							''                => esc_html__( 'Choose...', 'vamtam-elements-b' ),
							'product'         => esc_html__( 'Single Product', 'vamtam-elements-b' ),
							'product_page'    => esc_html__( 'Product Page', 'vamtam-elements-b' ),
							'products'        => esc_html__( 'Multiple Products', 'vamtam-elements-b' ),
							'add-cart'        => esc_html__( '"Add to Cart" Button', 'vamtam-elements-b' ),
							'categories'      => esc_html__( 'Categories', 'vamtam-elements-b' ),
							'cart'            => esc_html__( 'Cart', 'vamtam-elements-b' ),
							'checkout'        => esc_html__( 'Checkout', 'vamtam-elements-b' ),
							'tracking'        => esc_html__( 'Order Tracking', 'vamtam-elements-b' ),
							'account'         => esc_html__( 'My Account', 'vamtam-elements-b' ),
						),
						'toggle' => array(
							'product' => array(
								'fields' => array( 'product_id' ),
							),
							'product_page' => array(
								'fields' => array( 'product_id' ),
							),
							'products' => array(
								'sections' => array( 'multiple_products' ),
							),
							'add-cart' => array(
								'fields' => array( 'product_id' ),
							),
							'categories' => array(
								'fields' => array( 'parent_cat_id', 'cat_columns' ),
							),
						),
					),
					'product_id' => array(
						'type'    => 'text',
						'label'   => esc_html__( 'Product ID', 'vamtam-elements-b' ),
						'default' => '',
						'size'    => '4',
						'help'    => esc_html__( 'As you add products in the WooCommerce Products area, each will be assigned a unique ID. You can find this unique product ID by visiting the Products area and rolling over the product. The unique ID will be the first attribute.', 'vamtam-elements-b' ),
					),
					'parent_cat_id' => array(
						'type'    => 'text',
						'label'   => esc_html__( 'Parent Category ID', 'vamtam-elements-b' ),
						'default' => '0',
						'size'    => '4',
						'help'    => esc_html__( 'As you add product categories in the WooCommerce Products area, each will be assigned a unique ID. This ID can be found by hovering on the category in the categories area under Products and looking in the URL that is displayed in your browser. The ID will be the only number value in the URL.', 'vamtam-elements-b' ),
					),
					'cat_columns' => array(
						'type'    => 'select',
						'label'   => esc_html__( 'Columns', 'vamtam-elements-b' ),
						'default' => '4',
						'options' => array(
							'1' => '1',
							'2' => '2',
							'3' => '3',
							'4' => '4',
						),
					),
				),
			),
			'multiple_products' => array(
				'title'  => esc_html__( 'Multiple Products', 'vamtam-elements-b' ),
				'fields' => array(
					'products_source' => array(
						'type'    => 'select',
						'label'   => esc_html__( 'Products Source', 'vamtam-elements-b' ),
						'default' => 'ids',
						'options' => array(
							'ids'          => esc_html__( 'Products IDs', 'vamtam-elements-b' ),
							'category'     => esc_html__( 'Product Category', 'vamtam-elements-b' ),
							'featured'     => esc_html__( 'Featured Products', 'vamtam-elements-b' ),
							'sale'         => esc_html__( 'Sale Products', 'vamtam-elements-b' ),
							'best-selling' => esc_html__( 'Best Selling Products', 'vamtam-elements-b' ),
							'top-rated'    => esc_html__( 'Top Rated Products', 'vamtam-elements-b' ),
						),
						'toggle' => array(
							'ids' => array(
								'fields' => array( 'product_ids', 'columns', 'orderby', 'order' ),
							),
							'category' => array(
								'fields' => array( 'category_slug', 'num_products', 'columns', 'orderby', 'order' ),
							),
							'featured' => array(
								'fields' => array( 'num_products', 'columns', 'orderby', 'order' ),
							),
							'sale' => array(
								'fields' => array( 'num_products', 'columns', 'orderby', 'order' ),
							),
							'best-selling' => array(
								'fields' => array( 'num_products', 'columns' ),
							),
							'top-rated' => array(
								'fields' => array( 'num_products', 'columns', 'orderby', 'order' ),
							),
						),
					),
					'product_ids' => array(
						'type'    => 'text',
						'label'   => esc_html__( 'Product IDs', 'vamtam-elements-b' ),
						'default' => '',
						'help'    => esc_html__( 'As you add products in the WooCommerce Products area, each will be assigned a unique ID. You can find this unique product ID by visiting the Products area and rolling over the product. The unique ID will be the first attribute and you can add several here separated by a comma.', 'vamtam-elements-b' ),
					),
					'category_slug' => array(
						'type'    => 'text',
						'label'   => esc_html__( 'Category Slug', 'vamtam-elements-b' ),
						'default' => '',
						'help'    => esc_html__( 'As you add product categories in the WooCommerce Products area, each will be assigned a unique slug or you can edit and add your own. These slugs can be found in the Categories area under WooCommerce Products. Several can be added here separated by a comma.', 'vamtam-elements-b' ),
					),
					'num_products' => array(
						'type'    => 'text',
						'label'   => esc_html__( 'Number of Products', 'vamtam-elements-b' ),
						'default' => '12',
						'size'    => '4',
					),
					'columns' => array(
						'type'    => 'select',
						'label'   => esc_html__( 'Columns', 'vamtam-elements-b' ),
						'default' => '4',
						'options' => array(
							'0' => '0',
							'1' => '1',
							'2' => '2',
							'3' => '3',
							'4' => '4',
						),
					),
					'orderby' => array(
						'type'    => 'select',
						'label'   => esc_html__( 'Sort By', 'vamtam-elements-b' ),
						'default' => 'menu_order',
						'options' => array(
							'menu_order' => _x( 'Default', 'Sort by.', 'vamtam-elements-b' ),
							'popularity' => esc_html__( 'Popularity', 'vamtam-elements-b' ),
							'rating'     => esc_html__( 'Rating', 'vamtam-elements-b' ),
							'date'       => esc_html__( 'Date', 'vamtam-elements-b' ),
							'price'      => esc_html__( 'Price', 'vamtam-elements-b' ),
						),
					),
					'order' => array(
						'type'    => 'select',
						'label'   => esc_html__( 'Sort Direction', 'vamtam-elements-b' ),
						'default' => 'menu_order',
						'options' => array(
							'ASC'  => esc_html__( 'Ascending', 'vamtam-elements-b' ),
							'DESC' => esc_html__( 'Descending', 'vamtam-elements-b' ),
						),
					),
				),
			),
		),
	),
));
