<?php if ( ! empty( $settings->icon_size ) ) : ?>
	.fl-builder-content .fl-node-<?php echo esc_html( $id ); ?> .fl-bwl-icon,
	.fl-builder-content .fl-node-<?php echo esc_html( $id ); ?> .fl-bwl-hover-icon {
		font-size: <?php echo $settings->icon_size; ?>px;
	}
<?php endif; ?>

<?php if ( ! empty( $settings->icon_color ) ) : ?>
	.fl-builder-content .fl-node-<?php echo esc_html( $id ); ?> .fl-bwl-icon {
		color: <?php echo esc_html( vamtam_sanitize_accent( $settings->icon_color ) ); ?>;
	}
<?php endif; ?>

<?php if ( ! empty( $settings->hover_icon_color ) ) : ?>
	.fl-builder-content .fl-node-<?php echo esc_html( $id ); ?> .fl-bwl-hover-icon {
		color: <?php echo esc_html( vamtam_sanitize_accent( $settings->hover_icon_color ) ); ?>;
	}
<?php endif; ?>

<?php if ( ! empty( $settings->title_color ) ) : ?>
	.fl-builder-content .fl-node-<?php echo esc_html( $id ); ?> .fl-bwl-content > .fl-bwl-title {
		color: <?php echo esc_html( vamtam_sanitize_accent( $settings->title_color ) ); ?>;
	}
<?php endif; ?>

<?php if ( ! empty( $settings->title_color_hover ) ) : ?>
	.fl-builder-content .fl-node-<?php echo esc_html( $id ); ?> .fl-bwl-wrap:hover .fl-bwl-content > .fl-bwl-title {
		color: <?php echo esc_html( vamtam_sanitize_accent( $settings->title_color_hover ) ); ?>;
	}
<?php endif; ?>

<?php if ( ! empty( $settings->text_color ) ) : ?>
	.fl-builder-content .fl-node-<?php echo esc_html( $id ); ?> .fl-bwl-content > .fl-bwl-text {
		color: <?php echo esc_html( vamtam_sanitize_accent( $settings->text_color ) ); ?>;
	}
<?php endif; ?>

<?php if ( ! empty( $settings->text_color_hover ) ) : ?>
	.fl-builder-content .fl-node-<?php echo esc_html( $id ); ?> .fl-bwl-wrap:hover .fl-bwl-content > .fl-bwl-text {
		color: <?php echo esc_html( vamtam_sanitize_accent( $settings->text_color_hover ) ); ?>;
	}
<?php endif; ?>

<?php if ( ! empty( $settings->bg_color_hover ) ) : ?>
	.fl-builder-content .fl-node-<?php echo esc_html( $id ); ?> .fl-bwl-wrap:hover {
		background-color: <?php echo esc_html( vamtam_sanitize_accent( $settings->bg_color_hover ) ); ?>;
	}
<?php endif; ?>

<?php
// Padding - Desktop
FLBuilderCSS::rule( array(
	'selector' => ".fl-node-$id .fl-bwl-wrap",
	'props'    => array()
		+ ( ( $settings->padding_top !== '' ) ? array( 'padding-top' => $settings->padding_top . $settings->padding_unit ) : array() )
		+ ( ( $settings->padding_right !== '' ) ? array( 'padding-right' => $settings->padding_right . $settings->padding_unit ) : array() )
		+ ( ( $settings->padding_bottom !== '' ) ? array( 'padding-bottom' => $settings->padding_bottom . $settings->padding_unit ) : array() )
		+ ( ( $settings->padding_left !== '' ) ? array( 'padding-left' => $settings->padding_left . $settings->padding_unit ) : array() )
	)
);

// Padding - Medium
FLBuilderCSS::rule( array(
	'media'    => 'medium',
	'selector' => ".fl-node-$id .fl-bwl-wrap",
	'props'    => array()
		+ ( ( $settings->padding_top_medium !== '' ) ? array( 'padding-top' => $settings->padding_top_medium . $settings->padding_medium_unit ) : array() )
		+ ( ( $settings->padding_right_medium !== '' ) ? array( 'padding-right' => $settings->padding_right_medium . $settings->padding_medium_unit ) : array() )
		+ ( ( $settings->padding_bottom_medium !== '' ) ? array( 'padding-bottom' => $settings->padding_bottom_medium . $settings->padding_medium_unit ) : array() )
		+ ( ( $settings->padding_left_medium !== '' ) ? array( 'padding-left' => $settings->padding_left_medium . $settings->padding_medium_unit ) : array() )
	)
);

// Padding - Responsive
FLBuilderCSS::rule( array(
	'media'    => 'responsive',
	'selector' => ".fl-node-$id .fl-bwl-wrap",
	'props'    => array()
		+ ( ( $settings->padding_top_responsive !== '' ) ? array( 'padding-top' => $settings->padding_top_responsive . $settings->padding_responsive_unit ) : array() )
		+ ( ( $settings->padding_right_responsive !== '' ) ? array( 'padding-right' => $settings->padding_right_responsive . $settings->padding_responsive_unit ) : array() )
		+ ( ( $settings->padding_bottom_responsive !== '' ) ? array( 'padding-bottom' => $settings->padding_bottom_responsive . $settings->padding_responsive_unit ) : array() )
		+ ( ( $settings->padding_left_responsive !== '' ) ? array( 'padding-left' => $settings->padding_left_responsive . $settings->padding_responsive_unit ) : array() )
	)
);

// Typography
FLBuilderCSS::typography_field_rule( array(
	'settings'	=> $settings,
	'setting_name' 	=> 'title_typography',
	'selector' 	=> ".fl-node-$id .fl-bwl-title",
) );

// Min Height
FLBuilderCSS::responsive_rule( array(
	'settings'	=> $settings,
	'setting_name'	=> 'height',
	'selector'	=> ".fl-node-$id .fl-bwl-wrap",
	'prop'		=> 'min-height',
) );

