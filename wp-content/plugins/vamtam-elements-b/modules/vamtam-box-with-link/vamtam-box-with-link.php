<?php

/**
 * @class VamtamBoxWithLink
 */
class VamtamBoxWithLink extends FLBuilderModule {

	/**
	 * @method __construct
	 */
	public function __construct() {
		$path = trailingslashit( 'modules/' . basename( dirname( __FILE__ ) ) );

		parent::__construct(array(
			'name'            => __( 'Box with Link', 'vamtam-elements-b' ),
			'description'     => __( 'Display a link in a box with an optional image.', 'vamtam-elements-b' ),
			'category'        => __( 'VamTam Modules', 'vamtam-elements-b' ),
			'partial_refresh' => true,
			'dir'             => VAMTAMEL_B_DIR . $path,
			'url'             => VAMTAMEL_B_URL . $path,
			'enabled'         => current_theme_supports( 'vamtam-expand-scroll' ),
		));
	}
}

/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('VamtamBoxWithLink', array(
	'general' => array(
		'title'    => __( 'General', 'vamtam-elements-b' ),
		'sections' => array(
			'content' => array(
				'title'  => 'Content',
				'fields' => array(
					'title' => array(
						'type'        => 'text',
						'label'       => __( 'Title', 'vamtam-elements-b' ),
						'default'     => __( 'Economy And Jobs', 'vamtam-elements-b' ),
						'preview'     => array(
							'type'     => 'text',
							'selector' => '.fl-bwl-title',
						),
					),
					'icon' => array(
						'type'        => 'icon',
						'label'       => __( 'Icon', 'vamtam-elements-b' ),
						'show_remove' => true,
						'default'     => 'fas fa-suitcase',
					),
					'hover_icon' => array(
						'type'        => 'icon',
						'label'       => __( 'Hover Icon', 'vamtam-elements-b' ),
						'show_remove' => true,
						'default'     => 'vamtam-theme-arrow-right-sample',
						'help'        => __( 'While at the editor, the hover icon will be visible when the module is selected and hovered.', 'vamtam-elements-b' )
					),
					'text' => array(
						'type'        => 'text',
						'label'       => __( 'Text', 'vamtam-elements-b' ),
						'default'     => __( 'President Trump jump-started America’s economy into record growth, which created jobs and increased take-home pay for working Americans.', 'vamtam-elements-b' ),
						'preview'     => array(
							'type'     => 'text',
							'selector' => '.fl-bwl-text',
						),
					),
				),
			),
			'link' => array(
				'title'  => 'Link',
				'fields' => array(
					'link' => array(
						'type'    => 'link',
						'label'   => __( 'Link', 'vamtam-elements-b' ),
						'preview' => array(
							'type' => 'none',
						),
					),
					'link_target' => array(
						'type'    => 'select',
						'label'   => __( 'Link Target', 'vamtam-elements-b' ),
						'default' => '_self',
						'options' => array(
							'_self'  => __( 'Same Window', 'vamtam-elements-b' ),
							'_blank' => __( 'New Window', 'vamtam-elements-b' ),
						),
						'preview' => array(
							'type' => 'none',
						),
					),
				),
			),
		),
	),
	'style' => array(
		'title'    => __( 'Style', 'vamtam-elements-b' ),
		'sections' => array(
			'icon' => array( // Section
				'title'  => __( 'Icon', 'vamtam-elements-b' ), // Section Title
				'fields' => array( // Section Fields
					'icon_size' => array(
						'type'        => 'text',
						'label'       => __( 'Icon Size', 'vamtam-elements-b' ),
						'default'     => '60',
						'maxlength'   => '3',
						'size'        => '4',
						'description' => 'px',
					),
					'icon_color' => array(
						'type'       => 'vamtam-color',
						'label'      => __( 'Icon Color', 'vamtam-elements-b' ),
						'default'    => '#982929',
					),
					'hover_icon_color' => array(
						'type'       => 'vamtam-color',
						'label'      => __( 'Hover Icon Color', 'vamtam-elements-b' ),
						'default'    => '#ffffff',
					),
				),
			),
			'colors' => array(
				'title'  => __( 'Colors', 'vamtam-elements-b' ),
				'fields' => array(
					'title_color' => array(
						'type'       => 'vamtam-color',
						'label'      => __( 'Title Color', 'vamtam-elements-b' ),
						'default'    => '#1a378e',
					),
					'title_color_hover' => array(
						'type'       => 'vamtam-color',
						'label'      => __( 'Title Hover Color', 'vamtam-elements-b' ),
						'default'    => '#ffffff',
					),
					'text_color' => array(
						'type'       => 'vamtam-color',
						'label'      => __( 'Text Color', 'vamtam-elements-b' ),
						'default'    => '#555',
					),
					'text_color_hover' => array(
						'type'       => 'vamtam-color',
						'label'      => __( 'Text Hover Color', 'vamtam-elements-b' ),
						'default'    => '#ffffff',
					),
					'bg_color_hover' => array(
						'type'       => 'vamtam-color',
						'label'      => __( 'Background Hover Color', 'vamtam-elements-b' ),
						'default'    => '#1a378e',
					),
				),
			),
			'title_settings' => array(
				'title'  => __( 'Title Settings', 'vamtam-elements-b' ),
				'fields' => array(
					'title_typography' => array(
						'type'       => 'typography',
						'label'      => __( 'Typography', 'vamtam-elements-b' ),
						'preview'    => array(
							'type'	    => 'css',
							'selector'  => '.fl-bwl-title',
						),
					),
				),
			),
			'structure' => array(
				'title'  => __( 'Structure', 'vamtam-elements-b' ),
				'fields' => array(
					'height' => array(
						'type'   => 'unit',
						'responsive' => true,
						'label'  => __( 'Height', 'vamtam-elements-b' ),
						'units'  => array( 'px', '%', 'vh', 'vw' ),
						'slider' => array(
							'px'	=> array(
								'min'	=> 0,
								'max'	=> 1000,
							),
							'%'	=> array(
								'min'	=> 0,
								'max'	=> 100,
							),
							'vh'	=> array(
								'min'	=> 0,
								'max'	=> 100,
							),
							'vw'	=> array(
								'min'	=> 0,
								'max'	=> 100,
							),
						),
						'preview'	   => array(
							'type'          => 'css',
							'selector'      => '.fl-bwl-wrap',
							'property'      => 'min-height',
						),
						'vamtam-wpml' => 'LINE',
					),
					'padding' => array(
						'type'        => 'dimension',
						'label'       => __( 'Padding', 'vamtam-elements-b' ),
						'responsive'  => true,
						'units'   => array( 'px', '%' ),
						'slider'  => array(
							'width'  => array(
								'px' => array(
									'min'  => 0,
									'max'  => 1000,
								),
								'%'  => array(
									'min' => 0,
									'max' => 100,
								),
							),
						),
					),
				),
			),
		),
	),
));
