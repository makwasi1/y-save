<?php

/**
 * @class VamtamHotelBooking
 */
class VamtamHotelBooking extends FLBuilderModule {

	/**
	 * @method __construct
	 */
	public function __construct() {
		$path = trailingslashit( 'modules/' . basename( dirname( __FILE__ ) ) );

		parent::__construct(array(
			'name'            => esc_html__( 'WP Hotel Booking - Rooms', 'vamtam-elements-b' ),
			'description'     => esc_html__( 'Display your rooms.', 'vamtam-elements-b' ),
			'category'        => esc_html__( 'VamTam Modules', 'vamtam-elements-b' ),
			'partial_refresh' => true,
			'enabled'         => class_exists( 'WP_Hotel_Booking' ),
			'dir'             => VAMTAMEL_B_DIR . $path,
			'url'             => VAMTAMEL_B_URL . $path,
		));
	}
}

/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('VamtamHotelBooking', array(
	'layout' => array(
		'title'    => __( 'Layout', 'vamtam-elements-b' ),
		'sections' => array(
			'general' => array(
				'title'  => '',
				'fields' => array(
					'layout' => array(
						'type'    => 'select',
						'label'   => __( 'Layout', 'vamtam-elements-b' ),
						'default' => 'mosaic',
						'options' => array(
							'small'    => esc_html__( 'Masonry with Fixed Images', 'wpv' ),
							'mosaic'   => esc_html__( 'Masonry', 'wpv' ),
							'scroll-x' => esc_html__( 'Scrollable', 'wpv' ),
						),
						'toggle' => array(
							'mosaic' => array(
								'sections' => array( 'grid' ),
								'fields'   => array( 'pagination', 'lightbox_button' ),
							),
							'scrollable' => array(
								'sections' => array( 'grid' ),
							),
						),
					),
					'pagination' => array(
						'label'   => __( 'Pagination', 'vamtam-elements-b' ),
						'default' => 'false',
						'type'    => 'select',
						'options' => array(
							'true'  => esc_html__( 'On', 'vamtam-elements-b' ),
							'false' => esc_html__( 'Off', 'vamtam-elements-b' ),
						),
					),
					'posts_per_page' => array(
						'type'    => 'unit',
						'label'   => esc_html__( 'Projects Per Page', 'vamtam-elements-b' ),
						'default' => 10,
						'min'     => -1,
						'max'     => 100,
					),
					'show_view_all' => array(
						'type'    => 'unit',
						'label'   => esc_html__( 'Show "View All Rooms"', 'vamtam-elements-b' ),
						'default' => 'true',
					),
				),
			),
			'grid' => array(
				'title'  => esc_html__( 'Grid', 'vamtam-elements-b' ),
				'fields' => array(
					'columns' => array(
						'type'    => 'unit',
						'label'   => esc_html__( 'Columns', 'vamtam-elements-b' ),
						'default' => 3,
						'min'     => 2,
						'max'     => 4,
					),
					'gap' => array(
						'type'    => 'select',
						'label'   => esc_html__( 'Gap Between Items', 'vamtam-elements-b' ),
						'default' => 'false',
						'options' => array(
							'true'  => esc_html__( 'On', 'vamtam-elements-b' ),
							'false' => esc_html__( 'Off', 'vamtam-elements-b' ),
						),
					),
				),
			),
		),
	),
	'content' => array(
		'title' => esc_html__( 'Query', 'vamtam-elements-b' ),
		'file'  => plugin_dir_path( __FILE__ ) . 'includes/loop-settings.php',
	),
));

