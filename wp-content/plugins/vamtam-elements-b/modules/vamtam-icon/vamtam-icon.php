<?php

/**
 * @class VamtamIconModule
 */
class VamtamIconModule extends FLBuilderModule {

	/**
	 * @method __construct
	 */
	public function __construct() {
		$path = trailingslashit( 'modules/' . basename( dirname( __FILE__ ) ) );

		parent::__construct(array(
			'name'            => __( 'Icon', 'vamtam-elements-b' ),
			'description'     => __( 'Display an icon and optional title.', 'vamtam-elements-b' ),
			'category'        => __( 'VamTam Modules', 'vamtam-elements-b' ),
			'editor_export'   => false,
			'partial_refresh' => true,
			'dir'             => VAMTAMEL_B_DIR . $path,
			'url'             => VAMTAMEL_B_URL . $path,
		));
	}
}

/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('VamtamIconModule', array(
	'general' => array( // Tab
		'title'    => __( 'General', 'vamtam-elements-b' ), // Tab title
		'sections' => array( // Tab Sections
			'general' => array( // Section
				'title'  => '', // Section Title
				'fields' => array( // Section Fields
					'icon' => array(
						'type'  => 'icon',
						'label' => __( 'Icon', 'vamtam-elements-b' ),
					),
				),
			),
			'link' => array(
				'title'  => __( 'Link', 'vamtam-elements-b' ),
				'fields' => array(
					'link' => array(
						'type'    => 'link',
						'label'   => __( 'Link', 'vamtam-elements-b' ),
						'preview' => array(
							'type' => 'none',
						),
					),
					'lightbox_embed' => array(
						'type'    => 'select',
						'label'   => __( 'Display contents in a lightbox (oEmbed required)', 'vamtam-elements-b' ),
						'default' => 'false',
						'options' => array(
							'false' => __( 'No, treat as regular link', 'vamtam-elements-b' ),
							'true'  => __( 'Use lightbox', 'vamtam-elements-b' ),
						),
						'preview' => array(
							'type' => 'none',
						),
						'toggle' => array(
							'false' => array(
								'fields' => array( 'link_target' ),
							),
						),
					),
					'link_target' => array(
						'type'    => 'select',
						'label'   => __( 'Link Target', 'vamtam-elements-b' ),
						'default' => '_self',
						'options' => array(
							'_self'  => __( 'Same Window', 'vamtam-elements-b' ),
							'_blank' => __( 'New Window', 'vamtam-elements-b' ),
						),
						'preview' => array(
							'type' => 'none',
						),
					),
				),
			),
			'text' => array(
				'title'  => esc_html__( 'Text', 'wpv' ),
				'fields' => array(
					'text' => array(
						'type'          => 'editor',
						'label'         => '',
						'media_buttons' => false,
					),
				),
			),
			'screen_reader_text' => array(
				'title'  => esc_html__( 'Screen Reader Text', 'wpv' ),
				'fields' => array(
					'screen_reader_text' => array(
						'type'          => 'text',
						'label'         => '',
						'media_buttons' => false,
						'vamtam-wpml'   => 'LINE',
					),
				),
			),
		),
	),
	'style' => array( // Tab
		'title'    => __( 'Style', 'vamtam-elements-b' ), // Tab title
		'sections' => array( // Tab Sections
			'colors' => array( // Section
				'title'  => __( 'Colors', 'vamtam-elements-b' ), // Section Title
				'fields' => array( // Section Fields
					'color' => array(
						'type'  => 'vamtam-color',
						'label' => __( 'Color', 'vamtam-elements-b' ),
					),
					'hover_color' => array(
						'type'    => 'vamtam-color',
						'label'   => __( 'Hover Color', 'vamtam-elements-b' ),
						'preview' => array(
							'type' => 'none',
						),
					),
					'bg_color' => array(
						'type'  => 'vamtam-color',
						'label' => __( 'Background Color', 'vamtam-elements-b' ),
					),
					'bg_hover_color' => array(
						'type'    => 'vamtam-color',
						'label'   => __( 'Background Hover Color', 'vamtam-elements-b' ),
						'preview' => array(
							'type' => 'none',
						),
					),
				),
			),
			'structure' => array( // Section
				'title'  => __( 'Structure', 'vamtam-elements-b' ), // Section Title
				'fields' => array( // Section Fields
					'size' => array(
						'type'        => 'text',
						'label'       => __( 'Size', 'vamtam-elements-b' ),
						'default'     => '30',
						'maxlength'   => '3',
						'size'        => '4',
						'description' => 'px',
					),
					'align' => array(
						'type'    => 'select',
						'label'   => __( 'Alignment', 'vamtam-elements-b' ),
						'default' => 'left',
						'options' => array(
							'center' => __( 'Center', 'vamtam-elements-b' ),
							'left'   => __( 'Left', 'vamtam-elements-b' ),
							'right'  => __( 'Right', 'vamtam-elements-b' ),
						),
					),
				),
			),
			'r_structure' => array(
				'title'  => __( 'Mobile Structure', 'vamtam-elements-b' ),
				'fields' => array(
					'r_align' => array(
						'type'    => 'select',
						'label'   => __( 'Alignment', 'vamtam-elements-b' ),
						'default' => 'default',
						'options' => array(
							'default' => __( 'Default', 'vamtam-elements-b' ),
							'custom'  => __( 'Custom', 'vamtam-elements-b' ),
						),
						'toggle' => array(
							'custom' => array(
								'fields' => array( 'r_custom_align' ),
							),
						),
					),
					'r_custom_align' => array(
						'type'    => 'select',
						'label'   => __( 'Custom Alignment', 'vamtam-elements-b' ),
						'default' => 'left',
						'options' => array(
							'left'   => __( 'Left', 'vamtam-elements-b' ),
							'center' => __( 'Center', 'vamtam-elements-b' ),
							'right'  => __( 'Right', 'vamtam-elements-b' ),
						),
					),
				),
			),
		),
	),
));
