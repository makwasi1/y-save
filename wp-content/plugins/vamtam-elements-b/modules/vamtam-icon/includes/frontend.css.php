<?php if ( ! isset( $settings->exclude_wrapper ) && ! empty( $settings->align ) ) : ?>
	.fl-node-<?php echo $id; ?>.fl-module-vamtam-icon {
		text-align: <?php echo $settings->align; ?>
	}
<?php endif; ?>

.fl-node-<?php echo $id; ?> .fl-module-content .fl-icon i {
	font-size: <?php echo $settings->size; ?>px;

	<?php if ( $settings->color ) : ?>
		color: <?php echo esc_html( vamtam_el_sanitize_accent( $settings->color ) ) ?>;
	<?php endif; ?>
	<?php if ( $settings->bg_color ) : ?>
		background: <?php echo esc_html( vamtam_el_sanitize_accent( $settings->bg_color ) ) ?>;
	<?php endif; ?>
}

<?php if ( ! empty( $settings->bg_hover_color ) || ! empty( $settings->hover_color ) ) : ?>
	.fl-node-<?php echo $id; ?> .fl-module-content .fl-icon i:hover,
	.fl-node-<?php echo $id; ?> .fl-module-content .fl-icon a:hover i {
		<?php if ( ! empty( $settings->bg_hover_color ) ) : ?>
			background: <?php echo esc_html( vamtam_el_sanitize_accent( $settings->bg_hover_color ) ) ?>;
		<?php endif; ?>
		<?php if ( ! empty( $settings->hover_color ) ) : ?>
			color: <?php echo esc_html( vamtam_el_sanitize_accent( $settings->hover_color ) ) ?>;
		<?php endif; ?>
	}
<?php endif ?>

<?php if ( ! empty( $settings->bg_hover_color ) || ! empty( $settings->bg_color ) ) : ?>
	.fl-node-<?php echo $id; ?> .fl-module-content .fl-icon-text {
		height: <?php echo $settings->size * 1.75; ?>px;
	}
<?php endif ?>

<?php if ( $global_settings->responsive_enabled && ($settings->r_align == 'custom') ) : ?>
	@media screen and (max-width: <?php echo $global_settings->responsive_breakpoint; ?>px) {
		.fl-node-<?php echo $id; ?>.fl-module-vamtam-icon {
			text-align: <?php echo $settings->r_custom_align ?> !important;
		}
	}
<?php endif; ?>
