<?php

$news       = in_array( $settings->layout, array( 'scroll-x', 'small', 'mosaic' ) );
$scrollable = $news && $settings->layout === 'scroll-x';

if ( ! $news ) {
	$settings->columns = 1;
}

$called_from_shortcode = true;

$settings->columns = (int) $settings->columns;

$max_columns = $settings->columns;

if ( 0 === $settings->columns ) {
	$settings->columns = 4; // this is used for thumbnails only
}

$settings->gap = vamtam_sanitize_bool( $settings->gap );

wp_reset_query();

// Get the query data.
$blog_query = FLBuilderLoop::query( $settings );

$GLOBALS['vamtam_blog_query'] = $blog_query;

if ( $scrollable ) {
	include locate_template( array( 'templates/blog-scrollable.php' ) );
} else {
	include locate_template( array( 'loop.php' ) );
}

// Render the empty message.
if ( ! $blog_query->have_posts() && (defined( 'DOING_AJAX' ) || isset( $_REQUEST['fl_builder'] )) ) :

?>
<div class="fl-post-grid-empty">
	<?php esc_html_e( 'No posts found.', 'vamtam-elements-b' ); ?>
</div>

<?php

endif;

wp_reset_postdata();

unset( $GLOBALS['vamtam_blog_query'] );

wp_reset_query();
