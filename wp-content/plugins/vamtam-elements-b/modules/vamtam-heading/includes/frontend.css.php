<?php
	$alignment_tr = array(
		'left'   => 'flex-start',
		'center' => 'center',
		'right'  => 'flex-end',
	);
?>
.fl-node-<?php echo esc_html( $id ); ?>.fl-module-vamtam-heading .vamtam-heading {
	text-align: <?php echo esc_html( $settings->alignment ); ?>;

	<?php if ( 'off' === $settings->with_divider ) : ?>
		justify-content: <?php echo esc_html( $alignment_tr[ $settings->alignment ] ) ?>;
	<?php endif ?>

	<?php if ( 'custom' === $settings->font_size ) : ?>
		font-size: <?php echo intval( $settings->custom_font_size ); ?>px;
	<?php endif; ?>
	<?php if ( 'custom' === $settings->line_height ) : ?>
		line-height: <?php echo esc_html( $settings->custom_line_height ); ?>;
	<?php endif; ?>
	<?php if ( 'custom' === $settings->letter_spacing ) : ?>
		letter-spacing: <?php echo intval( $settings->custom_letter_spacing ); ?>px;
	<?php endif; ?>
}

<?php if ( ! empty( $settings->color ) ) : ?>
	.fl-node-<?php echo esc_html( $id ) ?> <?php echo esc_html( $settings->tag ) ?>.vamtam-heading * {
		color: <?php echo esc_html( vamtam_el_sanitize_accent( $settings->color ) ); ?> !important;
		<?php // !important necessary to override default Beaver CSS ?>
	}
<?php endif; ?>

<?php if ( in_array( $settings->alignment, array( 'left', 'right' ), true ) ) : ?>
	.fl-node-<?php echo esc_html( $id ); ?>.fl-module-vamtam-heading .vamtam-heading-line-<?php echo esc_html( $settings->alignment ) ?> {
		display: none;
	}
<?php endif ?>

.fl-node-<?php echo esc_html( $id ); ?> .vamtam-heading .vamtam-heading-text {
	<?php if ( ! empty( $settings->font ) && 'Default' !== $settings->font['family'] ) : ?>
		<?php FLBuilderFonts::font_css( $settings->font ); ?>
	<?php endif; ?>
}

<?php if ( $global_settings->responsive_enabled && in_array( 'custom', array( $settings->r_font_size, $settings->r_alignment, $settings->r_line_height, $settings->r_letter_spacing ), true ) ) : ?>
	@media screen and (max-width: <?php echo intval( $global_settings->responsive_breakpoint ) ?>px) {

		<?php if ( in_array( $settings->alignment, array( 'left', 'right' ), true ) ) : ?>
			.fl-node-<?php echo esc_html( $id ); ?>.fl-module-vamtam-heading .vamtam-heading-line-<?php echo esc_html( $settings->r_custom_alignment ) ?> {
				display: none;
			}
			.fl-node-<?php echo esc_html( $id ); ?>.fl-module-vamtam-heading .vamtam-heading-line:not(.vamtam-heading-line-<?php echo esc_html( $settings->r_custom_alignment ) ?>) {
				display: block;
			}
		<?php endif ?>

		.fl-node-<?php echo esc_html( $id ); ?>.fl-module-vamtam-heading .vamtam-heading {
			<?php if ( 'custom' === $settings->r_alignment ) : ?>
				text-align: <?php echo esc_html( $settings->r_custom_alignment ) ?>;
			<?php endif; ?>

			<?php if ( 'off' === $settings->with_divider && 'custom' === $settings->r_alignment ) : ?>
				justify-content: <?php echo esc_html( $alignment_tr[ $settings->r_custom_alignment ] ) ?>;
			<?php endif ?>

			<?php if ( 'custom' === $settings->r_font_size ) : ?>
				font-size: <?php echo intval( $settings->r_custom_font_size ) ?>px;
			<?php endif; ?>

			<?php if ( 'custom' === $settings->r_line_height ) : ?>
				line-height: <?php echo esc_html( $settings->r_custom_line_height ) ?>;
			<?php endif; ?>

			<?php if ( 'custom' === $settings->r_letter_spacing ) : ?>
				letter-spacing: <?php echo intval( $settings->r_custom_letter_spacing ) ?>px;
			<?php endif; ?>
		}
	}
<?php endif; ?>
