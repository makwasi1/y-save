<?php

/**
 * @class VamtamExpandScroll
 */
class VamtamExpandScroll extends FLBuilderModule {

	/**
	 * @method __construct
	 */
	public function __construct() {
		$path = trailingslashit( 'modules/' . basename( dirname( __FILE__ ) ) );

		parent::__construct(array(
			'name'            => __( 'Expand + Scroll', 'vamtam-elements-b' ),
			'description'     => __( 'The central area is expanded and scrolls horizontally.', 'vamtam-elements-b' ),
			'category'        => __( 'VamTam Modules', 'vamtam-elements-b' ),
			'editor_export'   => false,
			'partial_refresh' => true,
			'dir'             => VAMTAMEL_B_DIR . $path,
			'url'             => VAMTAMEL_B_URL . $path,
			'enabled'         => current_theme_supports( 'vamtam-expand-scroll' ),
		));

	}
}

/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('VamtamExpandScroll', array(
	'general' => array( // Tab
		'title'    => __( 'General', 'vamtam-elements-b' ), // Tab title
		'sections' => array( // Tab Sections
			'general' => array( // Section
				'title'  => '', // Section Title
				'fields' => array( // Section Fields
					'above_content' => array(
						'type'    => 'editor',
						'label'   => __( 'Above Content', 'vamtam-elements-b' ),
						'default' => '',
					),
					'items' => array(
						'type'         => 'form',
						'label'        => __( 'Pane', 'vamtam-elements-b' ),
						'form'         => 'scrollable_content_form',
						'preview_text' => 'label',
						'multiple'     => true,
					),
					'below_content' => array(
						'type'    => 'editor',
						'label'   => __( 'Below Content', 'vamtam-elements-b' ),
						'default' => '',
					),
				),
			),
			'animation' => array( // Section
				'title'  => esc_html__( 'Scroll Settings', 'vamtam-elements-b' ), // Section Title
				'fields' => array( // Section Fields
					'duration' => array(
						'type'        => 'text',
						'label'       => __( 'Scroll Duration', 'vamtam-elements-b' ),
						'default'     => '2000',
						'maxlength'   => '6',
						'size'        => '6',
						'description' => 'px',
						'preview'     => [
							'type' => 'none',
						],
					),
				),
			),
		),
	),
));

FLBuilder::register_settings_form('scrollable_content_form', array(
	'title' => __( 'Add Pane', 'vamtam-elements-b' ),
	'tabs'  => array(
		'general' => array(
			'title'    => __( 'General', 'vamtam-elements-b' ),
			'sections' => array(
				'general' => array(
					'title'  => '',
					'fields' => array(
						'template' => array(
							'type'    => 'select',
							'label'   => __( 'Template', 'vamtam-elements-b' ),
							'options' => function_exists( 'vamtam_get_beaver_layouts' ) ? vamtam_get_beaver_layouts( [
								'' => esc_html__( '-- Select Template --', 'wpv' ),
							] ) : [],
							'vamtam-wpml' => 'LINE',
						),
					),
				),
			),
		),
	),
));
