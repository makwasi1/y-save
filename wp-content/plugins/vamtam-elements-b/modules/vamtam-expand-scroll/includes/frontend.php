<?php
global $content_width;

$single_width = $content_width + 2 * FLBuilderModel::get_global_settings()->module_margins;
$duration = $single_width * count( $settings->items );

$options = [
	'type'       => 'progressive',
	'origin'     => 'center center',
	'exit'       => false,
	'delay'      => 0,
	'mobile'     => false,
	'pin'        => $settings->duration,
	'pinUnit'    => 'px',
	'pinTrigger' => 'center',
];

$content_style = FLBuilderModel::is_builder_active() ? '' : 'display:flex;width:' . (int) $duration . 'px';

?>
<div class="vamtam-expand-scroll-wrapper" data-progressive-animation="expand-scroll" data-vamtam-animation-options="<?php echo esc_attr( json_encode( $options ) ) ?>">
	<div class="vamtam-expand-scroll-above"><?php echo $settings->above_content ?></div>
	<div class="vamtam-expand-scroll-content" style="<?php echo esc_attr( $content_style ) ?>">
		<?php foreach ( $settings->items as $item ) : if ( empty( $item->template ) ) continue; ?>
			<div class="vamtam-expand-scroll-content-pane" style="width:<?= (int) $single_width?>px">
				<?php
					echo FLBuilderShortcodes::insert_layout( [ // xss ok
						'slug' => $item->template,
					] );
				?>
			</div>
		<?php endforeach ?>
	</div>
	<div class="vamtam-expand-scroll-below"><?php echo $settings->below_content ?></div>
</div>
