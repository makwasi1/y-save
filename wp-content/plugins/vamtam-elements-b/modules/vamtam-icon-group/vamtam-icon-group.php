<?php

/**
 * @class VamtamIconGroupModule
 */
class VamtamIconGroupModule extends FLBuilderModule {

	/**
	 * @method __construct
	 */
	public function __construct() {
		$path = trailingslashit( 'modules/' . basename( dirname( __FILE__ ) ) );

		parent::__construct(array(
			'name'            => __( 'Icon Group', 'vamtam-elements-b' ),
			'description'     => __( 'Display a group of linked Font Awesome icons.', 'vamtam-elements-b' ),
			'category'        => __( 'VamTam Modules', 'vamtam-elements-b' ),
			'editor_export'   => false,
			'partial_refresh' => true,
			'dir'             => VAMTAMEL_B_DIR . $path,
			'url'             => VAMTAMEL_B_URL . $path,
		));
	}
}

/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('VamtamIconGroupModule', array(
	'icons' => array(
		'title'    => __( 'Icons', 'vamtam-elements-b' ),
		'sections' => array(
			'general' => array(
				'title'  => '',
				'fields' => array(
					'icons' => array(
						'type'         => 'form',
						'label'        => __( 'Icon', 'vamtam-elements-b' ),
						'form'         => 'icon_group_form', // ID from registered form below
						'preview_text' => 'icon', // Name of a field to use for the preview text
						'multiple'     => true,
					),
				),
			),
		),
	),
	'style' => array( // Tab
		'title'    => __( 'Style', 'vamtam-elements-b' ), // Tab title
		'sections' => array( // Tab Sections
			'colors' => array( // Section
				'title'  => __( 'Colors', 'vamtam-elements-b' ), // Section Title
				'fields' => array( // Section Fields
					'color' => array(
						'type'  => 'vamtam-color',
						'label' => __( 'Color', 'vamtam-elements-b' ),
					),
					'hover_color' => array(
						'type'    => 'vamtam-color',
						'label'   => __( 'Hover Color', 'vamtam-elements-b' ),
						'preview' => array(
							'type' => 'none',
						),
					),
					'bg_color' => array(
						'type'  => 'vamtam-color',
						'label' => __( 'Background Color', 'vamtam-elements-b' ),
					),
					'bg_hover_color' => array(
						'type'    => 'vamtam-color',
						'label'   => __( 'Background Hover Color', 'vamtam-elements-b' ),
						'preview' => array(
							'type' => 'none',
						),
					),
				),
			),
			'structure' => array( // Section
				'title'  => __( 'Structure', 'vamtam-elements-b' ), // Section Title
				'fields' => array( // Section Fields
					'size' => array(
						'type'        => 'text',
						'label'       => __( 'Size', 'vamtam-elements-b' ),
						'default'     => '30',
						'maxlength'   => '3',
						'size'        => '4',
						'description' => 'px',
					),
					'spacing' => array(
						'type'        => 'text',
						'label'       => __( 'Spacing', 'vamtam-elements-b' ),
						'default'     => '10',
						'maxlength'   => '3',
						'size'        => '4',
						'description' => 'px',
					),
					'align' => array(
						'type'    => 'select',
						'label'   => __( 'Alignment', 'vamtam-elements-b' ),
						'default' => 'center',
						'options' => array(
							'center' => __( 'Center', 'vamtam-elements-b' ),
							'left'   => __( 'Left', 'vamtam-elements-b' ),
							'right'  => __( 'Right', 'vamtam-elements-b' ),
						),
					),
				),
			),
		),
	),
));

/**
 * Register a settings form to use in the "form" field type above.
 */
FLBuilder::register_settings_form('icon_group_form', array(
	'title' => __( 'Add Icon', 'vamtam-elements-b' ),
	'tabs'  => array(
		'general' => array( // Tab
			'title'    => __( 'General', 'vamtam-elements-b' ), // Tab title
			'sections' => array( // Tab Sections
				'general' => array( // Section
					'title'  => '', // Section Title
					'fields' => array( // Section Fields
						'icon' => array(
							'type'  => 'icon',
							'label' => __( 'Icon', 'vamtam-elements-b' ),
						),
						'link' => array(
							'type'  => 'link',
							'label' => __( 'Link', 'vamtam-elements-b' ),
						),
						'link_target' => array(
							'type'    => 'select',
							'label'   => __( 'Link Target', 'vamtam-elements-b' ),
							'default' => '_blank',
							'options' => array(
								'_self'  => __( 'Same Window', 'vamtam-elements-b' ),
								'_blank' => __( 'New Window', 'vamtam-elements-b' ),
							),
						),
						'screen_reader_text' => array(
							'type'          => 'text',
							'label'         => esc_html__( 'Screen Reader Text', 'wpv' ),
							'media_buttons' => false,
							'vamtam-wpml'   => 'LINE',
						),
					),
				),
			),
		),
		'style' => array( // Tab
			'title'    => __( 'Style', 'vamtam-elements-b' ), // Tab title
			'sections' => array( // Tab Sections
				'colors' => array( // Section
					'title'  => __( 'Colors', 'vamtam-elements-b' ), // Section Title
					'fields' => array( // Section Fields
						'color' => array(
							'type'  => 'vamtam-color',
							'label' => __( 'Color', 'vamtam-elements-b' ),
						),
						'hover_color' => array(
							'type'    => 'vamtam-color',
							'label'   => __( 'Hover Color', 'vamtam-elements-b' ),
							'preview' => array(
								'type' => 'none',
							),
						),
						'bg_color' => array(
							'type'  => 'vamtam-color',
							'label' => __( 'Background Color', 'vamtam-elements-b' ),
						),
						'bg_hover_color' => array(
							'type'    => 'vamtam-color',
							'label'   => __( 'Background Hover Color', 'vamtam-elements-b' ),
							'preview' => array(
								'type' => 'none',
							),
						),
					),
				),
			),
		),
	),
));
