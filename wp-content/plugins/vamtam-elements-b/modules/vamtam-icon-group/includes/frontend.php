<div class="fl-icon-group text<?php echo esc_attr( $settings->align ) ?>">
<?php

foreach ( $settings->icons as $icon ) {

	if ( ! is_object( $icon ) ) {
		continue;
	}

	$icon_settings = array(
		'bg_color'           => $icon->bg_color ? $icon->bg_color : $settings->bg_color,
		'bg_hover_color'     => $icon->bg_hover_color ? $icon->bg_hover_color : $settings->bg_hover_color,
		'color'              => $icon->color ? $icon->color : $settings->color,
		'hover_color'        => $icon->hover_color ? $icon->hover_color : $settings->hover_color,
		'exclude_wrapper'    => true,
		'icon'               => $icon->icon,
		'link'               => $icon->link,
		'link_target'        => isset( $icon->link_target ) ? $icon->link_target : '_blank',
		'size'               => $settings->size,
		'text'               => '',
		'screen_reader_text' => $icon->screen_reader_text,
	);

	FLBuilder::render_module_html( 'vamtam-icon', $icon_settings );
}

?>
</div>
