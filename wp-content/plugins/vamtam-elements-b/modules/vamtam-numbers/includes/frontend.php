<div class="fl-number fl-number-<?php echo esc_attr( $settings->layout ) ?>">
	<?php if ( $settings->layout == 'circle' ) : ?>
		<div class="fl-number-circle-container">
			<div class="fl-number-text">
				<?php if ( ! empty( $settings->before_number_text ) ) : ?>
					<span class="fl-number-before-text"><?php echo $settings->before_number_text; // xss ok ?></span>
				<?php endif; ?>

				<?php $module->render_number(); ?>

				<?php if ( ! empty( $settings->after_number_text ) ) : ?>
					<span class="fl-number-after-text"><?php echo $settings->after_number_text; // xss ok ?></span>
				<?php endif; ?>
			</div>
			<?php $module->render_circle_bar(); ?>
		</div>
	<?php elseif ( $settings->layout == 'bars' ) : ?>
		<div class="fl-number-text fl-number-position-<?php echo esc_attr( $settings->number_position );?>">
			<?php if ( ! empty( $settings->before_number_text ) ) : ?>
				<span class="fl-number-before-text"><?php echo $settings->before_number_text; // xss ok ?></span>
			<?php endif; ?>

			<?php $position = $settings->number_position ? $settings->number_position : 'default'; ?>

			<?php if ( $position == 'above' ) :  ?>
				<?php $module->render_number(); ?>
				<div class="fl-number-bars-container">
					<div class="fl-number-bar"></div>
				</div>
			<?php elseif ( $position == 'below' ) : ?>
				<div class="fl-number-bars-container">
					<div class="fl-number-bar"></div>
				</div>
				<?php $module->render_number(); ?>
			<?php else : ?>
				<div class="fl-number-bars-container">
					<div class="fl-number-bar"></div>
					<?php $module->render_number(); ?>
				</div>
			<?php endif; ?>

			<?php if ( ! empty( $settings->after_number_text ) ) : ?>
				<span class="fl-number-after-text"><?php echo $settings->after_number_text; // xss ok ?></span>
			<?php endif; ?>

			<style>
				.fl-node-<?php echo esc_html( $id ) ?> .fl-number-bars-container {
					background-color: <?php echo esc_html( vamtam_el_sanitize_accent( $settings->bar_bg_color ) )?>;
				}
				.fl-node-<?php echo esc_html( $id ) ?> .fl-number-bar {
					background-color: <?php echo esc_html( vamtam_el_sanitize_accent( $settings->bar_color ) )?>;
				}
			</style>
		</div>
	<?php else : ?>
		<div class="fl-number-text">
			<?php if ( ! empty( $settings->before_number_text ) ) : ?>
				<span class="fl-number-before-text"><?php echo $settings->before_number_text; // xss ok ?></span>
			<?php endif; ?>

			<?php $module->render_number(); ?>

			<?php if ( ! empty( $settings->after_number_text ) ) : ?>
				<span class="fl-number-after-text"><?php echo $settings->after_number_text; // xss ok ?></span>
			<?php endif; ?>
		</div>
	<?php endif; ?>
</div>
