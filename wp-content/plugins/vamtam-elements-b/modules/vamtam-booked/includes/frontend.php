<?php

global $shortcode_tags;

switch ( $settings->shortcode ) {
	case 'booked-calendar':
		if ( isset( $shortcode_tags[ $settings->shortcode ] ) ) {
			echo call_user_func( $shortcode_tags[ $settings->shortcode ], array(
				'size'         => $settings->size,
				'calendar'     => $settings->calendar,
				'year'         => $settings->year,
				'month'        => $settings->month,
				'day'          => $settings->day,
				'switcher'     => $settings->switcher,
				'style'        => $settings->style,
				'members-only' => $settings->members_only,
			) );
		}
	break;

	default:
		if ( isset( $shortcode_tags[ $settings->shortcode ] ) ) {
			echo call_user_func( $shortcode_tags[ $settings->shortcode ], array() );
		}
}
