<?php

/**
 * @class VamtamBookedModule
 */
class VamtamBookedModule extends FLBuilderModule {

	/**
	 * @method __construct
	 */
	public function __construct() {
		$path = trailingslashit( 'modules/' . basename( dirname( __FILE__ ) ) );

		parent::__construct(array(
			'name'            => __( 'Booked', 'vamtam-elements-b' ),
			'description'     => __( 'Display a Booked Calendar.', 'vamtam-elements-b' ),
			'category'        => __( 'VamTam Modules', 'vamtam-elements-b' ),
			'partial_refresh' => true,
			'dir'             => VAMTAMEL_B_DIR . $path,
			'url'             => VAMTAMEL_B_URL . $path,
			'enabled'         => shortcode_exists( 'booked-calendar' ),
		));
	}

	public static function get_booked_calendars() {
		if ( ! is_plugin_active( 'booked/booked.php' ) ) return array();

		$calendars = get_terms( 'booked_custom_calendars', array(
			'orderby'    => 'name',
			'order'      => 'ASC',
			'hide_empty' => false,
		) );

		$result = array();

		foreach ( $calendars as $calendar ) {
			if ( is_object( $calendar ) ) {
				$result[ $calendar->term_id ] = $calendar->name;
			}
		}

		return $result;
	}
}

/**
 * Register the module and its form settings.
 */
FLBuilder::register_module( 'VamtamBookedModule', array(
	'layout' => array(
		'title'    => __( 'General', 'vamtam-elements-b' ),
		'sections' => array(
			'general' => array(
				'title'  => '',
				'fields' => array(
					'shortcode' => array(
						'label'   => esc_html__( 'Type', 'vamtam-elements-b' ),
						'default' => 'booked-calendar',
						'type'    => 'select',
						'options' => array(
							'booked-calendar'     => esc_html__( 'Calendar', 'vamtam-elements-b' ),
							'booked-login'        => esc_html__( 'Login form', 'vamtam-elements-b' ),
							'booked-profile'      => esc_html__( 'User Profile', 'vamtam-elements-b' ),
							'booked-appointments' => esc_html__( 'User Appointments', 'vamtam-elements-b' ),
						),
						'toggle' => array(
							'booked-calendar' => array(
								'sections' => array( 'calendar_options' ),
							),
						),
					),
				),
			),
			'calendar_options' => array(
				'title'  => esc_html__( 'Calendar Options', 'vamtam-elements-b' ),
				'fields' => array(
					'year' => array(
						'label'   => esc_html__( 'Year', 'vamtam-elements-b' ),
						'desc'    => esc_html__( 'Leave blank for default', 'vamtam-elements-b' ),
						'default' => '',
						'type'    => 'text',
					),

					'month' => array(
						'label'   => esc_html__( 'Month', 'vamtam-elements-b' ),
						'desc'    => esc_html__( 'Leave blank for default', 'vamtam-elements-b' ),
						'default' => '',
						'type'    => 'text',
					),

					'day' => array(
						'label'   => esc_html__( 'Day', 'vamtam-elements-b' ),
						'desc'    => esc_html__( 'Leave blank for default', 'vamtam-elements-b' ),
						'default' => '',
						'type'    => 'text',
					),

					'switcher' => array(
						'label'   => esc_html__( 'Switcher', 'vamtam-elements-b' ),
						'default' => '0',
						'type'    => 'select',
						'options' => array(
							'1' => esc_html__( 'On', 'vamtam-elements-b' ),
							'0' => esc_html__( 'Off', 'vamtam-elements-b' ),
						),
					),

					'style' => array(
						'label'   => esc_html__( 'Style', 'vamtam-elements-b' ),
						'default' => 'calendar',
						'type'    => 'select',
						'options' => array(
							'list'     => esc_html__( 'List', 'vamtam-elements-b' ),
							'calendar' => esc_html__( 'Calendar', 'vamtam-elements-b' ),
						),
					),

					'calendar' => array(
						'label'       => esc_html__( 'Calendar', 'vamtam-elements-b' ),
						'default'     => '',
						'prompt'      => '',
						'options'     => VamtamBookedModule::get_booked_calendars(),
						'type'        => 'select',
						'vamtam-wpml' => 'LINE',
					),

					'size' => array(
						'label'   => esc_html__( 'Size', 'vamtam-elements-b' ),
						'default' => 'large',
						'type'    => 'select',
						'options' => array(
							'small' => esc_html__( 'Small', 'vamtam-elements-b' ),
							'large' => esc_html__( 'Large', 'vamtam-elements-b' ),
						),
					),

					'members_only' => array(
						'label'   => esc_html__( 'Visibility', 'vamtam-elements-b' ),
						'default' => '0',
						'type'    => 'select',
						'options' => array(
							'1' => esc_html__( 'Logged in users only', 'vamtam-elements-b' ),
							'0' => esc_html__( 'All users', 'vamtam-elements-b' ),
						),
					),
				),
			),
		),
	),
) );
