<?php

/**
 * @class VamtamNinjaFormModule
 */
class VamtamNinjaFormModule extends FLBuilderModule {

	/**
	 * @method __construct
	 */
	public function __construct() {
		$path = trailingslashit( 'modules/' . basename( dirname( __FILE__ ) ) );

		parent::__construct(array(
			'name'            => __( 'Ninja Forms', 'vamtam-elements-b' ),
			'description'     => __( 'Display a contact form.', 'vamtam-elements-b' ),
			'category'        => __( 'VamTam Modules', 'vamtam-elements-b' ),
			'partial_refresh' => false,
			'enabled'         => class_exists( 'Ninja_Forms' ),
			'dir'             => VAMTAMEL_B_DIR . $path,
			'url'             => VAMTAMEL_B_URL . $path,
		));
	}

	public static function get_ninja_forms() {
		if ( ! class_exists( 'Ninja_Forms' ) ) return array();

		$forms = Ninja_Forms()->form();

		if ( ! method_exists( $forms, 'get_forms' ) ) {
			return array(
				'' => esc_html__( 'Ninja Forms 3 is required for this module', 'vamtam-elements-b' ),
			);
		}

		$forms = $forms->get_forms();

		$result = array();

		$result[''] = esc_html__( 'Choose form', 'vamtam-elements-b' );

		foreach ( $forms as $form ) {
			$form_id = $form->get_id();

			$result[ $form_id ] = "($form_id) " . $form->get_setting( 'title' );
		}

		return $result;
	}
}

/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('VamtamNinjaFormModule', array(
	'layout' => array(
		'title'    => __( 'Basic', 'vamtam-elements-b' ),
		'sections' => array(
			'general' => array(
				'title'  => '',
				'fields' => array(
					'form_id' => array(
						'label'       => esc_html__( 'Form', 'vamtam-elements-b' ),
						'default'     => '',
						'options'     => VamtamNinjaFormModule::get_ninja_forms(),
						'type'        => 'select',
						'vamtam-wpml' => 'LINE',
					),
				),
			),
		),
	),
));
