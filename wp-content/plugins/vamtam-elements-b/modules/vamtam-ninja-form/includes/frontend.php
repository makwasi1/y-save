<?php

if ( empty( $settings->form_id ) ) {
	echo esc_html__( 'No form selected.', 'vamtam-elements-b' );
}

echo do_shortcode( '[ninja_form id="' . $settings->form_id . '"]' );
