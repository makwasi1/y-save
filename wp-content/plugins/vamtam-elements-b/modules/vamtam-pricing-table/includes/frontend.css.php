/*Features Min Height*/
.fl-builder-content .fl-node-<?php echo $id; ?> .fl-pricing-table-features  {
	min-height: <?php echo $settings->min_height; ?>px;
}

<?php
// Loop through and style each pricing box
for ( $i = 0; $i < count( $settings->pricing_columns ); $i++ ) :

	if ( ! is_object( $settings->pricing_columns[ $i ] )) continue;

	// Pricing Box Settings
	$pricing_column = $settings->pricing_columns[ $i ];

?>

	/*Pricing Box Style*/
	.fl-builder-content .fl-node-<?php echo $id; ?> .fl-pricing-table-column-<?php echo $i; ?> {
		margin-top: <?php echo $pricing_column->margin; ?>px;
	}
	.fl-builder-content .fl-node-<?php echo $id; ?> .fl-pricing-table-column-<?php echo $i; ?> h2 {
		font-size: <?php echo $pricing_column->title_size; ?>px;
	}
	.fl-builder-content .fl-node-<?php echo $id; ?> .fl-pricing-table-column-<?php echo $i; ?> .fl-pricing-table-price {
		font-size: <?php echo $pricing_column->price_size; ?>px;
	}

	/*Fix when price is NOT highlighted*/
	<?php if ( $settings->highlight == 'title' || $settings->highlight == 'none' ) : ?>
		.fl-builder-content .fl-node-<?php echo $id; ?> .fl-pricing-table-column-<?php echo $i; ?> .fl-pricing-table-price {
			margin-bottom: 0;
			padding-bottom: 0;
		}
		.fl-builder-content .fl-node-<?php echo $id; ?> .fl-pricing-table-column-<?php echo $i; ?> .fl-pricing-table-features {
			margin-top: 10px;
		}
	<?php endif; ?>

	/*Fix when NOTHING is highlighted*/
	<?php if ( $settings->highlight == 'none' ) : ?>
		.fl-builder-content .fl-node-<?php echo $id; ?> .fl-pricing-table-column-<?php echo $i; ?> .fl-pricing-table-title {
			padding-bottom: 0;
		}
		.fl-builder-content .fl-node-<?php echo $id; ?> .fl-pricing-table-column-<?php echo $i; ?> .fl-pricing-table-price {
			padding-top: 0;
		}
	<?php endif; ?>

	/*Button CSS*/
	.fl-builder-content .fl-node-<?php echo $id; ?> .fl-pricing-table-column-<?php echo $i; ?> a.vamtam-button {
		<?php if ( empty( $pricing_column->btn_width ) ) : ?>
			 display:block;
			 margin: 0 30px 5px;
		<?php endif; ?>
	}

<?php endfor; ?>
