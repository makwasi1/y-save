<?php

/**
 * @class VamtamRichTextModule
 */
class VamtamPricingTableModule extends FLBuilderModule {

	/**
	 * @method __construct
	 */
	public function __construct() {
		$path = trailingslashit( 'modules/' . basename( dirname( __FILE__ ) ) );

		parent::__construct(array(
			'name'            => __( 'Pricing Table', 'vamtam-elements-b' ),
			'description'     => __( 'A simple pricing table generator.', 'vamtam-elements-b' ),
			'category'        => __( 'VamTam Modules', 'vamtam-elements-b' ),
			'partial_refresh' => true,
			'dir'             => VAMTAMEL_B_DIR . $path,
			'url'             => VAMTAMEL_B_URL . $path,
		));
	}

	public static function get_button_settings( $settings ) {
		return array(
			'align'          => $settings->btn_align,
			'color'          => $settings->btn_color,
			'hover_color'    => $settings->btn_hover_color,
			'font_size'      => $settings->btn_font_size,
			'icon'           => $settings->btn_icon,
			'icon_position'  => $settings->btn_icon_position,
			'link'           => $settings->button_url,
			'link_target'    => $settings->btn_link_target,
			'padding'        => $settings->btn_padding,
			'layout_type'    => $settings->btn_layout_type,
			'text'           => $settings->button_text,
			'width'          => $settings->btn_width,
		);
	}

	/**
	 * @method render_button
	 */
	public function render_button( $column ) {
		FLBuilder::render_module_html( 'vamtam-button', self::get_button_settings( $this->settings->pricing_columns[ $column ] ) );
	}
}

/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('VamtamPricingTableModule', array(
	'columns' => array(
		'title'    => __( 'Pricing Boxes', 'vamtam-elements-b' ),
		'sections' => array(
			'general' => array(
				'title'  => '',
				'fields' => array(
					'pricing_columns' => array(
						'type'         => 'form',
						'label'        => __( 'Pricing Box', 'vamtam-elements-b' ),
						'form'         => 'pricing_column_form',
						'preview_text' => 'title',
						'multiple'     => true,
					),
				),
			),
		),
	),
	'style' => array(
		'title'    => __( 'Style', 'vamtam-elements-b' ),
		'sections' => array(
			'general' => array(
				'title'  => '',
				'fields' => array(
					'highlight' => array(
						'type'    => 'select',
						'label'   => __( 'Highlight', 'vamtam-elements-b' ),
						'default' => 'price',
						'options' => array(
							'price' => __( 'Price', 'vamtam-elements-b' ),
							'title' => __( 'Title', 'vamtam-elements-b' ),
							'none'  => __( 'None', 'vamtam-elements-b' ),
						),
					),
					'border_radius' => array(
						'type'    => 'select',
						'label'   => __( 'Border Style', 'vamtam-elements-b' ),
						'default' => 'rounded',
						'options' => array(
							'rounded'  => __( 'Rounded', 'vamtam-elements-b' ),
							'straight' => __( 'Straight', 'vamtam-elements-b' ),
						),
					),
					'border_size' => array(
						'type'    => 'select',
						'label'   => __( 'Border Size', 'vamtam-elements-b' ),
						'default' => 'wide',
						'options' => array(
							'large'  => _x( 'Large', 'Border size.', 'vamtam-elements-b' ),
							'medium' => _x( 'Medium', 'Border size.', 'vamtam-elements-b' ),
							'small'  => _x( 'Small', 'Border size.', 'vamtam-elements-b' ),
						),
					),
					'spacing' => array(
						'type'    => 'select',
						'label'   => __( 'Spacing', 'vamtam-elements-b' ),
						'default' => 'wide',
						'options' => array(
							'large'  => __( 'Large', 'vamtam-elements-b' ),
							'medium' => __( 'Medium', 'vamtam-elements-b' ),
							'none'   => __( 'None', 'vamtam-elements-b' ),
						),
					),
					'min_height' => array(
						'type'        => 'text',
						'label'       => __( 'Features Min Height', 'vamtam-elements-b' ),
						'default'     => '0',
						'size'        => '5',
						'description' => 'px',
						'help'        => __( 'Use this to normalize the height of your boxes when they have different numbers of features.', 'vamtam-elements-b' ),
					),
				),
			),
		),
	),
));

FLBuilder::register_settings_form('pricing_column_form', array(
	'title' => __( 'Add Pricing Box', 'vamtam-elements-b' ),
	'tabs'  => array(
		'general' => array(
			'title'    => __( 'General', 'vamtam-elements-b' ),
			'sections' => array(
				'title' => array(
					'title'  => __( 'Title', 'vamtam-elements-b' ),
					'fields' => array(
						'title' => array(
							'type'        => 'text',
							'label'       => __( 'Title', 'vamtam-elements-b' ),
							'vamtam-wpml' => 'LINE',
						),
						'title_size' => array(
							'type'        => 'text',
							'label'       => __( 'Title Size', 'vamtam-elements-b' ),
							'default'     => '24',
							'maxlength'   => '3',
							'size'        => '4',
							'description' => 'px',
						),
					),
				),
				'price-box' => array(
					'title'  => __( 'Price Box', 'vamtam-elements-b' ),
					'fields' => array(
						'price' => array(
							'type'        => 'text',
							'label'       => __( 'Price', 'vamtam-elements-b' ),
							'vamtam-wpml' => 'LINE',
						),
						'duration' => array(
							'type'        => 'text',
							'label'       => __( 'Duration', 'vamtam-elements-b' ),
							'placeholder' => __( 'per Year', 'vamtam-elements-b' ),
							'vamtam-wpml' => 'LINE',
						),
						'price_size' => array(
							'type'        => 'text',
							'label'       => __( 'Price Size', 'vamtam-elements-b' ),
							'default'     => '31',
							'maxlength'   => '3',
							'size'        => '4',
							'description' => 'px',
						),
					),
				),
				'features' => array(
					'title'  => _x( 'Features', 'Price features displayed in pricing box.', 'vamtam-elements-b' ),
					'fields' => array(
						'features' => array(
							'type'        => 'text',
							'label'       => '',
							'placeholder' => __( 'One feature per line. HTML is okay.', 'vamtam-elements-b' ),
							'multiple'    => true,
							'vamtam-wpml' => 'AREA',
						),
					),
				),
			),
		),
		'button' => array(
			'title'    => __( 'Button', 'vamtam-elements-b' ),
			'sections' => array(
				'default' => array(
					'title'  => '',
					'fields' => array(
						'button_text' => array(
							'type'    => 'text',
							'label'   => __( 'Button Text', 'vamtam-elements-b' ),
							'default' => __( 'Get Started', 'vamtam-elements-b' ),
						),
						'button_url' => array(
							'type'  => 'link',
							'label' => __( 'Button URL', 'vamtam-elements-b' ),
						),
						'btn_link_target' => array(
							'type'    => 'select',
							'label'   => __( 'Link Target', 'vamtam-elements-b' ),
							'default' => '_self',
							'options' => array(
								'_self'  => __( 'Same Window', 'vamtam-elements-b' ),
								'_blank' => __( 'New Window', 'vamtam-elements-b' ),
							),
							'preview' => array(
								'type' => 'none',
							),
						),
						'btn_icon' => array(
							'type'        => 'icon',
							'label'       => __( 'Button Icon', 'vamtam-elements-b' ),
							'show_remove' => true,
						),
						'btn_icon_position' => array(
							'type'    => 'select',
							'label'   => __( 'Button Icon Position', 'vamtam-elements-b' ),
							'default' => 'before',
							'options' => array(
								'before' => __( 'Before Text', 'vamtam-elements-b' ),
								'after'  => __( 'After Text', 'vamtam-elements-b' ),
							),
						),
					),
				),
				'btn_style' => array(
					'title'  => __( 'Button Style', 'vamtam-elements-b' ),
					'fields' => array(
						'btn_layout_type' => array(
							'type'    => 'select',
							'label'   => __( 'Button Type', 'vamtam-elements-b' ),
							'default' => 'solid',
							'options' => array(
								'solid'     => esc_html__( 'Solid', 'vamtam-elements-b' ),
								'border'    => esc_html__( 'Border', 'vamtam-elements-b' ),
								'underline' => esc_html__( 'Underline', 'vamtam-elements-b' ),
							),
						),
					),
				),
				'btn_colors' => array(
					'title'  => __( 'Button Colors', 'vamtam-elements-b' ),
					'fields' => array(
						'btn_color' => array(
							'type'    => 'select',
							'label'   => __( 'Normal Color', 'vamtam-elements-b' ),
							'default' => 'accent1',
							'options' => array(
								'accent1' => esc_html__( 'Accent 1', 'vamtam-elements-b' ),
								'accent2' => esc_html__( 'Accent 2', 'vamtam-elements-b' ),
								'accent3' => esc_html__( 'Accent 3', 'vamtam-elements-b' ),
								'accent4' => esc_html__( 'Accent 4', 'vamtam-elements-b' ),
								'accent5' => esc_html__( 'Accent 5', 'vamtam-elements-b' ),
								'accent6' => esc_html__( 'Accent 6', 'vamtam-elements-b' ),
								'accent7' => esc_html__( 'Accent 7', 'vamtam-elements-b' ),
								'accent8' => esc_html__( 'Accent 8', 'vamtam-elements-b' ),
							),
						),
						'btn_hover_color' => array(
							'type'    => 'select',
							'label'   => __( 'Hover Color', 'vamtam-elements-b' ),
							'default' => 'accent2',
							'options' => array(
								'accent1' => esc_html__( 'Accent 1', 'vamtam-elements-b' ),
								'accent2' => esc_html__( 'Accent 2', 'vamtam-elements-b' ),
								'accent3' => esc_html__( 'Accent 3', 'vamtam-elements-b' ),
								'accent4' => esc_html__( 'Accent 4', 'vamtam-elements-b' ),
								'accent5' => esc_html__( 'Accent 5', 'vamtam-elements-b' ),
								'accent6' => esc_html__( 'Accent 6', 'vamtam-elements-b' ),
								'accent7' => esc_html__( 'Accent 7', 'vamtam-elements-b' ),
								'accent8' => esc_html__( 'Accent 8', 'vamtam-elements-b' ),
							),
						),
					),
				),
				'btn_structure' => array(
					'title'  => __( 'Button Structure', 'vamtam-elements-b' ),
					'fields' => array(
						'btn_width' => array(
							'type'    => 'select',
							'label'   => __( 'Width', 'vamtam-elements-b' ),
							'default' => 'full',
							'options' => array(
								'auto' => _x( 'Auto', 'Width.', 'vamtam-elements-b' ),
								'full' => __( 'Full Width', 'vamtam-elements-b' ),
							),
						),
						'btn_align' => array(
							'type'    => 'select',
							'label'   => __( 'Alignment', 'vamtam-elements-b' ),
							'default' => 'center',
							'options' => array(
								'left'   => __( 'Left', 'vamtam-elements-b' ),
								'center' => __( 'Center', 'vamtam-elements-b' ),
								'right'  => __( 'Right', 'vamtam-elements-b' ),
							),
							'preview' => array(
								'type' => 'none',
							),
						),
						'btn_font_size' => array(
							'type'        => 'text',
							'label'       => __( 'Font Size', 'vamtam-elements-b' ),
							'default'     => '16',
							'maxlength'   => '3',
							'size'        => '4',
							'description' => 'px',
						),
						'btn_padding' => array(
							'type'        => 'text',
							'label'       => __( 'Padding', 'vamtam-elements-b' ),
							'default'     => '12',
							'maxlength'   => '3',
							'size'        => '4',
							'description' => 'px',
						),
					),
				),
			),
		),
		'style' => array(
			'title'    => __( 'Style', 'vamtam-elements-b' ),
			'sections' => array(
				'style' => array(
					'title'  => 'Style',
					'fields' => array(
						'border' => array(
							'type'    => 'vamtam-color',
							'label'   => __( 'Box Border', 'vamtam-elements-b' ),
							'default' => '#F2F2F2',
						),
						'background' => array(
							'type'    => 'vamtam-color',
							'label'   => __( 'Box Background', 'vamtam-elements-b' ),
							'default' => '#ffffff',
						),
						'text_color' => array(
							'type'    => 'vamtam-color',
							'label'   => __( 'Text Color', 'vamtam-elements-b' ),
							'default' => '#cccccc',
						),
						'highlight_background' => array(
							'type'    => 'vamtam-color',
							'default' => '#66686b',
							'label'   => __( 'Highlight Background', 'vamtam-elements-b' ),
						),
						'highlight_color' => array(
							'type'    => 'vamtam-color',
							'default' => '#ffffff',
							'label'   => __( 'Highlight Text Color', 'vamtam-elements-b' ),
						),
						'margin' => array(
							'type'        => 'text',
							'label'       => __( 'Box Top Margin', 'vamtam-elements-b' ),
							'default'     => '0',
							'maxlength'   => '3',
							'size'        => '3',
							'description' => 'px',
						),
					),
				),
			),
		),
	),
));
