<div class="fl-subscribe-form fl-subscribe-form-<?php echo esc_attr( $settings->layout ) ?> fl-subscribe-form-name-<?php echo esc_attr( $settings->show_name ) ?> fl-form fl-clearfix" <?php if ( isset( $module->template_id ) ) echo 'data-template-id="' . esc_attr( $module->template_id ) . '" data-template-node-id="' . esc_attr( $module->template_node_id ) . '"'; ?>>

	<?php if ( 'show' == $settings->show_name ) : ?>
	<div class="fl-form-field">
		<input type="text" name="fl-subscribe-form-name" placeholder="<?php echo esc_attr_x( 'Name', 'First and last name.', 'vamtam-elements-b' ); ?>" />
		<div class="fl-form-error-message"><?php esc_html_e( 'Please enter your name.', 'vamtam-elements-b' ); ?></div>
	</div>
	<?php endif; ?>

	<div class="fl-form-field">
		<input type="text" name="fl-subscribe-form-email" placeholder="<?php esc_attr_e( 'Email Address', 'vamtam-elements-b' ); ?>" />
		<div class="fl-form-error-message"><?php esc_html_e( 'Please enter a valid email address.', 'vamtam-elements-b' ); ?></div>
	</div>

	<div class="fl-form-button" data-wait-text="<?php esc_attr_e( 'Please Wait...', 'vamtam-elements-b' ); ?>">
		<?php FLBuilder::render_module_html( 'vamtam-button', VamtamSubscribeFormModule::get_button_settings( $settings ) ); ?>
	</div>

	<div class="fl-form-error-message"><?php esc_html_e( 'Something went wrong. Please check your entries and try again.', 'vamtam-elements-b' ); ?></div>

</div>
