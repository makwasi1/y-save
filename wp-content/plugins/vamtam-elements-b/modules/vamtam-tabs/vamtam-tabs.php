<?php

/**
 * @class VamtamTabsModule
 */
class VamtamTabsModule extends FLBuilderModule {

	/**
	 * @method __construct
	 */
	public function __construct() {
		$path = trailingslashit( 'modules/' . basename( dirname( __FILE__ ) ) );

		parent::__construct(array(
			'name'            => __( 'Tabs', 'vamtam-elements-b' ),
			'description'     => __( 'Display a collection of tabbed content.', 'vamtam-elements-b' ),
			'category'        => __( 'VamTam Modules', 'vamtam-elements-b' ),
			'partial_refresh' => true,
			'dir'             => VAMTAMEL_B_DIR . $path,
			'url'             => VAMTAMEL_B_URL . $path,
		));
	}
}

/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('VamtamTabsModule', array(
	'items' => array(
		'title'    => __( 'Items', 'vamtam-elements-b' ),
		'sections' => array(
			'general' => array(
				'title'  => '',
				'fields' => array(
					'items' => array(
						'type'         => 'form',
						'label'        => __( 'Item', 'vamtam-elements-b' ),
						'form'         => 'items_form', // ID from registered form below
						'preview_text' => 'label', // Name of a field to use for the preview text
						'multiple'     => true,
					),
				),
			),
		),
	),
	'style' => array(
		'title'    => __( 'Style', 'vamtam-elements-b' ),
		'sections' => array(
			'general' => array(
				'title'  => '',
				'fields' => array(
					'layout' => array(
						'type'    => 'select',
						'label'   => __( 'Layout', 'vamtam-elements-b' ),
						'default' => 'horizontal',
						'options' => array(
							'horizontal' => __( 'Horizontal', 'vamtam-elements-b' ),
							'vertical'   => __( 'Vertical', 'vamtam-elements-b' ),
						),
					),
					'border_color' => array(
						'type'    => 'vamtam-color',
						'label'   => __( 'Border Color', 'vamtam-elements-b' ),
						'default' => 'accent7',
					),
				),
			),
		),
	),
));

/**
 * Register a settings form to use in the "form" field type above.
 */
FLBuilder::register_settings_form('items_form', array(
	'title' => __( 'Add Item', 'vamtam-elements-b' ),
	'tabs'  => array(
		'general' => array(
			'title'    => __( 'General', 'vamtam-elements-b' ),
			'sections' => array(
				'general' => array(
					'title'  => '',
					'fields' => array(
						'label' => array(
							'type'        => 'text',
							'label'       => __( 'Label', 'vamtam-elements-b' ),
							'vamtam-wpml' => 'line',
						),
					),
				),
				'content' => array(
					'title'  => __( 'Content', 'vamtam-elements-b' ),
					'fields' => array(
						'content' => array(
							'type'  => 'editor',
							'label' => '',
						),
					),
				),
			),
		),
	),
));
