<div class="fl-tabs fl-tabs-<?php echo esc_attr( $settings->layout ) ?> fl-clearfix">

	<div class="fl-tabs-labels fl-clearfix">
		<?php for ( $i = 0; $i < count( $settings->items ); $i++ ) : if ( ! is_object( $settings->items[ $i ] )) continue; ?>
		<div class="fl-tabs-label<?php if ($i == 0) echo ' fl-tab-active'; ?>" data-index="<?php echo esc_attr( $i )  ?>">
			<?php echo $settings->items[ $i ]->label; // xss ok ?>
		</div>
		<?php endfor; ?>
	</div>

	<div class="fl-tabs-panels fl-clearfix">
		<?php for ( $i = 0; $i < count( $settings->items ); $i++ ) : if ( ! is_object( $settings->items[ $i ] )) continue; ?>
		<div class="fl-tabs-panel"<?php if ( ! empty( $settings->id ) ) echo ' id="' . sanitize_html_class( $settings->id ) . '-' . esc_attr( $i ) . '"'; ?>>
			<div class="fl-tabs-panel-content fl-clearfix<?php if ( $i == 0 ) echo ' fl-tab-active'; ?>" data-index="<?php echo esc_attr( $i ) ?>">
				<?php echo $settings->items[ $i ]->content; // xss ok ?>
			</div>
		</div>
		<?php endfor; ?>
	</div>
</div>
