.fl-node-<?php echo esc_html( $id ) ?> .fl-tabs-label.fl-tab-active {
	border-color: <?php echo esc_html( vamtam_el_sanitize_accent( $settings->border_color ) ); ?>;
}

.fl-node-<?php echo esc_html( $id ) ?> .fl-tabs-panels,
.fl-node-<?php echo esc_html( $id ) ?> .fl-tabs-panel {
	border-color: <?php echo esc_html( vamtam_el_sanitize_accent( $settings->border_color ) ); ?>;
}