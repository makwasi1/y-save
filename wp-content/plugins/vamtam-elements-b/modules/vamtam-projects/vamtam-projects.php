<?php

/**
 * @class VamtamProjectsModule
 */
class VamtamProjectsModule extends FLBuilderModule {

	/**
	 * @method __construct
	 */
	public function __construct() {
		$path = trailingslashit( 'modules/' . basename( dirname( __FILE__ ) ) );

		parent::__construct(array(
			'name'            => esc_html__( 'Projects', 'vamtam-elements-b' ),
			'description'     => esc_html__( 'Display your Jetpack projects.', 'vamtam-elements-b' ),
			'category'        => esc_html__( 'VamTam Modules', 'vamtam-elements-b' ),
			'partial_refresh' => true,
			'editor_export'   => false,
			'enabled'         => class_exists( 'Jetpack_Portfolio' ),
			'dir'             => VAMTAMEL_B_DIR . $path,
			'url'             => VAMTAMEL_B_URL . $path,
		));
	}
}

/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('VamtamProjectsModule', array(
	'layout' => array(
		'title'    => __( 'Layout', 'vamtam-elements-b' ),
		'sections' => array(
			'general' => array(
				'title'  => '',
				'fields' => array(
					'layout' => array(
						'type'    => 'select',
						'label'   => __( 'Layout', 'vamtam-elements-b' ),
						'default' => 'small',

						'options' => array(
							'grid'       => esc_html__( 'Static', 'wpv' ),
							'mosaic'     => esc_html__( 'Masonry', 'wpv' ),
							'scrollable' => esc_html__( 'Scrollable', 'wpv' ),
						),
						'toggle' => array(
							'grid' => array(
								'sections' => array( 'grid' ),
								'fields'   => array( 'link_opens', 'type_filter', 'pagination' ),
							),
							'mosaic' => array(
								'sections' => array( 'grid' ),
								'fields'   => array( 'link_opens', 'type_filter', 'pagination', 'lightbox_button' ),
							),
							'scrollable' => array(
								'sections' => array( 'grid' ),
							),
						),
					),
					'hover_animation' => array(
						'label'   => esc_html__( 'Hover Animation Type', 'wpv' ),
						'default' => 'hover-animation-1',
						'type'    => 'select',
						'options' => array(
							'hover-animation-1'  => esc_html__( 'Type 1', 'vamtam-elements-b' ),
							'hover-animation-2'  => esc_html__( 'Type 2', 'vamtam-elements-b' ),
							'hover-animation-3'  => esc_html__( 'Type 3', 'vamtam-elements-b' ),
							'hover-animation-4'  => esc_html__( 'Type 4', 'vamtam-elements-b' ),
						),
					),
					'pagination' => array(
						'label'   => __( 'Pagination', 'vamtam-elements-b' ),
						'default' => 'true',
						'type'    => 'select',
						'options' => array(
							'true'  => esc_html__( 'On', 'vamtam-elements-b' ),
							'false' => esc_html__( 'Off', 'vamtam-elements-b' ),
						),
					),
					'posts_per_page' => array(
						'type'    => 'unit',
						'label'   => esc_html__( 'Projects Per Page', 'vamtam-elements-b' ),
						'default' => 10,
						'min'     => -1,
						'max'     => 100,
					),
					'type_filter' => array(
						'label'   => esc_html__( 'Type Filter', 'wpv' ),
						'default' => 'false',
						'type'    => 'select',
						'options' => array(
							'true'  => esc_html__( 'On', 'vamtam-elements-b' ),
							'false' => esc_html__( 'Off', 'vamtam-elements-b' ),
						),
						'toggle' => array(
							'true' => array(
								'fields' => array( 'title_filter' ),
							),
						),
					),
					'title_filter' => array(
						'label'   => esc_html__( 'Title Filter', 'wpv' ),
						'default' => 'false',
						'type'    => 'select',
						'options' => array(
							'true'  => esc_html__( 'On', 'vamtam-elements-b' ),
							'false' => esc_html__( 'Off', 'vamtam-elements-b' ),
						),
					),
					'lightbox_button' => array(
						'label'   => esc_html__( 'Lightbox Button', 'wpv' ),
						'default' => 'true',
						'type'    => 'select',
						'options' => array(
							'true'  => esc_html__( 'On', 'vamtam-elements-b' ),
							'false' => esc_html__( 'Off', 'vamtam-elements-b' ),
						),
					),
				),
			),
			'grid' => array(
				'title'  => esc_html__( 'Grid', 'vamtam-elements-b' ),
				'fields' => array(
					'columns' => array(
						'type'    => 'unit',
						'label'   => esc_html__( 'Columns', 'vamtam-elements-b' ),
						'default' => 3,
						'min'     => 2,
						'max'     => 4,
					),
					'gap' => array(
						'type'    => 'select',
						'label'   => esc_html__( 'Gap Between Items', 'vamtam-elements-b' ),
						'default' => 'false',
						'options' => array(
							'true'  => esc_html__( 'On', 'vamtam-elements-b' ),
							'false' => esc_html__( 'Off', 'vamtam-elements-b' ),
						),
					),
				),
			),
			'content' => array(
				'title'  => esc_html__( 'Content', 'vamtam-elements-b' ),
				'fields' => array(
					'link_opens' => array(
						'label'   => esc_html__( 'Item Link Opens', 'wpv' ),
						'default' => 'single',
						'type'    => 'select',
						'options' => array(
							'single' => esc_html__( 'Single project page', 'wpv' ),
							'ajax'   => esc_html__( 'Simplified project page in a modal', 'wpv' ),
						),
					),

					'show_title' => array(
						'label'   => esc_html__( 'Title', 'wpv' ),
						'desc'    => esc_html__( 'If the option is on, it will display the title of the project.', 'wpv' ),
						'default' => 'false',
						'type'    => 'select',
						'options' => array(
							'0' => esc_html__( 'Hide', 'wpv' ),
							'1' => esc_html__( 'Show', 'wpv' ),
						),
					),

					'description' => array(
						'label'   => esc_html__( 'Description', 'wpv' ),
						'desc'    => esc_html__( 'If the option is on, it will display short description of the project.', 'wpv' ),
						'default' => 'false',
						'type'    => 'select',
						'options' => array(
							'0' => esc_html__( 'Hide', 'wpv' ),
							'1' => esc_html__( 'Show', 'wpv' ),
						),
					),

				),
			),
		),
	),
	'content' => array(
		'title' => esc_html__( 'Query', 'vamtam-elements-b' ),
		'file'  => plugin_dir_path( __FILE__ ) . 'includes/loop-settings.php',
	),
));

