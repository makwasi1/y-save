(function($){

	FLBuilder.registerModuleHelper('vamtam-iframe', {
		rules: {
			src: {
				required: true
			},
			height: {
				required: true,
				number: true
			},
			width: {
				required: true,
			}
		},

		init: function() {
			const height = document.querySelector( '.fl-builder-settings input[name=height]' );
			const width  = document.querySelector( '.fl-builder-settings input[name=width]' );
			const src    = document.querySelector( '.fl-builder-settings input[name=src]' );

			height.addEventListener( 'change', this._previewHeight.bind( this ) );
			height.addEventListener( 'keyup', this._previewHeight.bind( this ) );

			width.addEventListener( 'change', this._previewWidth.bind( this ) );
			width.addEventListener( 'keyup', this._previewWidth.bind( this ) );

			src.addEventListener( 'keyup', function() {
				this._getIframe().src = document.querySelector( '.fl-builder-settings input[name=src]' ).value;
			}.bind( this ) );
		},

		_previewHeight: function() {
			const height = document.querySelector( '.fl-builder-settings input[name=height]' ).value;

			if ( ! isNaN( height ) ) {
				this._getIframe().height = height + 'px';
			}
		},

		_previewWidth: function() {
			const width = document.querySelector( '.fl-builder-settings input[name=width]' ).value;

			if ( ! isNaN( width ) ) {
				this._getIframe().width = width;
			}
		},

		_getIframe: function() {
			return document.querySelector( FLBuilder.preview.classes.node + ' iframe' );
		}
	});

})(jQuery);
