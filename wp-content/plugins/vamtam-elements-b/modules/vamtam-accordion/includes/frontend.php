<?php $border_color = empty( $settings->border_color ) ? 'var( --vamtam-default-line-color )' : vamtam_el_sanitize_accent( $settings->border_color ); ?>
<?php $open_first = (isset( $settings->open_first ) && $settings->open_first) ? 'fl-accordion-open-first' : ''; ?>
<div class="fl-accordion fl-accordion-<?php echo esc_attr( $settings->label_size ); if ( $settings->collapse ) echo ' fl-accordion-collapse'; ?> <?php echo esc_attr( $open_first ) ?>">
	<?php for ( $i = 0; $i < count( $settings->items ); $i++ ) : if ( empty( $settings->items[ $i ] ) ) continue; ?>
	<div class="fl-accordion-item"<?php if ( ! empty( $settings->id ) ) echo ' id="' . sanitize_html_class( $settings->id ) . '-' . (int)$i . '"'; ?> style="border-color:<?php echo esc_attr( $border_color ) ?>">
		<div class="fl-accordion-button">
			<span class="fl-accordion-button-label"><?php echo $settings->items[ $i ]->label // xss ok ?></span>
			<i class="fl-accordion-button-icon vamtam-theme-plus"></i>
		</div>
		<div class="fl-accordion-content fl-clearfix"><?php echo $settings->items[ $i ]->content // xss ok ?></div>
	</div>
	<?php endfor; ?>
</div>
