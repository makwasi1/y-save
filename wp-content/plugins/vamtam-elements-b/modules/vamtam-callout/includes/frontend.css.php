<?php

if ( $settings->cta_type == 'button' ) {
	FLBuilder::render_module_css( 'vamtam-button', $id, VamtamCalloutModule::get_button_settings( $settings ) );
}

if ( $settings->image_type == 'icon' ) {
	FLBuilder::render_module_css('vamtam-icon', $id, array(
		'align'          => '',
		'bg_color'       => $settings->icon_bg_color,
		'bg_hover_color' => $settings->icon_bg_hover_color,
		'color'          => $settings->icon_color,
		'hover_color'    => $settings->icon_hover_color,
		'icon'           => $settings->icon,
		'link'           => $settings->link,
		'link_target'    => $settings->link_target,
		'size'           => $settings->icon_size,
		'text'           => '',
	));
}

?>
<?php if ( $settings->title_size == 'custom' ) : ?>
.fl-builder-content .fl-node-<?php echo $id; ?> .fl-callout-title {
	font-size: <?php echo $settings->title_custom_size; ?>px;
	line-height: <?php echo $settings->title_custom_size; ?>px;
}
<?php endif; ?>

<?php if ( $global_settings->responsive_enabled && ($settings->r_align == 'custom') ) : ?>
	@media screen and (max-width: <?php echo $global_settings->responsive_breakpoint; ?>px) {
		.fl-node-<?php echo $id; ?> .fl-callout-photo-horizontal {
			display: block;
		}
		.fl-node-<?php echo $id; ?> .fl-callout-photo-horizontal .fl-callout-photo,
		.fl-node-<?php echo $id; ?> .fl-callout-photo-horizontal .fl-callout-content {
			display: block;
			padding-left: 0;
			padding-right: 0;
			width: auto;
		}
		.fl-node-<?php echo $id; ?> .fl-callout-photo-left .fl-callout-photo,
		.fl-node-<?php echo $id; ?> .fl-callout-photo-left-text .fl-callout-photo {
			margin-bottom: 15px;
		}
		.fl-node-<?php echo $id; ?> .fl-callout-photo-right .fl-callout-photo,
		.fl-node-<?php echo $id; ?> .fl-callout-photo-right-text .fl-callout-photo {
			margin-top: 25px;
		}
	}
<?php endif ?>
