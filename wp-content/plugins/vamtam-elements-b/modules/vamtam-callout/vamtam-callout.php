<?php

/**
 * @class VamtamCalloutModule
 */
class VamtamCalloutModule extends FLBuilderModule {

	/**
	 * @method __construct
	 */
	public function __construct() {
		$path = trailingslashit( 'modules/' . basename( dirname( __FILE__ ) ) );

		parent::__construct(array(
			'name'            => __( 'Callout', 'vamtam-elements-b' ),
			'description'     => __( 'A heading and snippet of text with an optional link, icon and image.', 'vamtam-elements-b' ),
			'category'        => __( 'VamTam Modules', 'vamtam-elements-b' ),
			'partial_refresh' => true,
			'dir'             => VAMTAMEL_B_DIR . $path,
			'url'             => VAMTAMEL_B_URL . $path,
		));
	}

	public function enqueue_scripts() {
		$this->add_css( 'vamtam-callout' );
	}

	/**
	 * @method update
	 * @param $settings {object}
	 */
	public function update( $settings ) {

		// Cache the photo data.
		if ( ! empty( $settings->photo ) ) {

			$data = FLBuilderPhoto::get_attachment_data( $settings->photo );

			if ( $data ) {
				$settings->photo_data = $data;
			}
		}

		return $settings;
	}

	/**
	 * @method delete
	 */
	public function delete() {

		// Delete photo module cache.
		if ( $this->settings->image_type == 'photo' && ! empty( $this->settings->photo_src ) ) {
			$module_class                         = get_class( FLBuilderModel::$modules['photo'] );
			$photo_module                         = new $module_class();
			$photo_module->settings               = new stdClass();
			$photo_module->settings->photo_source = 'library';
			$photo_module->settings->photo_src    = $this->settings->photo_src;
			$photo_module->settings->crop         = $this->settings->photo_crop;
			$photo_module->delete();
		}
	}

	/**
	 * @method get_classname
	 */
	public function get_classname() {

		$classname = 'fl-callout fl-callout-' . $this->settings->align;

		if ( $this->settings->image_type == 'photo' ) {
			$classname .= ' fl-callout-has-photo fl-callout-photo-' . $this->settings->photo_position;

			if ( preg_match( '/left|right/', $this->settings->photo_position ) ) {
				$classname .= ' fl-callout-photo-horizontal';
			}
		} elseif ( $this->settings->image_type == 'icon' ) {
			$classname .= ' fl-callout-has-icon fl-callout-icon-' . $this->settings->icon_position;
		}

		return $classname;
	}

	/**
	 * @method render_title
	 */
	public function render_title() {

		echo '<' . esc_html( $this->settings->title_tag ) . ' class="fl-callout-title">';

		$this->render_image( 'left-title' );

		echo '<span>';

		if ( ! empty( $this->settings->link ) ) {
			echo '<a href="' . esc_url( $this->settings->link ) . '" target="' . esc_attr( $this->settings->link_target ) . '" class="fl-callout-title-link">';
		}

		echo $this->settings->title; // xss ok

		if ( ! empty( $this->settings->link ) ) {
			echo '</a>';
		}

		echo '</span>';

		$this->render_image( 'right-title' );

		echo '</' . esc_html( $this->settings->title_tag ) . '>';
	}

	/**
	 * @method render_text
	 */
	public function render_text() {

		echo '<div class="fl-callout-text">' . $this->settings->text . '</div>'; // xss ok
	}

	/**
	 * @method render_link
	 */
	public function render_link() {

		if ( $this->settings->cta_type == 'link' ) {
			echo '<a href="' . esc_url( $this->settings->link ) . '" target="' . esc_attr( $this->settings->link_target ) . '" class="fl-callout-cta-link">' . $this->settings->cta_text . '</a>'; // xss ok
		}
	}

	public static function get_button_settings( $settings ) {
		return array(
			'align'          => '',
			'color'          => $settings->btn_color,
			'hover_color'    => $settings->btn_hover_color,
			'font_size'      => $settings->btn_font_size,
			'icon'           => $settings->btn_icon,
			'icon_position'  => $settings->btn_icon_position,
			'link'           => $settings->link,
			'link_target'    => $settings->link_target,
			'padding'        => $settings->btn_padding,
			'layout_type'    => $settings->btn_layout_type,
			'text'           => $settings->cta_text,
			'width'          => $settings->btn_width,
		);
	}

	/**
	 * @method render_button
	 */
	public function render_button() {
		if ( $this->settings->cta_type == 'button' ) {
			echo '<div class="fl-callout-button">';
			FLBuilder::render_module_html( 'vamtam-button', self::get_button_settings( $this->settings ) );
			echo '</div>';
		}
	}

	/**
	 * @method render_image
	 */
	public function render_image( $position ) {

		if ( $this->settings->image_type == 'photo' && $this->settings->photo_position == $position ) {

			if ( empty( $this->settings->photo ) ) {
				return;
			}

			$photo_data = FLBuilderPhoto::get_attachment_data( $this->settings->photo );

			if ( ! $photo_data ) {
				$photo_data = $this->settings->photo_data;
			}

			$photo_settings = array(
				'align'        => 'center',
				'crop'         => $this->settings->photo_crop,
				'link_target'  => $this->settings->link_target,
				'link_type'    => 'url',
				'link_url'     => $this->settings->link,
				'photo'        => $photo_data,
				'photo_src'    => $this->settings->photo_src,
				'photo_source' => 'library',
			);

			echo '<div class="fl-callout-photo">';
			FLBuilder::render_module_html( 'photo', $photo_settings );
			echo '</div>';
		} elseif ( $this->settings->image_type == 'icon' && $this->settings->icon_position == $position ) {

			$icon_settings = array(
				'bg_color'        => $this->settings->icon_bg_color,
				'bg_hover_color'  => $this->settings->icon_bg_hover_color,
				'color'           => $this->settings->icon_color,
				'exclude_wrapper' => true,
				'hover_color'     => $this->settings->icon_hover_color,
				'icon'            => $this->settings->icon,
				'link'            => $this->settings->link,
				'link_target'     => $this->settings->link_target,
				'size'            => $this->settings->icon_size,
				'text'            => '',
			);

			FLBuilder::render_module_html( 'vamtam-icon', $icon_settings );
		}
	}
}

/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('VamtamCalloutModule', array(
	'general' => array(
		'title'    => __( 'General', 'vamtam-elements-b' ),
		'sections' => array(
			'title' => array(
				'title'  => '',
				'fields' => array(
					'title' => array(
						'type'    => 'text',
						'label'   => __( 'Heading', 'vamtam-elements-b' ),
						'preview' => array(
							'type'     => 'text',
							'selector' => '.fl-callout-title',
						),
					),
				),
			),
			'text' => array(
				'title'  => __( 'Text', 'vamtam-elements-b' ),
				'fields' => array(
					'text' => array(
						'type'          => 'editor',
						'label'         => '',
						'media_buttons' => false,
						'preview'       => array(
							'type'     => 'text',
							'selector' => '.fl-callout-text',
						),
					),
				),
			),
		),
	),
	'style' => array(
		'title'    => __( 'Style', 'vamtam-elements-b' ),
		'sections' => array(
			'overall_structure' => array(
				'title'  => __( 'Structure', 'vamtam-elements-b' ),
				'fields' => array(
					'align' => array(
						'type'    => 'select',
						'label'   => __( 'Overall Alignment', 'vamtam-elements-b' ),
						'default' => 'left',
						'options' => array(
							'center' => __( 'Center', 'vamtam-elements-b' ),
							'left'   => __( 'Left', 'vamtam-elements-b' ),
							'right'  => __( 'Right', 'vamtam-elements-b' ),
						),
						'help'    => __( 'The alignment that will apply to all elements within the callout.', 'vamtam-elements-b' ),
						'preview' => array(
							'type' => 'none',
						),
					),
				),
			),
			'title_structure' => array(
				'title'  => __( 'Heading Structure', 'vamtam-elements-b' ),
				'fields' => array(
					'title_tag' => array(
						'type'    => 'select',
						'label'   => __( 'Heading Tag', 'vamtam-elements-b' ),
						'default' => 'h3',
						'options' => array(
							'h1' => 'h1',
							'h2' => 'h2',
							'h3' => 'h3',
							'h4' => 'h4',
							'h5' => 'h5',
							'h6' => 'h6',
						),
					),
					'title_size' => array(
						'type'    => 'select',
						'label'   => __( 'Heading Size', 'vamtam-elements-b' ),
						'default' => 'default',
						'options' => array(
							'default' => __( 'Default', 'vamtam-elements-b' ),
							'custom'  => __( 'Custom', 'vamtam-elements-b' ),
						),
						'toggle' => array(
							'custom' => array(
								'fields' => array( 'title_custom_size' ),
							),
						),
					),
					'title_custom_size' => array(
						'type'        => 'text',
						'label'       => __( 'Heading Custom Size', 'vamtam-elements-b' ),
						'default'     => '24',
						'maxlength'   => '3',
						'size'        => '4',
						'description' => 'px',
					),
				),
			),
		),
	),
	'image' => array(
		'title'    => __( 'Image', 'vamtam-elements-b' ),
		'sections' => array(
			'general' => array(
				'title'  => '',
				'fields' => array(
					'image_type' => array(
						'type'    => 'select',
						'label'   => __( 'Image Type', 'vamtam-elements-b' ),
						'default' => 'photo',
						'options' => array(
							'none'  => _x( 'None', 'Image type.', 'vamtam-elements-b' ),
							'photo' => __( 'Photo', 'vamtam-elements-b' ),
							'icon'  => __( 'Icon', 'vamtam-elements-b' ),
						),
						'toggle' => array(
							'none'  => array(),
							'photo' => array(
								'sections' => array( 'photo' ),
							),
							'icon' => array(
								'sections' => array( 'icon', 'icon_colors', 'icon_structure' ),
							),
						),
					),
				),
			),
			'photo' => array(
				'title'  => __( 'Photo', 'vamtam-elements-b' ),
				'fields' => array(
					'photo' => array(
						'type'  => 'photo',
						'label' => __( 'Photo', 'vamtam-elements-b' ),
					),
					'photo_crop' => array(
						'type'    => 'select',
						'label'   => __( 'Crop', 'vamtam-elements-b' ),
						'default' => '',
						'options' => array(
							''          => _x( 'None', 'Photo Crop.', 'vamtam-elements-b' ),
							'landscape' => __( 'Landscape', 'vamtam-elements-b' ),
							'panorama'  => __( 'Panorama', 'vamtam-elements-b' ),
							'portrait'  => __( 'Portrait', 'vamtam-elements-b' ),
							'square'    => __( 'Square', 'vamtam-elements-b' ),
							'circle'    => __( 'Circle', 'vamtam-elements-b' ),
						),
					),
					'photo_position' => array(
						'type'    => 'select',
						'label'   => __( 'Position', 'vamtam-elements-b' ),
						'default' => 'above-title',
						'options' => array(
							'above-title' => __( 'Above Heading', 'vamtam-elements-b' ),
							'below-title' => __( 'Below Heading', 'vamtam-elements-b' ),
							'left'        => __( 'Left of Text and Heading', 'vamtam-elements-b' ),
							'right'       => __( 'Right of Text and Heading', 'vamtam-elements-b' ),
						),
					),
				),
			),
			'icon' => array(
				'title'  => __( 'Icon', 'vamtam-elements-b' ),
				'fields' => array(
					'icon' => array(
						'type'  => 'icon',
						'label' => __( 'Icon', 'vamtam-elements-b' ),
					),
					'icon_position' => array(
						'type'    => 'select',
						'label'   => __( 'Position', 'vamtam-elements-b' ),
						'default' => 'left-title',
						'options' => array(
							'above-title' => __( 'Above Heading', 'vamtam-elements-b' ),
							'below-title' => __( 'Below Heading', 'vamtam-elements-b' ),
							'left-title'  => __( 'Left of Heading', 'vamtam-elements-b' ),
							'right-title' => __( 'Right of Heading', 'vamtam-elements-b' ),
							'left'        => __( 'Left of Text and Heading', 'vamtam-elements-b' ),
							'right'       => __( 'Right of Text and Heading', 'vamtam-elements-b' ),
						),
					),
				),
			),
			'icon_colors' => array(
				'title'  => __( 'Icon Colors', 'vamtam-elements-b' ),
				'fields' => array(
					'icon_color' => array(
						'type'  => 'vamtam-color',
						'label' => __( 'Color', 'vamtam-elements-b' ),
					),
					'icon_hover_color' => array(
						'type'    => 'vamtam-color',
						'label'   => __( 'Hover Color', 'vamtam-elements-b' ),
						'preview' => array(
							'type' => 'none',
						),
					),
					'icon_bg_color' => array(
						'type'  => 'vamtam-color',
						'label' => __( 'Background Color', 'vamtam-elements-b' ),
					),
					'icon_bg_hover_color' => array(
						'type'    => 'vamtam-color',
						'label'   => __( 'Background Hover Color', 'vamtam-elements-b' ),
						'preview' => array(
							'type' => 'none',
						),
					),
				),
			),
			'icon_structure' => array(
				'title'  => __( 'Icon Structure', 'vamtam-elements-b' ),
				'fields' => array(
					'icon_size' => array(
						'type'        => 'text',
						'label'       => __( 'Size', 'vamtam-elements-b' ),
						'default'     => '30',
						'maxlength'   => '3',
						'size'        => '4',
						'description' => 'px',
					),
				),
			),
		),
	),
	'cta' => array(
		'title'    => __( 'Call To Action', 'vamtam-elements-b' ),
		'sections' => array(
			'link' => array(
				'title'  => __( 'Link', 'vamtam-elements-b' ),
				'fields' => array(
					'link' => array(
						'type'    => 'link',
						'label'   => __( 'Link', 'vamtam-elements-b' ),
						'help'    => __( 'The link applies to the entire module. If choosing a call to action type below, this link will also be used for the text or button.', 'vamtam-elements-b' ),
						'preview' => array(
							'type' => 'none',
						),
					),
					'link_target' => array(
						'type'    => 'select',
						'label'   => __( 'Link Target', 'vamtam-elements-b' ),
						'default' => '_self',
						'options' => array(
							'_self'  => __( 'Same Window', 'vamtam-elements-b' ),
							'_blank' => __( 'New Window', 'vamtam-elements-b' ),
						),
						'preview' => array(
							'type' => 'none',
						),
					),
				),
			),
			'cta' => array(
				'title'  => __( 'Call to Action', 'vamtam-elements-b' ),
				'fields' => array(
					'cta_type' => array(
						'type'    => 'select',
						'label'   => __( 'Type', 'vamtam-elements-b' ),
						'default' => 'none',
						'options' => array(
							'none'   => _x( 'None', 'Call to action.', 'vamtam-elements-b' ),
							'link'   => __( 'Text', 'vamtam-elements-b' ),
							'button' => __( 'Button', 'vamtam-elements-b' ),
						),
						'toggle' => array(
							'none' => array(),
							'link' => array(
								'fields' => array( 'cta_text' ),
							),
							'button' => array(
								'fields'   => array( 'cta_text', 'btn_icon', 'btn_icon_position' ),
								'sections' => array( 'btn_style', 'btn_colors', 'btn_structure' ),
							),
						),
					),
					'cta_text' => array(
						'type'    => 'text',
						'label'   => __( 'Text', 'vamtam-elements-b' ),
						'default' => __( 'Read More', 'vamtam-elements-b' ),
					),
					'btn_icon' => array(
						'type'        => 'icon',
						'label'       => __( 'Button Icon', 'vamtam-elements-b' ),
						'show_remove' => true,
					),
					'btn_icon_position' => array(
						'type'    => 'select',
						'label'   => __( 'Button Icon Position', 'vamtam-elements-b' ),
						'default' => 'before',
						'options' => array(
							'before' => __( 'Before Text', 'vamtam-elements-b' ),
							'after'  => __( 'After Text', 'vamtam-elements-b' ),
						),
					),
				),
			),
			'btn_colors' => array(
				'title'  => __( 'Button Colors', 'vamtam-elements-b' ),
				'fields' => array(
					'btn_color' => array(
						'type'    => 'select',
						'label'   => __( 'Normal Color', 'vamtam-elements-b' ),
						'default' => 'accent1',
						'options' => array(
							'accent1' => esc_html__( 'Accent 1', 'vamtam-elements-b' ),
							'accent2' => esc_html__( 'Accent 2', 'vamtam-elements-b' ),
							'accent3' => esc_html__( 'Accent 3', 'vamtam-elements-b' ),
							'accent4' => esc_html__( 'Accent 4', 'vamtam-elements-b' ),
							'accent5' => esc_html__( 'Accent 5', 'vamtam-elements-b' ),
							'accent6' => esc_html__( 'Accent 6', 'vamtam-elements-b' ),
							'accent7' => esc_html__( 'Accent 7', 'vamtam-elements-b' ),
							'accent8' => esc_html__( 'Accent 8', 'vamtam-elements-b' ),
						),
					),
					'btn_hover_color' => array(
						'type'    => 'select',
						'label'   => __( 'Hover Color', 'vamtam-elements-b' ),
						'default' => 'accent2',
						'options' => array(
							'accent1' => esc_html__( 'Accent 1', 'vamtam-elements-b' ),
							'accent2' => esc_html__( 'Accent 2', 'vamtam-elements-b' ),
							'accent3' => esc_html__( 'Accent 3', 'vamtam-elements-b' ),
							'accent4' => esc_html__( 'Accent 4', 'vamtam-elements-b' ),
							'accent5' => esc_html__( 'Accent 5', 'vamtam-elements-b' ),
							'accent6' => esc_html__( 'Accent 6', 'vamtam-elements-b' ),
							'accent7' => esc_html__( 'Accent 7', 'vamtam-elements-b' ),
							'accent8' => esc_html__( 'Accent 8', 'vamtam-elements-b' ),
						),
					),
				),
			),
			'btn_style' => array(
				'title'  => __( 'Button Style', 'vamtam-elements-b' ),
				'fields' => array(
					'btn_layout_type' => array(
						'type'    => 'select',
						'label'   => __( 'Button Type', 'vamtam-elements-b' ),
						'default' => 'solid',
						'options' => array(
							'solid'     => esc_html__( 'Solid', 'vamtam-elements-b' ),
							'border'    => esc_html__( 'Border', 'vamtam-elements-b' ),
							'underline' => esc_html__( 'Underline', 'vamtam-elements-b' ),
						),
					),
				),
			),
			'btn_structure' => array(
				'title'  => __( 'Button Structure', 'vamtam-elements-b' ),
				'fields' => array(
					'btn_width' => array(
						'type'    => 'select',
						'label'   => __( 'Button Width', 'vamtam-elements-b' ),
						'default' => 'auto',
						'options' => array(
							'auto' => _x( 'Auto', 'Width.', 'vamtam-elements-b' ),
							'full' => __( 'Full Width', 'vamtam-elements-b' ),
						),
					),
					'btn_font_size' => array(
						'type'        => 'text',
						'label'       => __( 'Font Size', 'vamtam-elements-b' ),
						'default'     => '14',
						'maxlength'   => '3',
						'size'        => '4',
						'description' => 'px',
					),
					'btn_padding' => array(
						'type'        => 'text',
						'label'       => __( 'Padding', 'vamtam-elements-b' ),
						'default'     => '10',
						'maxlength'   => '3',
						'size'        => '4',
						'description' => 'px',
					),
				),
			),
		),
	),
));
