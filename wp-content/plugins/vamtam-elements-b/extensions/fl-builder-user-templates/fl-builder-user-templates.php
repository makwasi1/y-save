<?php

// Defines
define( 'VAMTAMEL_B_USER_TEMPLATES_DIR', VAMTAMEL_B_DIR . 'extensions/fl-builder-user-templates/' );
define( 'VAMTAMEL_B_USER_TEMPLATES_URL', VAMTAMEL_B_URL . 'extensions/fl-builder-user-templates/' );

// Classes
require_once VAMTAMEL_B_USER_TEMPLATES_DIR . 'classes/class-fl-builder-user-templates.php';
require_once VAMTAMEL_B_USER_TEMPLATES_DIR . 'classes/class-fl-builder-user-templates-post-type.php';

// Admin Classes
if ( is_admin() ) {
	require_once VAMTAMEL_B_USER_TEMPLATES_DIR . 'classes/class-fl-builder-user-templates-admin-add.php';
	require_once VAMTAMEL_B_USER_TEMPLATES_DIR . 'classes/class-fl-builder-user-templates-admin-edit.php';
	require_once VAMTAMEL_B_USER_TEMPLATES_DIR . 'classes/class-fl-builder-user-templates-admin-list.php';
	require_once VAMTAMEL_B_USER_TEMPLATES_DIR . 'classes/class-fl-builder-user-templates-admin-menu.php';
	require_once VAMTAMEL_B_USER_TEMPLATES_DIR . 'classes/class-fl-builder-user-templates-admin-settings.php';
}
