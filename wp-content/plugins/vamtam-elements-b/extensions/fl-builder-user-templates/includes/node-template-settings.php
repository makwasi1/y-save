<?php

FLBuilder::register_settings_form('node_template', array(
	'tabs' => array(
		'general' => array(
			'title'    => __( 'General', 'vamtam-elements-b' ),
			'sections' => array(
				'general' => array(
					'title'  => '',
					'fields' => array(
						'name'   => array(
							'type'  => 'text',
							'label' => _x( 'Name', 'Template name.', 'vamtam-elements-b' ),
						),
						'global' => array(
							'type'    => 'select',
							'label'   => _x( 'Global', 'Whether this is a global row, column or module.', 'vamtam-elements-b' ),
							'help'    => __( 'Global rows, cols and modules can be added to multiple pages and edited in one place.', 'vamtam-elements-b' ),
							'default' => '0',
							'options' => array(
								'0' => __( 'No', 'vamtam-elements-b' ),
								'1' => __( 'Yes', 'vamtam-elements-b' ),
							),
						),
					),
				),
			),
		),
	),
));
