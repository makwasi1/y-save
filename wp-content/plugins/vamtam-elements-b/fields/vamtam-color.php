<#
	let classes = [ 'text', 'vamtam-color-picker-value' ];

	if ( data.field.class ) {
		classes.push( data.field.class );
	}

	if ( data.field.size ) {
		classes.push( 'text-full' );
	}

	if ( ! ( 'show_alpha' in data.field ) ) {
		data.field.show_alpha = true;
	}
#>
<div class="vamtam-color-picker <# if ( data.field.class ) { #> {{data.field.class}}<# } #>">
	<div class="vamtam-color-picker-color <# if ( '' === data.value ) { #> vamtam-color-picker-empty<# } #><# if ( data.field.show_alpha ) { #> vamtam-color-picker-alpha-enabled<# } #>"></div>
	<# if ( data.field.show_reset ) { #>
		<div class="vamtam-color-picker-clear"><div class="vamtam-color-picker-icon-remove"></div></div>
	<# } #>
	<input
		type="text"
		name="{{data.name}}"
		value="{{{data.value}}}"
		class="{{ classes.join( ' ' ) }}"
	/>
</div>
