<?php

class FLBuilderWhiteLabel {
	public static function is_white_labeled() {
		return true;
	}

	public static function get_branding() {
		return apply_filters( 'vamtam-beaver-branding', esc_html__( 'VamTam Builder', 'wpv' ) );
	}

	public static function get_branding_icon() {
		return apply_filters( 'vamtam-beaver-branding-logo', VAMTAMEL_B_URL . 'assets/logo.svg' );
	}

	public static function get_help_button_settings() {
		return array(
			'enabled'            => true,
			'tour'               => true,
			'video'              => true,
			'video_embed'        => '<iframe src="https://player.vimeo.com/video/124230072?autoplay=1" width="420" height="315" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>',
			'knowledge_base'     => true,
			'knowledge_base_url' => 'https://www.wpbeaverbuilder.com/knowledge-base/',
			'forums'             => true,
			'forums_url'         => 'http://support.vamtam.com/support/login',
		);
	}

	public static function plugins_page( $plugins ) {
		return $plugins;
	}

	public static function themes_page( $themes ) {
		return $themes;
	}

	public static function theme_gettext( $text ) {
		return $text;
	}
}
