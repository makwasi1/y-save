<?php

class Vamtam_Icons_List {
	public function __construct() {
		add_shortcode( 'vamtam_icons_list', array( __CLASS__, 'shortcode' ) );
	}

	public static function shortcode() {
		FLBuilderIcons::enqueue_all_custom_icons_styles();
		wp_enqueue_style( 'font-awesome' );
		wp_enqueue_style( 'foundation-icons' );
		wp_enqueue_style( 'dashicons' );

		$sets   = FLBuilderIcons::get_sets();
		$output = '';

		foreach ( $sets as $set ) {
			$output .= '<h3 class="textcenter">' . $set['name'] . '</h3>';
			$output .= '<table>';
			$output .= '<tr>';
			foreach ( $set['icons'] as $num => $icon ) {
				$output .= "<td><span class='{$set['prefix']} {$icon}' style='padding: 10px; font-size: 20px; line-height: 1'></span></td><td>{$icon}</td>";
				if ( $num % 3 == 2 ) {
					$output .= '</tr><tr>';
				}
			}
			$output .= '</tr>';
			$output .= '</table>';
		}

		return $output;
	}
}

new Vamtam_Icons_List();
