<?php
/**
 * single checkbox
 */

$option = $value;
$value  = $default;

// sanitize boolean values
if ( $value === '1' || $value === 'true' ) {
	$value = true;
}

if ( $value === '0' || $value === 'false' ) {
	$value = false;
}

?>

<div class="vamtam-config-row <?php echo esc_attr( $class ) ?>">
	<div class="ritlte">
	</div>

	<div class="rcontent clearfix">
		<label>
			<input type="checkbox" name="<?php echo esc_attr( $id ) ?>" id="<?php echo esc_attr( $id ) ?>" value="true" class="" <?php checked( $value, true ) ?> />
			<?php echo esc_html( $name ) ?>
		</label>
	</div>
</div>
