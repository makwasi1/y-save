<?php

/**
 * Plugin Name: VamTam Elements (B)
 * Plugin URI: http://vamtam.com
 * Description: Drag&Drop elements and shortcodes used in VamTam themes (requires Beaver Builder)
 * Version: 1.21.0
 * Author: VamTam
 * Author URI: http://vamtam.com
 */

define( 'VAMTAMEL_B_DIR', plugin_dir_path( __FILE__ ) );
define( 'VAMTAMEL_B_URL', plugins_url( '/', __FILE__ ) );

class Vamtam_Elements_B {

	public static $dir;
	public static $url;

	public static $lightboxes = array();

	public static function setup() {
		if ( ! class_exists( 'Vamtam_Updates_2' ) ) {
			require 'vamtam-updates/class-vamtam-updates.php';
		}

		new Vamtam_Updates_2( __FILE__ );

		// The include below may be split into its own plugin in the future
		// it is only added here to ease the migration
		include_once VAMTAMEL_B_DIR . 'metaboxes/vamtam-additional-content.php';

		// enable shortcodes in widgets

		add_filter( 'widget_text', 'do_shortcode' );
		add_filter( 'widget_title', 'do_shortcode' );


		// Beaver integration

		if ( ! class_exists( 'FLBuilderModel' ) ) {
			return;
		}

		add_action( 'init', array( __CLASS__, 'init' ) );
		add_action( 'admin_menu', array( __CLASS__, 'admin_menu' ), 100 );

		add_action( 'admin_init', array( __CLASS__, 'admin_init' ) );

		add_action( 'widgets_init', array( __CLASS__, 'widgets_init' ), 100 );

		// show premium buttons
		add_filter( 'fl_builder_ui_bar_buttons', array( __CLASS__, 'fl_builder_ui_bar_buttons' ) );
		add_filter( 'fl_builder_ui_js_config', array( __CLASS__, 'fl_builder_ui_js_config' ) );

		// remove row_width global option
		add_filter( 'option__fl_builder_settings', array( __CLASS__, 'option__fl_builder_settings' ) );
		add_filter( 'fl_builder_settings_form_defaults', array( __CLASS__, 'option__fl_builder_settings' ) );
		add_filter( 'fl_builder_register_settings_form', array( __CLASS__, 'fl_builder_register_settings_form' ), 10, 2 );

		// builder CSS
		add_action( 'wp_enqueue_scripts', array( __CLASS__, 'enqueue_scripts' ), 100 );
		add_action( 'wp_enqueue_scripts', array( __CLASS__, 'early_enqueue_scripts' ), 5 );

		// remove "upgrade" screen
		add_filter( 'fl_builder_admin_settings_nav_items', array( __CLASS__, 'fl_builder_admin_settings_nav_items' ) );
		add_filter( 'fl_builder_content_panel_data', array( __CLASS__, 'fl_builder_content_panel_data' ) );

		// no "premium" select options
		add_filter( 'fl_builder_render_settings_field', array( __CLASS__, 'fl_builder_render_settings_field' ), 10, 3 );

		// reenable the icon manager
		add_action( 'fl_builder_admin_settings_render_forms', array( __CLASS__, 'icon_font_manager' ) );

		// Add any custom font families to the typography options in Beaver
		add_filter( 'fl_builder_font_families_system', [ __CLASS__, 'fl_builder_font_families_system' ] );

		// disable some built-in modules
		add_filter( 'fl_builder_register_module', array( __CLASS__, 'fl_builder_register_module' ), 10, 2 );

		// override node attributes
		add_filter( 'fl_builder_module_attributes', array( __CLASS__, 'fl_builder_module_attributes' ), 10, 2 );

		// built-in module overrides
		add_filter( 'fl_builder_module_frontend_file', array( __CLASS__, 'fl_builder_module_frontend_file' ), 10, 2 );

		// override column attributes
		add_filter( 'fl_builder_column_attributes', array( __CLASS__, 'fl_builder_column_attributes' ), 10, 2 );

		// override row attributes
		add_filter( 'fl_builder_row_attributes', array( __CLASS__, 'fl_builder_row_attributes' ), 10, 2 );

		// Templates CPT arguments
		add_filter( 'fl_builder_register_template_post_type_args', array( __CLASS__, 'fl_builder_register_template_post_type_args' ) );

		// Some main menu items are disabled, enable them if we provide an alternative
		add_filter( 'fl_builder_main_menu', array( __CLASS__, 'fl_builder_main_menu' ) );
		add_filter( 'fl_builder_keyboard_shortcuts', array( __CLASS__, 'fl_builder_keyboard_shortcuts' ) );

		// custom icons
		add_filter( 'fl_builder_icon_sets', array( __CLASS__, 'fl_builder_icon_sets' ) );
		add_filter( 'fl_builder_enqueue_custom_styles_by_key', array( __CLASS__, 'fl_builder_enqueue_custom_styles_by_key' ), 10, 2 );

		// custom fields
		add_filter( 'fl_builder_custom_fields', array( __CLASS__, 'fl_builder_custom_fields' ) );

		if ( ! defined( 'VAMTAM_EL_DEFAULT_BRANDING') || ! VAMTAM_EL_DEFAULT_BRANDING ) {
			include_once VAMTAMEL_B_DIR . 'classes/white-label.php';
		}

		if ( ! class_exists( 'FLBuilderTemplateDataExporter' ) ) {
			include_once VAMTAMEL_B_DIR . 'classes/extensions.php';
		}

		include_once VAMTAMEL_B_DIR . 'helpers.php';
		include_once VAMTAMEL_B_DIR . 'wpml.php';

		$templates_path = VAMTAMEL_B_DIR . 'data/templates.dat';

		if ( file_exists( $templates_path ) ) {
			FLBuilderModel::register_templates( $templates_path );
		}
	}

	public static function is_beaver_used() {
		return class_exists( 'FLBuilderModel' ) && FLBuilderModel::is_builder_enabled() && ! is_archive();
	}

	public static function had_limit_wrapper() {
		return is_callable( [ 'VamtamTemplates', 'had_limit_wrapper' ] ) ? VamtamTemplates::had_limit_wrapper() : true;
	}

	/**
	 * Register modules and fields
	 */
	public static function init() {
		// regular shortcodes
		include VAMTAMEL_B_DIR . 'shortcodes/icons-list.php';

		// Beaver modules
		if ( class_exists( 'FLBuilder' ) ) {
			$modules = glob( VAMTAMEL_B_DIR . 'modules/*', GLOB_ONLYDIR );

			$modules = apply_filters( 'vamtam_elements_beaver_supported', array_map( 'basename', $modules ) );

			if ( is_array( $modules ) ) {
				foreach ( $modules as $module ) {
					$module_path = trailingslashit( $module ) . $module . '.php';

					$child_path  = get_stylesheet_directory() . '/fl-builder/modules/' . $module_path;
					$theme_path  = get_template_directory() . '/fl-builder/modules/' . $module_path;
					$plugin_path = VAMTAMEL_B_DIR . 'modules/' . $module_path;

					// Check for the module class in a child theme.
					if ( is_child_theme() && file_exists( $child_path ) ) {
						require_once $child_path;
					} elseif ( file_exists( $theme_path ) ) {
						require_once $theme_path;
					} elseif ( file_exists( $plugin_path ) ) {
						require_once $plugin_path;
					}
				}
			}
		}
	}

	public static function admin_init() {
		$beaver_settings = get_option( '_fl_builder_user_access', [] );

		foreach ( $beaver_settings as $category => $capabilities ) {
			// if all capabilities are set to false
			if ( count( array_unique( $capabilities ) ) === 1 && ! current( $capabilities ) ) {
				// reset capabilities to "all", which is the default
				foreach ( $beaver_settings[ $category ] as $role => $enabled ) {
					$beaver_settings[ $category ][ $role ] = true;
				}
			}
		}

		update_option( '_fl_builder_user_access', $beaver_settings );
	}

	/**
	 * Load widgets
	 */
	public static function widgets_init() {
		$widgets = apply_filters( 'vamtam-enabled-widgets', array(
			'beaver',
		) );

		foreach ( $widgets as $name ) {
			if ( ! class_exists( 'Vamtam_' . ucfirst( $name ) . '_Widget' ) ) {
				require_once VAMTAMEL_B_DIR . "widgets/$name.php";
			}
		}
	}

	/**
	 * Register custom setting fields
	 * @param  array $custom
	 * @return array
	 */
	public static function fl_builder_custom_fields( $custom ) {
		// Custom Beaver fields
		$fields = glob( VAMTAMEL_B_DIR . 'fields/*.php' );

		$fields = apply_filters( 'vamtam_elements_beaver_fields', $fields );
		$custom = array();

		if ( is_array( $fields ) ) {
			foreach ( $fields as $field_path ) {
				$custom[ basename( $field_path, '.php' ) ] = $field_path;
			}
		}

		return $custom;
	}

	/**
	 * Move the "VamTam Builder" admin page under "Appearance"
	 */
	public static function admin_menu() {
		global $submenu;

		if ( current_user_can( 'delete_users' ) ) {
			foreach ( $submenu as $li => $submenu_list ) {
				foreach ( $submenu_list as $si => $submenu_item ) {
					if ( isset( $submenu_item[2] ) && $submenu_item[2] === 'fl-builder-settings' ) {
						unset( $submenu[ $li ][ $si ] );
						break;
					}
				}
			}

			$title = FLBuilderModel::get_branding();

			add_submenu_page( 'edit.php?post_type=fl-builder-template', $title, esc_html__( 'Settings', 'vamtam-elements-b' ), 'delete_users', 'fl-builder-settings', array( 'FLBuilderAdminSettings', 'render' ) );
		}
	}

	/**
	 * Modify Beaver top UI bar
	 */
	public static function fl_builder_ui_bar_buttons( $bar ) {
		unset( $bar['upgrade'] );
		unset( $bar['buy'] );

		return $bar;
	}

	/**
	 * Modify Beaver JS config
	 */
	public static function fl_builder_ui_js_config( $config ) {
		$config['lite'] = false;

		return $config;
	}

	/**
	 * Modify global settings
	 */
	public static function option__fl_builder_settings( $value ) {
		global $vamtam_theme;

		if ( isset( $vamtam_theme['site-max-width'] ) ) {
			if ( is_array( $value ) && isset( $value['module_margins'] ) ) {
				$value['row_width']      = $vamtam_theme['site-max-width'] + 2 * $value['module_margins'];
				$value['row_width_unit'] = 'px';
			}

			if ( is_object( $value ) && isset( $value->module_margins ) ) {
				$value->row_width      = $vamtam_theme['site-max-width'] + 2 * $value->module_margins;
				$value->row_width_unit = 'px';
			}
		}

		if ( is_array( $value ) ) {
			$value['show_default_heading'] = '0';

			if ( ! isset( $value['css'] ) ) {
				$value['css'] = '';
			}

			if ( ! isset( $value['js'] ) ) {
				$value['js'] = '';
			}
		}

		if ( is_object( $value ) ) {
			$value->show_default_heading = '0';

			if ( ! isset( $value->css ) ) {
				$value->css = '';
			}

			if ( ! isset( $value->js ) ) {
				$value->js = '';
			}
		}

		return $value;
	}

	/**
	 * Modify global settings form
	 */
	public static function fl_builder_register_settings_form( $form, $id ) {
		if ( 'global' === $id ) {
			global $vamtam_theme;

			if ( isset( $vamtam_theme['site-max-width'] ) ) {
				// remove row_width option
				unset( $form['tabs']['general']['sections']['rows']['fields']['row_width'] );
			}

			// remove page_heading section
			unset( $form['tabs']['general']['sections']['page_heading'] );

			// remove css and js tabs
			unset( $form['tabs']['css'] );
			unset( $form['tabs']['js'] );
		} elseif ( 'row' === $id ) {
			unset( $form['tabs']['style']['sections']['background']['fields']['bg_type']['options']['slideshow'] );

			$form['tabs']['style']['sections']['bg_parallax']['fields']['bg_parallax_size'] = array(
				'type'    => 'select',
				'label'   => esc_html__( 'Background Scale', 'vamtam-elements-b' ),
				'default' => 'cover',
				'options' => array(
					'auto'    => esc_html( _x( 'None', 'Background scale.', 'vamtam-elements-b' ) ),
					'contain' => esc_html__( 'Fit', 'vamtam-elements-b' ),
					'cover'   => esc_html__( 'Fill', 'vamtam-elements-b' ),
				),
				'help'    => esc_html__( 'Scale applies to how the image should display in the background. You can select either fill or fit to the background.', 'vamtam-elements-b' ),
				'preview' => array(
					'type' => 'none',
				),
			);

			$form['tabs']['style']['sections']['bg_parallax']['fields']['bg_parallax_position'] = array(
				'type'    => 'select',
				'label'   => esc_html__( 'Background Position', 'vamtam-elements-b' ),
				'default' => 'left top',
				'options' => array(
					'left top'      => esc_html__( 'Left Top', 'fl-builder' ),
					'left center'   => esc_html__( 'Left Center', 'fl-builder' ),
					'left bottom'   => esc_html__( 'Left Bottom', 'fl-builder' ),
					'right top'     => esc_html__( 'Right Top', 'fl-builder' ),
					'right center'  => esc_html__( 'Right Center', 'fl-builder' ),
					'right bottom'  => esc_html__( 'Right Bottom', 'fl-builder' ),
					'center top'    => esc_html__( 'Center Top', 'fl-builder' ),
					'center center' => esc_html__( 'Center', 'fl-builder' ),
					'center bottom' => esc_html__( 'Center Bottom', 'fl-builder' ),
				),
				'help'    => esc_html__( 'Position will tell the image where it should sit in the background.', 'vamtam-elements-b' ),
				'preview' => array(
					'type' => 'none',
				),
			);
		} elseif ( 'photo' === $id ) {
			$form['general']['sections']['general']['fields']['vamtam_sizes_attr'] = array(
				'type'        => 'select',
				'default'     => 'beaver',
				'label'       => esc_html__( 'Visible Size', 'fl-builder' ),
				'help'        => esc_html__( 'Setting this option will improve load times for your site. Use it to make the browser download an image which is not larger than actually needed. If set to "As generated by WP Core", the browser will download the largest image size available.', 'vamtam-elements-b' ),
				'description' => esc_html__( 'Choose "Based on parent column size" if the image expands to the full width of the parent column/row', 'vamtam-elements-b' ),
				'preview'     => array(
					'type' => 'none',
				),
				'options'     => array(
					'default' => esc_html__( 'As generated by WP Core', 'vamtam-elements-b' ),
					'beaver'  => esc_html__( 'Based on parent column size', 'vamtam-elements-b' ),
					'custom'  => esc_html__( 'Custom value', 'vamtam-elements-b' ),
				),
				'toggle' => array(
					'custom' => array(
						'fields' => array( 'vamtam_sizes_attr_custom' ),
					),
				),
			);

			$form['general']['sections']['general']['fields']['vamtam_sizes_attr_custom'] = array(
				'type'    => 'text',
				'default' => '',
				'label'   => esc_html__( 'Custom sizes="" attribute value', 'fl-builder' ),
			);
		}

		if ( 'row' === $id || 'col' === $id || 'module_advanced' === $id ) {
			$animation_options = array(
				'animation' => array(
					'type'      => 'text',
					'label'     => '',
					'default'   => '',
					'row_class' => 'hidden',
					'preview'   => array(
						'type' => 'none',
					),
				),
				'vamtam_animation_style' => array(
					'type'    => 'select',
					'label'   => esc_html__( 'Style', 'vamtam-elements-b' ),
					'default' => '',
					'preview' => array(
						'type' => 'none',
					),
					'options' => array(
						'' => esc_html_x( 'None', 'Animation style.', 'vamtam-elements-b' ),

						'dummy' => esc_html_x( 'Pin only, no other animations', 'Animation style.', 'vamtam-elements-b' ),

						'rotate' => esc_html_x( 'Rotate 360°', 'Animation style.', 'vamtam-elements-b' ),
						'fade'   => esc_html_x( 'Fade In', 'Animation style.', 'vamtam-elements-b' ),

						'move-scale-in-from-top'    => esc_html_x( 'Move with Scale (Zoom In) - Top', 'Animation style.', 'vamtam-elements-b' ),
						'move-scale-in-from-bottom' => esc_html_x( 'Move with Scale (Zoom In) - Bottom', 'Animation style.', 'vamtam-elements-b' ),
						'move-scale-in-from-left'   => esc_html_x( 'Move with Scale (Zoom In) - Left', 'Animation style.', 'vamtam-elements-b' ),
						'move-scale-in-from-right'  => esc_html_x( 'Move with Scale (Zoom In) - Right', 'Animation style.', 'vamtam-elements-b' ),

						'move-scale-out-from-top'    => esc_html_x( 'Move with Scale (Zoom Out) - Top', 'Animation style.', 'vamtam-elements-b' ),
						'move-scale-out-from-bottom' => esc_html_x( 'Move with Scale (Zoom Out) - Bottom', 'Animation style.', 'vamtam-elements-b' ),
						'move-scale-out-from-left'   => esc_html_x( 'Move with Scale (Zoom Out) - Left', 'Animation style.', 'vamtam-elements-b' ),
						'move-scale-out-from-right'  => esc_html_x( 'Move with Scale (Zoom Out) - Right', 'Animation style.', 'vamtam-elements-b' ),

						'move-from-top'    => esc_html_x( 'Move from Top', 'Animation style.', 'vamtam-elements-b' ),
						'move-from-bottom' => esc_html_x( 'Move from Bottom', 'Animation style.', 'vamtam-elements-b' ),
						'move-from-left'   => esc_html_x( 'Move from Left', 'Animation style.', 'vamtam-elements-b' ),
						'move-from-right'  => esc_html_x( 'Move from Right', 'Animation style.', 'vamtam-elements-b' ),

						'scale-in'  => esc_html_x( 'Scale In', 'Animation style.', 'vamtam-elements-b' ),
						'scale-out' => esc_html_x( 'Scale Out', 'Animation style.', 'vamtam-elements-b' ),

					),
				),
				'vamtam_animation_type' => array(
					'type'    => 'select',
					'label'   => esc_html__( 'Type', 'vamtam-elements-b' ),
					'default' => 'progressive',
					'preview' => array(
						'type' => 'none',
					),
					'options' => array(
						'immediate'   => esc_html__( 'Immediate', 'vamtam-elements-b' ),
						'progressive' => esc_html__( 'Progressive', 'vamtam-elements-b' ),
					),
					'toggle' => array(
						'progressive' => array(
							'fields' => array( 'vamtam_animation_exit', 'vamtam_animation_pin' ),
						),
					),
				),
				'vamtam_animation_exit' => array(
					'type'    => 'select',
					'label'   => esc_html__( 'Exit Animation', 'vamtam-elements-b' ),
					'default' => 'true',
					'preview' => array(
						'type' => 'none',
					),
					'options' => array(
						'1' => esc_html__( 'Enabled', 'vamtam-elements-b' ),
						'0' => esc_html__( 'Disabled', 'vamtam-elements-b' ),
					),
				),
				'vamtam_animation_delay' => array(
					'type'  => 'text',
					'label' => esc_html__( 'Animation Delay', 'vamtam-elements-b' ),
					'help'  => esc_html__( 'Entry and exit animamations have a duration of 2. An entry-only animation has a duration of 1. The animation progresses from -1 (bottom of screen), to 0 (middle of screen), to 1 (top of screen). For example, if you want to make it so that 50% animation progress happens in the bottom 75% of the screen (as opposed to the middle), you can set a delay of -0.5.', 'vamtam-elements-b' ),
					'default'     => '0',
					'preview'     => array(
						'type' => 'none',
					),
					'min'  => -1,
					'max'  => 1,
					'step' => 0.01,
				),
				'vamtam_animation_origin' => array(
					'type'    => 'text',
					'label'   => esc_html__( 'Transform Origin', 'vamtam-elements-b' ),
					'default' => 'center center',
					'preview' => array(
						'type' => 'none',
					),
				),
				'vamtam_animation_mobile' => array(
					'type'    => 'select',
					'label'   => esc_html__( 'Animate on Narrow Screens', 'vamtam-elements-b' ),
					'default' => '0',
					'preview' => array(
						'type' => 'none',
					),
					'options' => array(
						'1' => esc_html__( 'Enabled', 'vamtam-elements-b' ),
						'0' => esc_html__( 'Disabled', 'vamtam-elements-b' ),
					),
				),
			);

			if ( current_theme_supports( 'vamtam-scroll-pinning' ) ) {
				$allowed_pin_types = [
					'0'      => esc_html__( 'Disabled', 'vamtam-elements-b' ),
					'parent' => esc_html__( 'Within Parent Row', 'vamtam-elements-b' ),
					'fixed'  => esc_html__( 'Fixed Height (px)', 'vamtam-elements-b' ),
				];

				if ( 'row' === $id ) {
					unset( $allowed_pin_types['parent'] );
				}

				$animation_options = array_merge( $animation_options, array(
					'vamtam_animation_pin' => array(
						'type'    => 'select',
						'label'   => esc_html__( 'Pin', 'vamtam-elements-b' ),
						'default' => '0',
						'preview' => array(
							'type' => 'none',
						),
						'options' => $allowed_pin_types,
						'toggle' => array(
							'fixed' => array(
								'fields' => array( 'vamtam_animation_pin_duration', 'vamtam_animation_pin_trigger' ),
							),
							'parent' => array(
								'fields' => array( 'vamtam_animation_pin_trigger' ),
							),
						),
					),
					'vamtam_animation_pin_duration' => array(
						'type'    => 'unit',
						'label'   => esc_html__( 'Pin', 'vamtam-elements-b' ),
						'default' => 300,
						'preview' => array(
							'type' => 'none',
						),
					),
					'vamtam_animation_pin_trigger' => array(
						'type'  => 'select',
						'label' => esc_html__( 'Pin Trigger', 'vamtam-elements-b' ),
						'help'  => esc_html__( 'Pinning will begin when the top/center/bottom of the element aligns with the top/center/bottom of the viewport.', 'vamtam-elements-b' ),
						'default' => 'center',
						'preview' => array(
							'type' => 'none',
						),
						'options' => array(
							'top'    => esc_html__( 'Top', 'vamtam-elements-b' ),
							'center' => esc_html__( 'Center', 'vamtam-elements-b' ),
							'bottom' => esc_html__( 'Bottom', 'vamtam-elements-b' ),
						),
					),
				) );
			}

			if ( 'module_advanced' === $id ) {
				if ( isset( $form['sections']['animation']['fields']['animation_delay'] ) ) {
					$form['sections']['animation']['fields']['animation_delay']['row_class'] = 'hidden';
				}

				$form['sections']['animation']['fields'] = array_merge( $form['sections']['animation']['fields'], $animation_options );
			} else {
				$form['tabs']['advanced']['sections']['animation'] = array(
					'title'  => esc_html__( 'Animation', 'fl-builder' ),
					'fields' => $animation_options,
				);
			}
		}

		return $form;
	}

	/**
	 * Enqueue scripts and styles - lower priority
	 */
	public static function early_enqueue_scripts() {
		wp_register_script( 'vamtam-fl-subscribe-form', VAMTAMEL_B_URL . 'assets/fl-subscribe-form.js', array( 'jquery' ), false, true );
		wp_register_script( 'vamtam-fl-photo', VAMTAMEL_B_URL . 'assets/fl-photo.js', array( 'jquery' ), false, true );
		wp_register_script( 'vamtam-accordion', VAMTAMEL_B_URL . 'assets/vamtam-accordion.js', array( 'jquery' ), false, true );
		wp_register_script( 'vamtam-numbers', VAMTAMEL_B_URL . 'assets/vamtam-numbers.js', array(), false, true );

		wp_register_style( 'vamtam-callout', VAMTAMEL_B_URL . 'assets/vamtam-callout.css', [], '1.0.0' );
	}

	/**
	 * Enqueue scripts and styles
	 */
	public static function enqueue_scripts() {
		if ( FLBuilderModel::is_builder_active() ) {
			global $vamtam_theme;

			wp_enqueue_style( 'vamtam-elements-editor-styles', VAMTAMEL_B_URL . 'assets/builder.css', array( 'fl-builder-min' ), '1.0.0' );
			wp_enqueue_style( 'vamtam-elements-color-picker', VAMTAMEL_B_URL . 'assets/color-picker.css', array(), '1.0.0' );

			// color picker

			wp_enqueue_script( 'vamtam-elements-color-picker', VAMTAMEL_B_URL . 'assets/color-picker.js', array( 'jquery' ), '1.0.0', true );

			wp_localize_script( 'vamtam-elements-color-picker', 'VamtamColorPickerStrings', array(
				'colorPresetsAccents' => esc_html__( 'Color Presets and Accents', 'vamtam-elements-b' ),
				'accentsTitle'        => esc_html__( 'Accent Colors', 'vamtam-elements-b' ),
				'accents'             => isset( $vamtam_theme[ 'accent-color' ] ) ? array_filter( $vamtam_theme[ 'accent-color' ], function( $key ) {
					return is_numeric( $key );
				}, ARRAY_FILTER_USE_KEY ) : [],
			) );
		}

		wp_deregister_script( 'jquery-waypoints' );
	}

	/**
	 * Modify admin settings
	 */
	public static function fl_builder_admin_settings_nav_items( $items ) {
		unset( $items['upgrade'] );

		$items['icons']['show'] = true;

		return $items;
	}

	/**
	 * Beaver moved the icon manager to Beaver Pro, provide an alternative
	 */
	public static function icon_font_manager() {
		include VAMTAMEL_B_DIR . 'includes/admin-settings-icons.php';
	}

	/**
	 * Integrates our "Custom Font Families" option with the Beaver typography options
	 *
	 * @param  array $families
	 * @return array
	 */
	public static function fl_builder_font_families_system( $families ) {
		$custom = explode( "\n", get_option( 'vamtam_custom_font_families', '' ) );

		foreach ( $custom as $font ) {
			$families[ $font ] = [
				'weights' => [
					'default',
					'100',
					'200',
					'300',
					'400',
					'500',
					'600',
					'700',
					'800',
					'900',
				],
			];
		}

		return $families;
	}

	/**
	 * Remove "premium" attribute of select fields' options
	 */
	public static function fl_builder_render_settings_field( $field, $name, $settings ) {
		if ( 'select' === $field['type'] ) {
			foreach ( (array) $field['options'] as $option_key => $option_val ) {
				// regular options
				if ( is_array( $option_val ) && isset( $option_val['premium'] ) ) {
					unset( $field['options'][ $option_key ]['premium'] );
				}

				// option groups
				if ( is_array( $option_val ) && isset( $option_val['label'] ) && isset( $option_val['options'] ) ) {
					foreach ( (array) $option_val['options'] as $optgroup_option_key => $optgroup_option_val ) {
						if ( is_array( $optgroup_option_val ) && isset( $optgroup_option_val['premium'] ) ) {
							unset( $field['options'][ $option_key ]['options'][ $optgroup_option_key ]['premium'] );
						}
					}
				}
			}
		}

		return $field;
	}

	/**
	 * Disable some built-in Beaver modules
	 */
	public static function fl_builder_register_module( $enabled, $instance ) {
		if ( in_array( $instance->slug, array( 'sidebar' ), true ) ) {
			$enabled = false;
		}

		return $enabled;
	}

	/**
	 * Add animation attributes to an object
	 * @param  array  $attrs    original attributes
	 * @param  object $settings object (module, column, etc.) settings
	 * @return array            modified attributes
	 */
	private static function animation_attributes( $attrs, $settings ) {
		if ( isset( $attrs['data-animation-delay'] ) ) {
			unset( $attrs['data-animation-delay'] );

			// clear original class
			foreach ( $attrs['class'] as &$class ) {
				if ( strpos( $class, 'fl-animation' ) !== false ) {
					$class = '';
				}
			}
		}

		// no animations for ajax requests
		if ( ! isset( $_SERVER['HTTP_X_REQUESTED_WITH'] ) || 'xmlhttprequest' !== strtolower( $_SERVER['HTTP_X_REQUESTED_WITH'] ) ) {
			if ( ! empty( $settings->vamtam_animation_style ) ) {
				$pin        = false;
				$pinTrigger = '';

				if ( isset( $settings->vamtam_animation_pin ) && $settings->vamtam_animation_pin !== '0' ) {
					if ( $settings->vamtam_animation_pin === 'parent' ) {
						$pin = 'parent';
					} elseif ( $settings->vamtam_animation_pin === 'fixed' ) {
						$pin = (int) $settings->vamtam_animation_pin_duration;
					}

					$pinTrigger = $settings->vamtam_animation_pin_trigger;
				}

				$attrs['data-progressive-animation']    = $settings->vamtam_animation_style;
				$attrs['data-vamtam-animation-options'] = esc_attr( json_encode( array(
					'type'       => $settings->vamtam_animation_type,
					'origin'     => $settings->vamtam_animation_origin,
					'exit'       => (bool) $settings->vamtam_animation_exit,
					'delay'      => (float) $settings->vamtam_animation_delay,
					'exit'       => (bool) $settings->vamtam_animation_exit,
					'mobile'     => (bool) $settings->vamtam_animation_mobile,
					'pin'        => $pin,
					'pinTrigger' => $pinTrigger,
				) ) );
			}
		}

		return $attrs;
	}

	/**
	 * Custom module attributes
	 *
	 * @param  array  $attrs  original attributes
	 * @param  object $module module object
	 * @return array          new attributes
	 */
	public static function fl_builder_module_attributes( $attrs, $module ) {
		$attrs = self::animation_attributes( $attrs, $module->settings );

		if ( $module->settings->type === 'vamtam-blog' ) {
			$attrs['class'][] = 'vamtam-blog-' . $module->settings->layout;
		} elseif ( $module->settings->type === 'vamtam-projects' ) {
			$attrs['class'][] = 'vamtam-projects-' . $module->settings->layout;
		} elseif ( $module->settings->type === 'vamtam-testimonials' ) {
			$attrs['class'][] = 'vamtam-testimonials-' . $module->settings->layout_type;
		}

		return $attrs;
	}

	/**
	 * Override built-in module templates
	 *
	 * @param  string $file
	 * @param  string $module
	 */
	public static function fl_builder_module_frontend_file( $file, $module ) {
		$override = VAMTAMEL_B_DIR . 'built-in-modules/' . $module->settings->type . '/includes/frontend.php';

		if ( file_exists( $override ) ) {
			return $override;
		}

		return $file;
	}

	/**
	 * Custom column attributes
	 *
	 * @param  array  $attrs  original attributes
	 * @param  object $column column object
	 * @return array          new attributes
	 */
	public static function fl_builder_column_attributes( $attrs, $column ) {
		$attrs = self::animation_attributes( $attrs, $column->settings );

		return $attrs;
	}

	/**
	 * Custom row attributes
	 *
	 * @param  array  $attrs original attributes
	 * @param  object $row   row object
	 * @return array         new attributes
	 */
	public static function fl_builder_row_attributes( $attrs, $row ) {
		$attrs['class'][] = 'fl-row-bg-attachment-' . $row->settings->bg_attachment;

		if ( $row->settings->bg_type === 'parallax' ) {
			$attrs['data-background-position'] = $row->settings->bg_parallax_position;
			$attrs['data-background-size'] = $row->settings->bg_parallax_size;
		}

		$attrs = self::animation_attributes( $attrs, $row->settings );

		return $attrs;
	}

	/**
	 * Templates CPT attributes
	 * @param  array $args
	 * @return array
	 */
	public static function fl_builder_register_template_post_type_args( $args ) {
		$args['labels']['menu_name'] = esc_html__( 'VamTam Builder', 'vamtam-elements-b' );

		return $args;
	}

	/**
	 * Append more icon sets
	 *
	 * @param  array $sets original core sets
	 * @return array
	 */
	public static function fl_builder_icon_sets( $sets ) {
		if ( defined( 'VAMTAM_ASSETS_DIR' ) ) {
			$icomoon_icons = array_keys( include VAMTAM_ASSETS_DIR . 'fonts/icons/list.php' );

			foreach ( $icomoon_icons as &$icon ) {
				$icon = 'vamtam-icomoon-' . $icon;
			}

			$sets['vamtam-icomoon'] = array(
				'name'       => 'IcoMoon',
				'prefix'     => '',
				'type'       => 'icomoon',
				'stylesheet' => VAMTAM_ASSETS_URI . 'fonts/icons/style.css',
				'icons'      => $icomoon_icons,
			);

			$theme_icons = array_keys( include VAMTAM_ASSETS_DIR . 'fonts/theme-icons/list.php' );

			foreach ( $theme_icons as &$icon ) {
				$icon = 'vamtam-theme-' . $icon;
			}

			$sets['vamtam-theme'] = array(
				'name'       => 'VamTam (theme-specific)',
				'prefix'     => '',
				'type'       => 'icomoon',
				'stylesheet' => VAMTAM_ASSETS_URI . 'fonts/theme-icons/style.css',
				'icons'      => $theme_icons,
			);
		}

		return $sets;
	}

	/**
	 * Don't enqueue any stylesheet for the theme icons
	 * @param  bool   $enqueue
	 * @param  string $key     icon set name
	 * @return bool
	 */
	public static function fl_builder_enqueue_custom_styles_by_key( $enqueue, $key ) {
		if ( ! FLBuilderModel::is_builder_active() && $key === 'vamtam-theme' ) {
			return false;
		}

		return $enqueue;
	}

	/**
	 * Returns an array of slugs for all enabled icon sets.
	 *
	 * @since 1.4.6
	 * @return array
	 */
	public static function get_enabled_icons() {
		$value = FLBuilderModel::get_admin_settings_option( '_fl_builder_enabled_icons', true );

		return ! $value ? array( 'font-awesome', 'foundation-icons', 'dashicons', 'vamtam-icomoon', 'vamtam-theme' ) : $value;
	}

	/**
	 * Remove the "Update" prompt in the user templates dropdown
	 * @param  array $data
	 * @return array
	 */
	public static function fl_builder_content_panel_data( $data ) {
		if ( isset( $data['tabs']['templates']['views'][0]['handle'] ) && $data['tabs']['templates']['views'][0]['handle'] === 'standard' ) {
			unset( $data['tabs']['templates']['views'][0] );
		}

		return $data;
	}

	/**
	 * Some main menu items are disabled, enable them if we provide an alternative
	 * @param  array $menu
	 * @return array
	 */
	public static function fl_builder_main_menu( $views ) {
		$is_user_template  = FLBuilderModel::is_post_user_template();
		$enabled_templates = FLBuilderModel::get_enabled_templates();
		$key_shortcuts     = FLBuilder::get_keyboard_shortcuts();

		if ( ! $is_user_template && ( 'enabled' == $enabled_templates || 'user' == $enabled_templates ) ) {
			$views['main']['items'][10] = array(
				'label'     => __( 'Save Template', 'fl-builder' ),
				'type'      => 'event',
				'eventName' => 'saveTemplate',
				'accessory' => $key_shortcuts['saveTemplate']['keyLabel'],
			);
		}

		return $views;
	}

	/**
	 * Modify keyboard shortcuts
	 * @param  array $shortcuts
	 * @return array
	 */
	public static function fl_builder_keyboard_shortcuts( $shortcuts ) {
		$shortcuts['saveTemplate']['enabled'] = true;
		$shortcuts['showSearch']['enabled']   = true;

		return $shortcuts;
	}

	/**
	 * Removed in 1.2.0, use FLBuilderLoop::query()
	 */
	public static function query( $settings ) {
		return FLBuilderLoop::query( $settings );
	}

	/**
	 * Used for lightbox-enabled modules
	 */
	public static function enqueue_lightbox_template() {
		$hook = array( __CLASS__, 'print_lightbox_template' );
		$tag  = 'print_footer_scripts';

		if ( ! has_action( $tag, $hook ) ) {
			add_action( $tag, $hook );
		}
	}

	public static function print_lightbox_template() {
		?>
		<script type="text/html" id="vamtam-lightbox-template">
			<button class="vamtam-button-lightbox-close"><?php echo vamtam_get_icon_html( array( // xss ok
				'name' => 'vamtam-theme-close-sample',
			) ) ?></button>
			<div class="vamtam-button-lightbox-content">
				{{ lightbox_content }}
			</div>
		</script>
		<?php
	}
}

Vamtam_Elements_B::setup();

