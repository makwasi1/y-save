<?php

/**
 * Theme options / General / Posts
 *
 * @package vamtam/consulting
 */

return array(

	array(
		'label'       => esc_html__( 'Pagination Type', 'vamtam-consulting' ),
		'description' => esc_html__( 'Also used for portfolio', 'vamtam-consulting' ),
		'id'          => 'pagination-type',
		'type'        => 'select',
		'choices'     => array(
			'paged'              => esc_html__( 'Paged', 'vamtam-consulting' ),
			'load-more'          => esc_html__( 'Load more button', 'vamtam-consulting' ),
			'infinite-scrolling' => esc_html__( 'Infinite scrolling', 'vamtam-consulting' ),
		),
	),

	array(
		'label'       => esc_html__( 'Show "Related Posts" in Single Post View', 'vamtam-consulting' ),
		'description' => esc_html__( 'Enabling this option will show more posts from the same category when viewing a single post.', 'vamtam-consulting' ),
		'id'          => 'show-related-posts',
		'type'        => 'switch',
		'transport'   => 'postMessage',
	),

	array(
		'label'     => esc_html__( '"Related Posts" title', 'vamtam-consulting' ),
		'id'        => 'related-posts-title',
		'type'      => 'text',
		'transport' => 'postMessage',
	),

	array(
		'label'     => esc_html__( 'Meta Information', 'vamtam-consulting' ),
		'id'        => 'post-meta',
		'type'      => 'multicheck',
		'transport' => 'postMessage',
		'choices'   => array(
			'author'   => esc_html__( 'Post Author', 'vamtam-consulting' ),
			'tax'      => esc_html__( 'Categories and Tags', 'vamtam-consulting' ),
			'date'     => esc_html__( 'Timestamp', 'vamtam-consulting' ),
			'comments' => esc_html__( 'Comment Count', 'vamtam-consulting' ),
		),
	),

	array(
		'label'       => esc_html__( 'Show Featured Image on Single Posts', 'vamtam-consulting' ),
		'id'          => 'show-single-post-image',
		'description' => esc_html__( 'Please note, that this option works only for Blog Post Format Image.', 'vamtam-consulting' ),
		'type'        => 'switch',
		'transport'   => 'postMessage',
	),

	array(
		'label'       => esc_html__( 'Post Archive Layout', 'vamtam-consulting' ),
		'description' => '',
		'id'          => 'archive-layout',
		'type'        => 'radio',
		'choices'     => array(
			'normal' => esc_html__( 'Large', 'vamtam-consulting' ),
			'mosaic' => esc_html__( 'Small', 'vamtam-consulting' ),
		),
	),

);
