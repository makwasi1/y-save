<?php

/**
 * Theme options / Footer
 *
 * @package vamtam/consulting
 */

return array(
	array(
		'label'   => esc_html__( 'Show Footer on One Page Template', 'vamtam-consulting' ),
		'id'      => 'one-page-footer',
		'type'    => 'switch',
	),

	array(
		'label'     => esc_html__( 'Footer Template', 'vamtam-consulting' ),
		'id'        => 'footer-beaver-template',
		'type'      => 'select',
		'choices'   => vamtam_get_beaver_layouts( array(
			'' => esc_html__( '-- Select Template --', 'vamtam-consulting' ),
		) ),
	),

	array(
		'id'    => 'footer-typography-title',
		'label' => esc_html__( 'Typography', 'vamtam-consulting' ),
		'type'  => 'heading',
	),

	array(
		'label'       => esc_html__( 'Widget Areas Titles', 'vamtam-consulting' ),
		'description' => esc_html__( 'Please note that this option will override the general headings style set in the General Typography" tab.', 'vamtam-consulting' ),
		'id'          => 'footer-sidebars-titles',
		'type'        => 'typography',
		'compiler'    => true,
		'transport'   => 'postMessage',
	),
);
