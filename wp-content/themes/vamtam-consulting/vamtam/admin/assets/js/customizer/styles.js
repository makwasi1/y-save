/* jshint esnext:true */

import { isNumeric, hexToHsl, getLuminance } from './helpers';

const styles = ( api, $ ) => {
	'use strict';

	const prepare_background = to => {
		if ( to['background-image'] !== '' ) {
			to['background-image'] = 'url(' + to['background-image'] + ')';
		}

		return to;
	};

	api( 'vamtam_theme[top-nav-background]', value => {
		value.bind( to => {
			$( '#top-nav-wrapper, #top-nav-wrapper-filler' ).css( prepare_background( to ) );
		} );
	} );

	{
		const compiler_options = VAMTAM_CUSTOMIZE_PREVIEW.compiler_options;

		const real_id = function( id ) {
			return id.replace( /vamtam_theme\[([^\]]+)]/, '$1' );
		};

		const change_handler_by_type = {
			number: function( to ) {
				let id = real_id( this.id );

				if ( VAMTAM_CUSTOMIZE_PREVIEW.percentages.indexOf( id ) !== -1 ) {
					to += '%';
				} else if ( VAMTAM_CUSTOMIZE_PREVIEW.numbers.indexOf( id ) !== -1 ) {
					// as is
				} else {
					to += 'px';
				}

				document.documentElement.style.setProperty( `--vamtam-${id}`, to )

				// trigger a resize event if we change any dimension
				$( window ).resize();
			},
			background: function( to ) {
				let id = real_id( this.id );

				to = prepare_background( to );

				for ( let prop in to ) {
					document.documentElement.style.setProperty( `--vamtam-${id}-${prop}`, to[ prop ] );
				}
			},
			radio: function( to ) {
				let id = real_id( this.id );

				if ( isNumeric( to ) ) {
					change_handler_by_type.number.call( this, to );
				} else {
					document.documentElement.style.setProperty( `--vamtam-${id}`, to )
				}
			},
			select: function( to ) {
				let id = real_id( this.id );

				change_handler_by_type.radio.call( this, to );
			},
			typography: function( to, from ) {
				let id = real_id( this.id );

				let variant = to.variant;

				to['font-weight'] = 'normal';
				to['font-style']  = 'normal';

				to.variant = to.variant.split( ' ' );

				if ( to.variant.length === 2 ) {
					to['font-weight'] = to.variant[0];
					to['font-style']  = to.variant[1];
				} else if ( to.variant[0] === 'italic' ) {
					to['font-style'] = 'italic';
				} else {
					to['font-weight'] = to.variant[0];
				}

				delete to.variant;

				for ( let prop in to ) {
					document.documentElement.style.setProperty( `--vamtam-${id}-${prop}`, to[ prop ] );
				}

				// if the font-family is changed - we need to load the new font stylesheet
				if ( to['font-family'] !== from['font-family'] || to['variant'] !== from['variant'] ) {
					let new_font = window.top.VAMTAM_ALL_FONTS[ to['font-family'] ];

					if ( new_font.gf ) {
						let family = encodeURIComponent( to['font-family'] ) + ':' + new_font.weights.join( ',' ).replace( ' ', '' );
						let subset = ''; // no subset support here, only newer browser can preview Google Fonts

						let link = document.createElement("link");
						link.href = 'https://fonts.googleapis.com/css?family=' + family + '&subset=' + subset;
						link.type = 'text/css';
						link.rel = 'stylesheet';
						document.getElementsByTagName( 'head' )[0].appendChild(link);
					}
				}
			},
			'color-row': function( to ) {
				let id = real_id( this.id );

				if ( 'auto-contrast' in to && to['auto-contrast'] ) {
					for( let i=1; i<=8; i++ ) {
						let accent_color           = to[i];
						let accent_color_luminence = getLuminance( accent_color );
						if( accent_color_luminence > 0.4 ) {
							to[i+'-hc'] = '#000000';
						} else {
							to[i+'-hc'] = '#ffffff';
						}
					}
				}

				for ( let prop in to ) {
					if ( prop !=='auto-contrast' ) {
						document.documentElement.style.setProperty( `--vamtam-${id}-${prop}`, to[ prop ] );
					}
				}
			},
			color: function( to ) {
				let id = real_id( this.id );

				document.documentElement.style.setProperty( `--vamtam-${id}`, to )
			},
			'responsive-position': function( to ) {
				let id = real_id( this.id );

				for ( let device in to ) {
					for ( let side in to[ device ] ) {
						document.documentElement.style.setProperty( `--vamtam-${id}-${device}-${side}`, to[ device ][ side ] + 'px' );
					}
				}
			},
		};

		// const compiler_option_handler = ;
		for ( let opt_name in compiler_options ) {
			api( opt_name, function( setting ) {
				const type = compiler_options[ opt_name ];

				if ( type in change_handler_by_type ) {
					setting.bind( change_handler_by_type[ type ] );
				} else {
					console.error( `VamTam Customzier: Missing handler for option type ${type} - option ${opt_name}` );
					window.wpvval = setting;
				}
			} );
		}
	}

	api( 'vamtam_theme[page-title-background-hide-lowres]', value => {
		value.bind( to => {
			$( 'header.page-header' ).toggleClass( 'vamtam-hide-bg-lowres', to );
		} );
	} );

	api( 'vamtam_theme[main-background-hide-lowres]', value => {
		value.bind( to => {
			$( '.vamtam-main' ).toggleClass( 'vamtam-hide-bg-lowres', to );
		} );
	} );
};

export default styles;
