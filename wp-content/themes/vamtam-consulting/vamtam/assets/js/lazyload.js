(function(v, undefined) {
	'use strict';

	// lazy loading
	var observer;
	var supportsCssVars = ( window.CSS && window.CSS.supports && window.CSS.supports( '(--foo: red)' ) );

	if ( 'IntersectionObserver' in window ) {
		observer = new IntersectionObserver( function( changes ) {
			changes.forEach( function( change ) {
				if ( change.intersectionRatio > 0 || change.isIntersecting ) {
					showImage( change.target );
					observer.unobserve(change.target);
				}
			});
		}, {
			rootMargin: '200px',
		});
	}

	function onImageLoad() {
		/* jshint validthis: true */
		this.removeEventListener( 'load', onImageLoad );

		requestAnimationFrame( function() {
			if ( ! ( this.classList.contains( 'vamtam-lazyload-noparent' ) ) && this.parentElement ) {
				this.parentElement.classList.add( 'image-loaded' );
			} else {
				this.classList.add( 'image-loaded' );
			}
		}.bind( this ) );
	}

	function showImage( image ) {
		var srcset = image.dataset.srcset;

		if ( srcset ) {
			requestAnimationFrame( function() {
				image.addEventListener( 'load', onImageLoad );
				image.srcset = srcset;
			} );

			delete image.dataset.srcset;
		} else {
			onImageLoad.call( image );
		}
	}

	// Either observe the images, or load immediately if IntersectionObserver doesn't exist
	function addElements() {
		var images = document.querySelectorAll('img[data-srcset]');
		var i;

		// IE11 doesn't fire the load event
		if ( ! supportsCssVars ) {
			var allImages = document.querySelectorAll( '.vamtam-responsive-wrapper:not(.image-loaded), .vamtam-lazyload-noparent:not(.image-loaded)' );

			for ( i = 0; i < allImages.length; i++ ) {
				allImages[i].classList.add( 'image-loaded' );
			}
		}

		if ( observer && supportsCssVars ) {
			for ( i = 0; i < images.length; i++ ) {
				if ( ! ( 'vamtamLazyLoaded' in images[i] ) ) {
					images[i].vamtamLazyLoaded = true;
					observer.observe( images[i] );
				}
			}
		} else {
			for ( i = 0; i < images.length; i++ ) {
				if ( ! ( 'vamtamLazyLoaded' in images[i] ) ) {
					images[i].vamtamLazyLoaded = true;
					showImage( images[i] );
				}
			}
		}

		var otherImages = document.querySelectorAll('.vamtam-responsive-wrapper:not(.image-loaded) img:not([srcset])');

		for ( i = 0; i < otherImages.length; i++ ) {
			if ( ! ( 'vamtamLazyLoaded' in otherImages[i] ) ) {
				otherImages[i].vamtamLazyLoaded = true;
				showImage( otherImages[i] );
			}
		}

		var backgrounds = document.querySelectorAll( '.fl-row-content-wrap:not(.vamtam-show-bg-image), .fl-col-content:not(.vamtam-show-bg-image)' );
		for ( i = 0; i < backgrounds.length; i++ ) {
			backgrounds[i].classList.add( 'vamtam-show-bg-image' );
		}
	}

	document.addEventListener('DOMContentLoaded', function() {
		var mutationObserver = new MutationObserver( addElements );

		mutationObserver.observe( document.body, {
			childList: true,
			subtree: true
		} );

		addElements();
	});
})( window.VAMTAM );
