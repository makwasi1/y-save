<?php

/**
 * Declare plugin dependencies
 *
 * @package vamtam/consulting
 */

/**
 * Declare plugin dependencies
 */
function vamtam_register_required_plugins() {
	// this is for setting the minimum required version of a bundled plugin (not from store)
	// so tgmpa can see the new version and handle the plugin as updatable.
	$updates = get_site_transient( 'update_plugins' );
	$bundled_plugins_names = [
		'vamtam-elements-b/vamtam-elements.php',
		'vamtam-importers/vamtam-importers.php',
		'revslider/revslider.php',
	];

	$plugins = array(
		// this is a feature plugin,
		// will be removed when it's merged in WP Core
		array(
			'name'     => esc_html__( 'Safe SVG', 'vamtam-consulting' ),
			'slug'     => 'safe-svg',
			'required' => true,
			'category' => 'required',
		),

		array(
			'name'     => esc_html__( 'Jetpack', 'vamtam-consulting' ),
			'slug'     => 'jetpack',
			'required' => true,
			'category' => 'required',
		),

		array(
			'name'     => esc_html__( 'Unplug Jetpack', 'vamtam-consulting' ),
			'slug'     => 'unplug-jetpack',
			'required' => true,
			'category' => 'required',
		),

		array(
			'name'     => esc_html__( 'Beaver Builder - WordPress Page Builder', 'vamtam-consulting' ),
			'slug'     => 'beaver-builder-lite-version',
			'required' => true,
			'category' => 'required',
		),

		array(
			'name'     => esc_html__( 'WP Retina 2x', 'vamtam-consulting' ),
			'slug'     => 'wp-retina-2x',
			'required' => true,
			'category' => 'required',
		),

		array(
			'name'     => esc_html__( 'Max Mega Menu', 'vamtam-consulting' ),
			'slug'     => 'megamenu',
			'required' => true,
			'category' => 'required',
		),

		array(
			'name'     => esc_html__( 'Ninja Forms', 'vamtam-consulting' ),
			'slug'     => 'ninja-forms',
			'required' => true,
			'category' => 'required',
		),

		array(
			'name'     => esc_html__( 'WooCommerce', 'vamtam-consulting' ),
			'slug'     => 'woocommerce',
			'required' => true,
			'category' => 'required',
		),

		array(
			'name'     => esc_html__( 'The Events Calendar', 'vamtam-consulting' ),
			'slug'     => 'the-events-calendar',
			'required' => true,
			'category' => 'required',
		),

		array(
			'name'     => esc_html__( 'Event Tickets', 'vamtam-consulting' ),
			'slug'     => 'event-tickets',
			'required' => false,
			'category' => 'recommended',
		),

		array(
			'name'     => esc_html__( 'Vamtam Elements (B)', 'vamtam-consulting' ),
			'slug'     => 'vamtam-elements-b',
			'source'   => VAMTAM_PLUGINS . 'vamtam-elements-b.zip',
			'required' => true,
			'category' => 'required',
			'version'  => ( $updates !== false && isset( $updates->response[ $bundled_plugins_names[0] ] ) )
				? $updates->response[ $bundled_plugins_names[0] ]->new_version
				: '1.11.0',
		),

		array(
			'name'     => esc_html__( 'Vamtam Importers', 'vamtam-consulting' ),
			'slug'     => 'vamtam-importers',
			'source'   => VAMTAM_PLUGINS . 'vamtam-importers.zip',
			'required' => true,
			'category' => 'required',
			'version'  => ( $updates !== false && isset( $updates->response[ $bundled_plugins_names[0] ] ) )
				? $updates->response[ $bundled_plugins_names[0] ]->new_version
				: '2.12.0',
		),

		array(
			'name'     => esc_html__( 'Revolution Slider', 'vamtam-consulting' ),
			'slug'     => 'revslider',
			'source'   => VAMTAM_PLUGINS . 'revslider.zip',
			'required' => true,
			'category' => 'required',
			'version'  => ( $updates !== false && isset( $updates->response[ $bundled_plugins_names[0] ] ) )
				? $updates->response[ $bundled_plugins_names[0] ]->new_version
				: '5.4.7.4',
		),

		array(
			'name'         => 'Booked',
			'slug'         => 'booked',
			'required'     => true,
			'category' => 'required',
			'version'      => '2.0.7',
			'source'       => 'https://boxyupdates.com/get/?action=download&slug=booked',
			'external_url' => 'https://boxyupdates.com/get/?action=download&slug=booked',
		),

		array(
			'name'         => 'Booked Add On- Payments with WooCommerce',
			'slug'         => 'booked-woocommerce-payments',
			'source'       => 'https://boxyupdates.com/get/?action=download&slug=booked-woocommerce-payments',
			'external_url' => 'https://boxyupdates.com/get/?action=download&slug=booked-woocommerce-payments',
			'required'     => false,
			'category' => 'recommended',
			'version'      => '1.4.7',
		),
		array(
			'name'         => 'Booked Add On- Calendar Feeds',
			'slug'         => 'booked-calendar-feeds',
			'source'       => 'https://boxyupdates.com/get/?action=download&slug=booked-calendar-feeds',
			'external_url' => 'https://boxyupdates.com/get/?action=download&slug=booked-calendar-feeds',
			'required'     => false,
			'category' => 'recommended',
			'version'      => '1.1.5',
		),
		array(
			'name'         => 'Booked Add On- Front-End Agents',
			'slug'         => 'booked-frontend-agents',
			'source'       => 'https://boxyupdates.com/get/?action=download&slug=booked-frontend-agents',
			'external_url' => 'https://boxyupdates.com/get/?action=download&slug=booked-frontend-agents',
			'required'     => false,
			'category' => 'recommended',
			'version'      => '1.1.15',
		),

		array(
			'name'     => esc_html__( 'Easy Charts', 'vamtam-consulting' ),
			'slug'     => 'easy-charts',
			'required' => false,
			'category' => 'recommended',
		),

		array(
			'name'     => esc_html__( 'Wordpress SEO', 'vamtam-consulting' ),
			'slug'     => 'wordpress-seo',
			'required' => false,
			'category' => 'recommended',
		),

		array(
			'name'     => esc_html__( 'Really Simple SSL', 'vamtam-consulting' ),
			'slug'     => 'really-simple-ssl',
			'required' => false,
			'category' => 'recommended',
		),

		array(
			'name'     => esc_html__( 'Stops Core Theme and Plugin updates', 'vamtam-consulting' ),
			'slug'     => 'stops-core-theme-and-plugin-updates',
			'required' => false,
			'category' => 'recommended',
		),

		array(
			'name'     => esc_html__( 'Loco Translate', 'vamtam-consulting' ),
			'slug'     => 'loco-translate',
			'required' => false,
			'category' => 'recommended',
		),

		array(
			'name'     => esc_html__( 'UpDraftPlus', 'vamtam-consulting' ),
			'slug'     => 'updraftplus',
			'required' => false,
			'category' => 'recommended',
		),

		array(
			'name'     => esc_html__( 'WP Super Cache', 'vamtam-consulting' ),
			'slug'     => 'wp-super-cache',
			'required' => false,
			'category' => 'recommended',
		),

		array(
			'name'     => esc_html__( 'Limit Login Attempts Reloaded', 'vamtam-consulting' ),
			'slug'     => 'limit-login-attempts-reloaded',
			'required' => false,
			'category' => 'recommended',
		),

		array(
			'name'     => esc_html__( 'Wordfence', 'vamtam-consulting' ),
			'slug'     => 'wordfence',
			'required' => false,
			'category' => 'recommended',
		),

		array(
			'name'     => esc_html__( 'Instagram Feed', 'vamtam-consulting' ),
			'slug'     => 'instagram-feed',
			'required' => false,
			'category' => 'recommended',
		),
	);

	$config = array(
		'default_path' => '',    // Default absolute path to pre-packaged plugins
		'is_automatic' => true,  // Automatically activate plugins after installation or not
		'parent_slug' => 'vamtam_theme_setup',
	);

	tgmpa( $plugins, $config );
}
add_action( 'tgmpa_register', 'vamtam_register_required_plugins' );

function vamtam_tgmpa_table_columns( $columns ) {
	//Filter the header columns for the plugin page
	$columns = array(
		'cb'     => esc_html__( '<input type="checkbox" />', 'vamtam-consulting' ),
		'img'     => '',
		'plugin' => esc_html__( 'Name', 'vamtam-consulting' ),
		'description' => esc_html__( 'Description', 'vamtam-consulting' ),
		'status'   => esc_html__( 'Status', 'vamtam-consulting' ),
		'version'   => esc_html__( 'Version', 'vamtam-consulting' ),
	);
	return $columns;
}
add_filter( 'tgmpa_table_columns', 'vamtam_tgmpa_table_columns' );

function vamtam_tgmpa_table_data_item( $item, $plugin )
{
	$thumbnail_size = '128x128';

	$plugins_no_store_img = array(
		'booked',
		'booked-calendar-feeds',
		'booked-frontend-agents',
		'booked-woocommerce-payments',
		'easy-charts',
		'revslider',
		'unplug-jetpack',
		'vamtam-elements-b',
		'vamtam-importers',
		'limit-login-attempts-reloaded',
		'updraftplus',
		'woocommerce-product-archive-customiser',
	);

	// Plugin image
	if( in_array( $plugin['slug'], $plugins_no_store_img ) ) {
		$thumbnail = VAMTAM_ADMIN_ASSETS_URI . 'images/def-plugin.png';
	} else{
		$thumbnail = 'https://ps.w.org/'. $plugin['slug'] .'/assets/icon-'. $thumbnail_size .'.png';
	}

	$item['img'] = '<img src="' . esc_url( $thumbnail ) . '" width="64" height="64" data-category="' . esc_attr( $plugin['category'] ) . '" >';

	$tgmpa_instance = call_user_func( array( get_class( $GLOBALS['tgmpa'] ), 'get_instance' ) );

	if( $tgmpa_instance->is_plugin_installed( $plugin['slug'] ) ) {
		$plugin_data = get_plugin_data( WP_PLUGIN_DIR . '/' . $plugin['file_path'] );
	} else {
		// Get from store
		$plugin_data = VamtamPluginManager::get_plugin_data_by_slug( $plugin['slug'] );
	}

	// Plugin Version
	if ( ! isset( $item['available_version'] ) || empty( $item['available_version'] ) ) {
		$item['available_version'] = ! empty( $plugin_data ) && $plugin_data['Version'];
	}


	// Plugin description
	if( isset( $plugin_data['Description'] ) ) {
		$item['description'] = $plugin_data['Description'];
	} else {
		$item['description'] =  esc_html__( 'A simple WordPress Plugin.', 'vamtam-consulting' );
	}
	if ( strpos( $plugin['slug'], 'vamtam-consulting' ) !== false ) {
		$item['description'] =  esc_html__( 'Theme Exclusive.', 'vamtam-consulting' );
	}

	return $item;
}
add_filter( 'tgmpa_table_data_item', 'vamtam_tgmpa_table_data_item', 10, 2 );

// vamtam-tgmpa.js will gracefully hide the recommended plugins notice.
function vamtam_hide_tgmpa_notice() {
	$screen = get_current_screen();
	if ( is_admin() && $screen->id === 'plugins' ) {
		wp_enqueue_script( 'vamtam-tgmpa', VAMTAM_ADMIN_ASSETS_URI . 'js/vamtam-tgmpa.js', array( 'jquery-core' ), VamtamFramework::get_version(), true );
	}
}
add_action('admin_enqueue_scripts', 'vamtam_hide_tgmpa_notice');

/**
 * Essentially a copy of the standard tgmpa plugins page but with our dashboard and validation.
 *
 * This displays the admin page and form area where the user can select to install and activate the plugin.
 * Aborts early if we're processing a plugin installation action.
 *
 * Important!!
 * 	For this to work we need to make tgmpa's do_plugin_install public ( from protected ).
 *
 * @return null Aborts early if we're processing a plugin installation action.
 */
function vamtam_install_plugins_page() {
	$tgmpa = TGM_Plugin_Activation::get_instance();
	// Store new instance of plugin table in object.
	$plugin_table = new TGMPA_List_Table;

	// Single item, row actions are handled by tgmpa's do_plugin_install().
	$row_actions_active = is_callable( [ $tgmpa, 'do_plugin_install' ] );

	// Return early if processing a plugin installation action.
	if ( ( ( 'tgmpa-bulk-install' === $plugin_table->current_action() || 'tgmpa-bulk-update' === $plugin_table->current_action() ) && $plugin_table->process_bulk_actions() ) || ( $row_actions_active && $tgmpa->do_plugin_install() ) ) {
		return;
	}

	// Force refresh of available plugin information so we'll know about manual updates/deletes.
	wp_clean_plugins_cache( false );
	$valid_key = Version_Checker::is_valid_purchase_code();
	?>
	<div id="vamtam-ts-tgmpa" class="vamtam-ts">
			<div id="vamtam-ts-side">
				<?php VamtamPurchaseHelper::dashboard_navigation(); ?>
			</div>
			<div id="vamtam-ts-main">
				<?php if ( $valid_key ) : ?>
				<?php $plugin_table->prepare_items(); ?>

				<?php
				if ( ! empty( $tgmpa->message ) && is_string( $tgmpa->message ) ) {
					echo wp_kses_post( $tgmpa->message );
				}
				?>
				<?php $plugin_table->views(); ?>

				<form id="tgmpa-plugins" action="" method="post" class="<?php echo esc_attr( ! $row_actions_active ? 'no-row-actions' : '' ); ?>">
					<?php VamtamPluginManager::filter_tabs(); ?>
					<input type="hidden" name="tgmpa-page" value="<?php echo esc_attr( $tgmpa->menu ); ?>" />
					<input type="hidden" name="plugin_status" value="<?php echo esc_attr( $plugin_table->view_context ); ?>" />
					<?php $plugin_table->display(); ?>
				</form>
				<?php else : ?>
					<?php VamtamPurchaseHelper::registration_warning(); ?>
				<?php endif ?>
			</div>
	</div>
	<?php
}

function vamtam_tgmpa_admin_menu_args( $args ) {
	$args['page_title'] = esc_html__( 'Install Plugins', 'vamtam-consulting' );
	$args['function'] = 'vamtam_install_plugins_page';

	return $args;
}
add_filter( 'tgmpa_admin_menu_args', 'vamtam_tgmpa_admin_menu_args' );

// Hide all notices on theme setup pages.
function vamtam_in_admin_header() {
	if ( isset ( $_GET['page'] ) ) {
		$is_vamtam_ts_page = in_array( $_GET['page'], array(
			'vamtam_theme_setup',
			'tgmpa-install-plugins',
			'vamtam_theme_setup_import_content',
			'vamtam_theme_help',
		) );
		if ( $is_vamtam_ts_page ) {
			remove_all_actions( 'admin_notices' );
			remove_all_actions( 'all_admin_notices' );
		}
	}
}
add_action('in_admin_header', 'vamtam_in_admin_header', 1000 );