<?php

/**
 * Displays the scroll to top button
 *
 * @package vamtam/consulting
 */
?>

<div id="scroll-to-top" class="vamtam-scroll-to-top icon"><?php vamtam_icon( 'vamtam-theme-arrow-top-sample' ) ?></div>
