<?php
	/**
	 * Top bar ( above the logo )
	 * @package vamtam/consulting
	 */

	$layout = vamtam_get_option( 'top-bar-layout' );

	if ( empty( $layout ) || $layout === 'none' || ! vamtam_extra_features() ) {
		return;
	}

	$is_beaver = strpos( $layout, 'beaver-' ) === 0;

	$layout = $is_beaver ? str_replace( 'beaver-', '', $layout ) : explode( '-', $layout );
?>
<div id="top-nav-wrapper" style="<?php echo esc_attr( VamtamTemplates::build_background( vamtam_get_option( 'top-nav-background' ) ) ) ?>">
	<?php do_action( 'vamtam_top_nav_before' ) ?>
	<?php
		if ( $is_beaver ) {
			if ( class_exists( 'FLBuilderShortcodes' ) ) {
				echo '<nav class="top-nav">';
				echo FLBuilderShortcodes::insert_layout( array( // xss ok
					'slug' => $layout,
				) );
				echo '</nav>';
			}
		} else {
			get_template_part( 'templates/header/top/nav', 'inner' );
		}
	?>
	<?php do_action( 'vamtam_top_nav_after' ) ?>
</div>
