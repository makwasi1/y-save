<?php
$show = vamtam_get_optionb( 'post-meta', 'tax' );

$categories_list = get_the_category_list( ', ' );

if ( $categories_list && ( $show || is_customize_preview() ) ) :
?>
	<div class="vamtam-meta-tax" <?php VamtamTemplates::display_none( $show ) ?>><span class="icon theme"><?php vamtam_icon( 'vamtam-theme-layers' ); ?></span> <span class="visuallyhidden"><?php esc_html_e( 'Category', 'vamtam-consulting' ) ?> </span><?php echo $categories_list // xss ok ?></div>
<?php
endif;
